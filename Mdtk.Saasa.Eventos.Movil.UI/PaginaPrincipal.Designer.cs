﻿namespace Mdtk.Saasa.Eventos.Movil.UI
{
    partial class PaginaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuRecepcionManifiesto = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuAsignarUbicacion = new System.Windows.Forms.MenuItem();
            this.menuConsultaUA = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuSalidaUA = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuDescargaManifiesto = new System.Windows.Forms.MenuItem();
            this.menuRecepcionGuias = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.btnEntrar = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.Location = new System.Drawing.Point(30, 85);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(180, 47);
            label1.Text = "Bienvenio al sistema Mobil\r\nde Saasa HERMES";
            label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem8);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuItem2);
            this.menuItem1.MenuItems.Add(this.menuItem3);
            this.menuItem1.MenuItems.Add(this.menuItem4);
            this.menuItem1.MenuItems.Add(this.menuItem5);
            this.menuItem1.MenuItems.Add(this.menuItem6);
            this.menuItem1.MenuItems.Add(this.menuItem7);
            this.menuItem1.MenuItems.Add(this.menuItem13);
            this.menuItem1.Text = "Eventos Impo";
            // 
            // menuItem2
            // 
            this.menuItem2.MenuItems.Add(this.menuRecepcionManifiesto);
            this.menuItem2.Text = "Llegada Manifiesto";
            // 
            // menuRecepcionManifiesto
            // 
            this.menuRecepcionManifiesto.Text = "RecepcionManifiesto";
            this.menuRecepcionManifiesto.Click += new System.EventHandler(this.menuRecepcionManifiesto_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.MenuItems.Add(this.menuAsignarUbicacion);
            this.menuItem3.MenuItems.Add(this.menuConsultaUA);
            this.menuItem3.Text = "Ubicacion";
            // 
            // menuAsignarUbicacion
            // 
            this.menuAsignarUbicacion.Text = "AsignarUbicacion";
            this.menuAsignarUbicacion.Click += new System.EventHandler(this.menuAsignarUbicacion_Click);
            // 
            // menuConsultaUA
            // 
            this.menuConsultaUA.Text = "ConsultaUA";
            this.menuConsultaUA.Click += new System.EventHandler(this.menuConsultaUA_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.MenuItems.Add(this.menuSalidaUA);
            this.menuItem4.Text = "Salida";
            // 
            // menuSalidaUA
            // 
            this.menuSalidaUA.Text = "SalidaUA";
            this.menuSalidaUA.Click += new System.EventHandler(this.menuSalidaUA_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.MenuItems.Add(this.menuDescargaManifiesto);
            this.menuItem5.MenuItems.Add(this.menuRecepcionGuias);
            this.menuItem5.Text = "Descarga Manifiesto";
            // 
            // menuDescargaManifiesto
            // 
            this.menuDescargaManifiesto.Text = "DescargaManifiesto";
            this.menuDescargaManifiesto.Click += new System.EventHandler(this.menuDescargaManifiesto_Click);
            // 
            // menuRecepcionGuias
            // 
            this.menuRecepcionGuias.Text = "RecepcionGuias";
            this.menuRecepcionGuias.Click += new System.EventHandler(this.menuRecepcionGuias_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Text = "Registrar Carga de Manifiesto";
            this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Text = "Pendiente Devolucion";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.MenuItems.Add(this.menuItem14);
            this.menuItem13.Text = "Movilizaciones";
            this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
            // 
            // menuItem14
            // 
            this.menuItem14.Text = "Consulta Pendiente";
            this.menuItem14.Click += new System.EventHandler(this.menuItem14_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.MenuItems.Add(this.menuItem9);
            this.menuItem8.MenuItems.Add(this.menuItem12);
            this.menuItem8.Text = "Eventos Expo";
            // 
            // menuItem9
            // 
            this.menuItem9.MenuItems.Add(this.menuItem10);
            this.menuItem9.MenuItems.Add(this.menuItem11);
            this.menuItem9.Text = "Ubicacion";
            // 
            // menuItem10
            // 
            this.menuItem10.Text = "Asignar Ubicar";
            this.menuItem10.Click += new System.EventHandler(this.menuItem10_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Text = "Consulta";
            // 
            // menuItem12
            // 
            this.menuItem12.Text = "Pendiente Devolucion";
            // 
            // btnEntrar
            // 
            this.btnEntrar.Location = new System.Drawing.Point(80, 163);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(72, 20);
            this.btnEntrar.TabIndex = 3;
            this.btnEntrar.Text = "Entrar";
            // 
            // PaginaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(label1);
            this.Menu = this.mainMenu1;
            this.Name = "PaginaPrincipal";
            this.Text = "PaginaPrincipal";
            this.Load += new System.EventHandler(this.PaginaPrincipal_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuAsignarUbicacion;
        private System.Windows.Forms.MenuItem menuSalidaUA;
        private System.Windows.Forms.MenuItem menuRecepcionManifiesto;
        private System.Windows.Forms.MenuItem menuConsultaUA;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem menuDescargaManifiesto;
        private System.Windows.Forms.MenuItem menuRecepcionGuias;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem menuItem12;
        private System.Windows.Forms.MenuItem menuItem13;
        private System.Windows.Forms.MenuItem menuItem14;
    }
}