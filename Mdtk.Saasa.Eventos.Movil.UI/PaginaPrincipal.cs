﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.UI;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._1LlegadaManifiesto;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._2DescargaManifiesto;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._6Salida;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._5Devolucion;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion;
using Mdtk.Saasa.Eventos.Movil.UI.Exportacion._1Ubicacion;

namespace Mdtk.Saasa.Eventos.Movil.UI
{
    public partial class PaginaPrincipal : Form
    {
        public PaginaPrincipal()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            RecepcionManifiesto frmFormularioRecepcionManifiesto = new RecepcionManifiesto();
            frmFormularioRecepcionManifiesto.Show();
        }

        private void menuDescargaManifiesto_Click(object sender, EventArgs e)
        {
            DescargaManifiesto descargaManifiesto = new DescargaManifiesto();
            descargaManifiesto.Show();
        }

        private void menuRecepcionGuias_Click(object sender, EventArgs e)
        {
            RecepcionGuias recepcionGuias = new RecepcionGuias();
            recepcionGuias.Show();
        }

        private void menuSalidaUA_Click(object sender, EventArgs e)
        {
            SalidaUA salidaUA = new SalidaUA();
            salidaUA.Show();
        }

        private void menuAsignarUbicacion_Click(object sender, EventArgs e)
        {
            AsignarUbicacionUA asignarUbicacion = new AsignarUbicacionUA();
            asignarUbicacion.Show();
        }

        private void menuConsultaUA_Click(object sender, EventArgs e)
        {
            ConsultaUA consultaUA = new ConsultaUA();
            consultaUA.Show();
        }

        private void menuRecepcionManifiesto_Click(object sender, EventArgs e)
        {
            RecepcionManifiesto recepcionManifiesto = new RecepcionManifiesto();
            recepcionManifiesto.Show();
        }

        private void PaginaPrincipal_Load(object sender, EventArgs e)
        {

        }
    
        private void menuItem6_Click(object sender, EventArgs e)
        {
            RegistrarCargadeManifiesto NuevaCarga = new RegistrarCargadeManifiesto();
            NuevaCarga.Show();
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            PendienteDevolucion PendienteDevolucion = new PendienteDevolucion();
            PendienteDevolucion.Show();
        }

        private void menuItem10_Click(object sender, EventArgs e)
        {
            AsignarUbicacion AsignarUbicacion = new AsignarUbicacion();
            AsignarUbicacion.Show();
        }

        private void menuItem13_Click(object sender, EventArgs e)
        {
            
        }

        private void menuItem14_Click(object sender, EventArgs e)
        {
            ConsultaPendienteMovilizacion Consulta = new ConsultaPendienteMovilizacion();
            Consulta.Show();
        }
    }
}