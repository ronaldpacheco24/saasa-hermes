﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._1Ubicacion
{
    partial class AsignarUbicacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ScanUA = new System.Windows.Forms.Button();
            this.ScanUbi = new System.Windows.Forms.Button();
            this.lblUbicaciones = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.lblBultos = new System.Windows.Forms.Label();
            this.btnUbicar = new System.Windows.Forms.Button();
            this.txtCodigoUA = new System.Windows.Forms.TextBox();
            this.txtUbicacionUA = new System.Windows.Forms.TextBox();
            this.btnAforo = new System.Windows.Forms.Button();
            this.btnZonaInterna = new System.Windows.Forms.Button();
            this.btnPreAlmacen = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            this.lblCodigoUA = new System.Windows.Forms.Label();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ScanUA
            // 
            this.ScanUA.Location = new System.Drawing.Point(42, 202);
            this.ScanUA.Name = "ScanUA";
            this.ScanUA.Size = new System.Drawing.Size(72, 20);
            this.ScanUA.TabIndex = 77;
            this.ScanUA.Text = "Scan UA";
            // 
            // ScanUbi
            // 
            this.ScanUbi.Location = new System.Drawing.Point(42, 165);
            this.ScanUbi.Name = "ScanUbi";
            this.ScanUbi.Size = new System.Drawing.Size(72, 20);
            this.ScanUbi.TabIndex = 76;
            this.ScanUbi.Text = "Scan Ubi";
            // 
            // lblUbicaciones
            // 
            this.lblUbicaciones.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUbicaciones.ForeColor = System.Drawing.Color.Red;
            this.lblUbicaciones.Location = new System.Drawing.Point(25, 66);
            this.lblUbicaciones.Name = "lblUbicaciones";
            this.lblUbicaciones.Size = new System.Drawing.Size(210, 20);
            this.lblUbicaciones.Text = " ";
            this.lblUbicaciones.Visible = false;
            // 
            // lblPeso
            // 
            this.lblPeso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPeso.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPeso.Location = new System.Drawing.Point(196, 40);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(39, 20);
            this.lblPeso.Text = " ";
            // 
            // lblBultos
            // 
            this.lblBultos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblBultos.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblBultos.Location = new System.Drawing.Point(153, 40);
            this.lblBultos.Name = "lblBultos";
            this.lblBultos.Size = new System.Drawing.Size(39, 20);
            this.lblBultos.Text = " ";
            // 
            // btnUbicar
            // 
            this.btnUbicar.Location = new System.Drawing.Point(153, 177);
            this.btnUbicar.Name = "btnUbicar";
            this.btnUbicar.Size = new System.Drawing.Size(72, 20);
            this.btnUbicar.TabIndex = 75;
            this.btnUbicar.Text = "Ubicar";
            // 
            // txtCodigoUA
            // 
            this.txtCodigoUA.Location = new System.Drawing.Point(42, 38);
            this.txtCodigoUA.Name = "txtCodigoUA";
            this.txtCodigoUA.Size = new System.Drawing.Size(100, 21);
            this.txtCodigoUA.TabIndex = 74;
            // 
            // txtUbicacionUA
            // 
            this.txtUbicacionUA.Location = new System.Drawing.Point(43, 10);
            this.txtUbicacionUA.Name = "txtUbicacionUA";
            this.txtUbicacionUA.Size = new System.Drawing.Size(100, 21);
            this.txtUbicacionUA.TabIndex = 73;
            // 
            // btnAforo
            // 
            this.btnAforo.Location = new System.Drawing.Point(120, 115);
            this.btnAforo.Name = "btnAforo";
            this.btnAforo.Size = new System.Drawing.Size(72, 20);
            this.btnAforo.TabIndex = 72;
            this.btnAforo.Text = "AFORO";
            // 
            // btnZonaInterna
            // 
            this.btnZonaInterna.Location = new System.Drawing.Point(42, 115);
            this.btnZonaInterna.Name = "btnZonaInterna";
            this.btnZonaInterna.Size = new System.Drawing.Size(72, 20);
            this.btnZonaInterna.TabIndex = 71;
            this.btnZonaInterna.Text = "ZON-I";
            // 
            // btnPreAlmacen
            // 
            this.btnPreAlmacen.Location = new System.Drawing.Point(120, 89);
            this.btnPreAlmacen.Name = "btnPreAlmacen";
            this.btnPreAlmacen.Size = new System.Drawing.Size(72, 20);
            this.btnPreAlmacen.TabIndex = 70;
            this.btnPreAlmacen.Text = "PRE-A";
            // 
            // btnManual
            // 
            this.btnManual.Location = new System.Drawing.Point(42, 89);
            this.btnManual.Name = "btnManual";
            this.btnManual.Size = new System.Drawing.Size(72, 20);
            this.btnManual.TabIndex = 69;
            this.btnManual.Text = "Manual";
            // 
            // lblCodigoUA
            // 
            this.lblCodigoUA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblCodigoUA.Location = new System.Drawing.Point(6, 42);
            this.lblCodigoUA.Name = "lblCodigoUA";
            this.lblCodigoUA.Size = new System.Drawing.Size(30, 20);
            this.lblCodigoUA.Text = "UA";
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUbicacion.Location = new System.Drawing.Point(6, 12);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(30, 20);
            this.lblUbicacion.Text = "Ub.";
            // 
            // AsignarUbicacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.ScanUA);
            this.Controls.Add(this.ScanUbi);
            this.Controls.Add(this.lblUbicaciones);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.lblBultos);
            this.Controls.Add(this.btnUbicar);
            this.Controls.Add(this.txtCodigoUA);
            this.Controls.Add(this.txtUbicacionUA);
            this.Controls.Add(this.btnAforo);
            this.Controls.Add(this.btnZonaInterna);
            this.Controls.Add(this.btnPreAlmacen);
            this.Controls.Add(this.btnManual);
            this.Controls.Add(this.lblCodigoUA);
            this.Controls.Add(this.lblUbicacion);
            this.Menu = this.mainMenu1;
            this.Name = "AsignarUbicacion";
            this.Text = "AsignarUbicacion";
            this.Load += new System.EventHandler(this.AsignarUbicacion_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ScanUA;
        private System.Windows.Forms.Button ScanUbi;
        private System.Windows.Forms.Label lblUbicaciones;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblBultos;
        private System.Windows.Forms.Button btnUbicar;
        private System.Windows.Forms.TextBox txtCodigoUA;
        private System.Windows.Forms.TextBox txtUbicacionUA;
        private System.Windows.Forms.Button btnAforo;
        private System.Windows.Forms.Button btnZonaInterna;
        private System.Windows.Forms.Button btnPreAlmacen;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Label lblCodigoUA;
        private System.Windows.Forms.Label lblUbicacion;
    }
}