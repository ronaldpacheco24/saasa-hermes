﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._1Ubicacion
{
    public partial class AsignarUbicacion : Form
    {
        public AsignarUbicacion()
        {
            InitializeComponent();
        }
        private String Ubicacion = "";
        public AsignarUbicacion(string Ubi)
        {
            InitializeComponent();
            Ubicacion = Ubi;
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            AsignarManual AsignarManual = new AsignarManual();
            AsignarManual.Show();
        }

        private void btnPreAlmacen_Click(object sender, EventArgs e)
        {
            txtUbicacionUA.Text = "PRE-ALMACENAJE";
        }

        private void btnZonaInterna_Click(object sender, EventArgs e)
        {
            txtUbicacionUA.Text = "ZONA INTERMEDIA";
        }

        private void btnAforo_Click(object sender, EventArgs e)
        {
            txtUbicacionUA.Text = "ZONA AFORO I";
        }

        private void AsignarUbicacionUA_Load(object sender, EventArgs e)
        {
            txtUbicacionUA.Text = Ubicacion;
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            PaginaPrincipal Menu = new PaginaPrincipal();
            Menu.Show();
        }

        private void btnUbicar_Click(object sender, EventArgs e)
        {
            UnidadAlmacenamientoBE UnidadAlmacenamiento = new UnidadAlmacenamientoBE();
            UnidadAlmacenamiento = UnidadAlmacenamientoBL.Instancia.BuscarUnidadAlmacenamientoCodigo("UA1");

            string UbicacionDestino = UnidadAlmacenamiento.UbicacionDestino;
            string Ubicacion = txtUbicacionUA.Text;

            if (UbicacionDestino.Equals(Ubicacion))
            {
                MessageBox.Show("Se Ubico Correctamente");
                DateTime FechaActual = DateTime.Today;
                UnidadAlmacenamiento.FechaUbicacion = FechaActual;
                UnidadAlmacenamientoBL.Instancia.ActualizarUnidadAlmacenamiento(UnidadAlmacenamiento);
            }
            else
            {
                DialogResult dr = MessageBox.Show("La ubicación final no corresponde a la ubicación requerida. Confirmar", "Ubicar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                if (dr == DialogResult.Yes)
                {
                    LoginAutorizacion login = new LoginAutorizacion();
                    login.Show();
                }
            }
        }

        private void ScanUA_Click(object sender, EventArgs e)
        {
            UnidadAlmacenamientoBE UnidadAlmacenamiento = new UnidadAlmacenamientoBE();
            UnidadAlmacenamiento = UnidadAlmacenamientoBL.Instancia.BuscarUnidadAlmacenamientoCodigo("UA1");

            string UbicacionActual = UnidadAlmacenamiento.UbicacionActual;
            string UbicacionAsignada = UnidadAlmacenamiento.UbicacionAsignada;
            string UbicacionDestino = UnidadAlmacenamiento.UbicacionDestino;

            txtCodigoUA.Text = UnidadAlmacenamiento.CodigoUA;
            lblBultos.Text = Convert.ToString(UnidadAlmacenamiento.Bultos) + "b";
            lblPeso.Text = Convert.ToString(UnidadAlmacenamiento.Peso) + "k";

            if (UbicacionActual.Length >= 10)
            {
                UbicacionActual = UbicacionActual.Substring(0, 10);
            }
            if (UbicacionAsignada.Length >= 10)
            {
                UbicacionAsignada = UbicacionAsignada.Substring(0, 10);
            }
            if (UbicacionDestino.Length >= 10)
            {
                UbicacionDestino = UbicacionDestino.Substring(0, 10);
            }

            lblUbicaciones.Text = UbicacionActual + " / " + UbicacionAsignada + " / " + UbicacionDestino;
            lblUbicaciones.Show();
        }

        private void ScanUbi_Click(object sender, EventArgs e)
        {
            UbicacionBE Ubicacion = new UbicacionBE();
            Ubicacion = UbicacionBL.Instancia.BuscarUbicacion("");

            txtUbicacionUA.Text = Ubicacion.CodigoUbicacion;
        }

        private void AsignarUbicacion_Load(object sender, EventArgs e)
        {

        }
    }
}