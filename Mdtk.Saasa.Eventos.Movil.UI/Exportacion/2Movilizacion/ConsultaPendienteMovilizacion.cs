﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._2Movilizacion
{
    public partial class ConsultaPendienteMovilizacion : Form
    {
        public ConsultaPendienteMovilizacion()
        {
            InitializeComponent();
        }

        public int ObtenerMinutos(DateTime FechaInicial, DateTime FechaFinal)
        {
            int Mes, Dia, Hora, Minuto;

            Mes = FechaFinal.Month - FechaInicial.Month;
            Dia = FechaFinal.Day - FechaInicial.Day;
            Hora = FechaFinal.Hour - FechaInicial.Hour;
            Minuto = FechaFinal.Minute - FechaInicial.Minute;

            int MesFinal = FechaFinal.Month;
            int MesInicial = FechaInicial.Month;
            int ObtenerDiasMeses = 0;
            int DiasxMeses = 0;
            for (int i = 0; i < Mes; i++)
            {
                if (MesFinal > MesInicial)
                {
                    ObtenerDiasMeses = DateTime.DaysInMonth(FechaInicial.Year, FechaInicial.Month);
                    DiasxMeses = ObtenerDiasMeses + DiasxMeses;
                    MesInicial++;
                }
            }
            int DiaxHora = 0;
            int HoraxMinuto = 0;
            Dia = DiasxMeses + Dia;
            DiaxHora = (Dia * 24);
            HoraxMinuto = DiaxHora + Hora;
            HoraxMinuto = HoraxMinuto * 60;
            Minuto = HoraxMinuto + Minuto;

            return Minuto;
        }

        public List<DataGridPendienteMovilizacionExpoBE> CargarDatos()
        {
            List<DataGridPendienteMovilizacionExpoBE> DataGridList = new List<DataGridPendienteMovilizacionExpoBE>();

            List<WareHouseBE> WareHouseList = new List<WareHouseBE>();
            WareHouseBE WH = new WareHouseBE();
            WareHouseList = WareHouseBL.Instancia.BuscarWareHouse(WH);
            DateTime FechaActual = DateTime.Now;
            DateTime FechaUbicacion = new DateTime();
            foreach (var item in WareHouseList)
            {
                DataGridPendienteMovilizacionExpoBE DataGrid = new DataGridPendienteMovilizacionExpoBE();
                DataGrid.WareHouse = item.NumWareHouse;
                int countUA = 0;
                int bultos = 0;
                decimal PesoTotal = 0;
                foreach (var itemUA in item.ListUnidadAlmacenamiento)
                {
                    bultos = itemUA.Bultos + bultos;
                    PesoTotal = itemUA.Peso + PesoTotal;
                    countUA++;
                    DataGrid.UbicacionFinal = itemUA.UbicacionDestino;
                    FechaUbicacion = itemUA.FechaUbicacion;
                }
                DataGrid.TMin = ObtenerMinutos(FechaUbicacion, FechaActual);
                DataGrid.UAs = Convert.ToString(countUA);
                DataGrid.Bultos = bultos;
                DataGrid.Peso = PesoTotal;

                DataGridList.Add(DataGrid);
            }
            return DataGridList;
        }

        public void CargarDataGrid()
        {
            dgPendienteMovilizacion.DataSource = CargarDatos();
            int alto = CargarDatos().Count;
            dgPendienteMovilizacion.Height = (alto * 18) + 21;
            dgPendienteMovilizacion.Width = 330;
            btnTomar.Location = new Point(255, dgPendienteMovilizacion.Height + dgPendienteMovilizacion.Location.Y + 10);
        }

        private void btnTomar_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteMovilizacion.CurrentCell.RowNumber;
            List<DataGridPendienteMovilizacionExpoBE> DGPM = new List<DataGridPendienteMovilizacionExpoBE>();
            DGPM = CargarDatos();
            int count = 0;

            foreach (var item in DGPM)
            {
                if (count == fila)
                {
                    UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();
                    UA.CodigoUA = item.UAs;
                    UnidadAlmacenamientoBL.Instancia.ActualizarUnidadAlmacenamiento(UA);
                }
                count++;
            }
        }

        private void dgPendienteMovilizacion_CurrentCellChanged(object sender, EventArgs e)
        {
            dgPendienteMovilizacion.Select(dgPendienteMovilizacion.CurrentRowIndex);
        }

        private void ConsultaPendienteMovilizacion_Load(object sender, EventArgs e)
        {
            CargarDataGrid();
        }

    }
}