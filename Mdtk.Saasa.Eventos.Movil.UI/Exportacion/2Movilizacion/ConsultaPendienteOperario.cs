﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._2Movilizacion
{
    public partial class ConsultaPendienteOperario : Form
    {
        public ConsultaPendienteOperario()
        {
            InitializeComponent();
        }

        public int ObtenerMinutos(DateTime FechaInicial, DateTime FechaFinal)
        {
            int Mes, Dia, Hora, Minuto;

            Mes = FechaFinal.Month - FechaInicial.Month;
            Dia = FechaFinal.Day - FechaInicial.Day;
            Hora = FechaFinal.Hour - FechaInicial.Hour;
            Minuto = FechaFinal.Minute - FechaInicial.Minute;

            int MesFinal = FechaFinal.Month;
            int MesInicial = FechaInicial.Month;
            int ObtenerDiasMeses = 0;
            int DiasxMeses = 0;
            for (int i = 0; i < Mes; i++)
            {
                if (MesFinal > MesInicial)
                {
                    ObtenerDiasMeses = DateTime.DaysInMonth(FechaInicial.Year, FechaInicial.Month);
                    DiasxMeses = ObtenerDiasMeses + DiasxMeses;
                    MesInicial++;
                }
            }
            int DiaxHora = 0;
            int HoraxMinuto = 0;
            Dia = DiasxMeses + Dia;
            DiaxHora = (Dia * 24);
            HoraxMinuto = DiaxHora + Hora;
            HoraxMinuto = HoraxMinuto * 60;
            Minuto = HoraxMinuto + Minuto;

            return Minuto;
        }

        public List<DataGridPendienteOperarioExpoBE> CargarDatos()
        {
            List<DataGridPendienteOperarioExpoBE> DataGridList = new List<DataGridPendienteOperarioExpoBE>();

            List<WareHouseBE> WareHouseList = new List<WareHouseBE>();
            WareHouseBE WH = new WareHouseBE();
            WareHouseList = WareHouseBL.Instancia.BuscarWareHouse(WH);
            DateTime FechaActual = DateTime.Now;
            DateTime FechaUbicacion = new DateTime();
            foreach (var item in WareHouseList)
            {
                DataGridPendienteOperarioExpoBE DataGrid = new DataGridPendienteOperarioExpoBE();
                DataGrid.WareHouse = item.NumWareHouse;
                int countUA = 0;
                int bultos = 0;
                decimal PesoTotal = 0;
                foreach (var itemUA in item.ListUnidadAlmacenamiento)
                {
                    bultos = itemUA.Bultos + bultos;
                    PesoTotal = itemUA.Peso + PesoTotal;
                    countUA++;
                    DataGrid.UbicacionFinal = itemUA.UbicacionDestino;
                    FechaUbicacion = itemUA.FechaUbicacion;
                }
                DataGrid.TMin = ObtenerMinutos(FechaUbicacion, FechaActual);
                DataGrid.UAs = Convert.ToString(countUA);
                DataGrid.Bts = bultos;
                DataGrid.Peso = PesoTotal;

                DataGrid.UA_C = 3;
                DataGrid.UA_P = 3;

                DataGridList.Add(DataGrid);
            }
            return DataGridList;
        }

        public void CargarDataGrid()
        {
            dgPendienteOperario.DataSource = CargarDatos();
            int alto = CargarDatos().Count;
            dgPendienteOperario.Height = (alto * 18) + 21;
            dgPendienteOperario.Width = 430;
            btnEnviar.Location = new Point(355, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);
            lblMotivo.Location = new Point(51, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);
            cbMotivo.Location = new Point(152, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);

            btnDetalle.Location = new Point(261, cbMotivo.Height + cbMotivo.Location.Y + 20);
            btnDevolver.Location = new Point(152, cbMotivo.Height + cbMotivo.Location.Y + 20);
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteOperario.CurrentCell.RowNumber;
            List<DataGridPendienteOperarioExpoBE> DGPO = new List<DataGridPendienteOperarioExpoBE>();
            DGPO = CargarDatos();
            int count = 0;
            string NumWareHouse = "";
            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    NumWareHouse = item.WareHouse;
                }
                count++;
            }
            ConsultaDetalleGuia Consulta = new ConsultaDetalleGuia(NumWareHouse);
            Consulta.Show();
        }

        private void btnDevolver_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteOperario.CurrentCell.RowNumber;
            List<DataGridPendienteOperarioExpoBE> DGPO = new List<DataGridPendienteOperarioExpoBE>();
            DGPO = CargarDatos();
            int count = 0;

            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();
                    UA.CodigoUA = item.UAs;
                    UnidadAlmacenamientoBL.Instancia.ActualizarUnidadAlmacenamiento(UA);

                }
                count++;
            }
        }

        private void dgPendienteOperario_CurrentCellChanged(object sender, EventArgs e)
        {
            dgPendienteOperario.Select(dgPendienteOperario.CurrentRowIndex);
        }

        private void ConsultaPendienteOperario_Load(object sender, EventArgs e)
        {
            CargarDataGrid();
        }
    }
}