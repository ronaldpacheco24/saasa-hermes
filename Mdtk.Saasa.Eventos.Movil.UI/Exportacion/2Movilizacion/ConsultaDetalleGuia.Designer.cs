﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._2Movilizacion
{
    partial class ConsultaDetalleGuia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnUbicar = new System.Windows.Forms.Button();
            this.dgDetalleGuia = new System.Windows.Forms.DataGrid();
            this.txtUbicacionFinal = new System.Windows.Forms.TextBox();
            this.lblUbiFinal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUbicar
            // 
            this.btnUbicar.Location = new System.Drawing.Point(155, 148);
            this.btnUbicar.Name = "btnUbicar";
            this.btnUbicar.Size = new System.Drawing.Size(72, 20);
            this.btnUbicar.TabIndex = 7;
            this.btnUbicar.Text = "Ubicar";
            // 
            // dgDetalleGuia
            // 
            this.dgDetalleGuia.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgDetalleGuia.Location = new System.Drawing.Point(3, 39);
            this.dgDetalleGuia.Name = "dgDetalleGuia";
            this.dgDetalleGuia.Size = new System.Drawing.Size(234, 93);
            this.dgDetalleGuia.TabIndex = 6;
            // 
            // txtUbicacionFinal
            // 
            this.txtUbicacionFinal.Location = new System.Drawing.Point(109, 12);
            this.txtUbicacionFinal.Name = "txtUbicacionFinal";
            this.txtUbicacionFinal.Size = new System.Drawing.Size(128, 21);
            this.txtUbicacionFinal.TabIndex = 5;
            // 
            // lblUbiFinal
            // 
            this.lblUbiFinal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUbiFinal.Location = new System.Drawing.Point(3, 13);
            this.lblUbiFinal.Name = "lblUbiFinal";
            this.lblUbiFinal.Size = new System.Drawing.Size(100, 20);
            this.lblUbiFinal.Text = "Ubicación Final";
            // 
            // ConsultaDetalleGuia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnUbicar);
            this.Controls.Add(this.dgDetalleGuia);
            this.Controls.Add(this.txtUbicacionFinal);
            this.Controls.Add(this.lblUbiFinal);
            this.Menu = this.mainMenu1;
            this.Name = "ConsultaDetalleGuia";
            this.Text = "ConsultaDetalleGuia";
            this.Load += new System.EventHandler(this.ConsultaDetalleGuia_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUbicar;
        private System.Windows.Forms.DataGrid dgDetalleGuia;
        private System.Windows.Forms.TextBox txtUbicacionFinal;
        private System.Windows.Forms.Label lblUbiFinal;
    }
}