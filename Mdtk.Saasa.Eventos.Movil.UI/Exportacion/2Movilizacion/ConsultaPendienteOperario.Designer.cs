﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Exportacion._2Movilizacion
{
    partial class ConsultaPendienteOperario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnDetalle = new System.Windows.Forms.Button();
            this.btnDevolver = new System.Windows.Forms.Button();
            this.cbMotivo = new System.Windows.Forms.ComboBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.dgPendienteOperario = new System.Windows.Forms.DataGrid();
            this.lblMotivo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDetalle
            // 
            this.btnDetalle.Location = new System.Drawing.Point(261, 154);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(88, 20);
            this.btnDetalle.TabIndex = 12;
            this.btnDetalle.Text = "Ir a Detalle";
            // 
            // btnDevolver
            // 
            this.btnDevolver.Location = new System.Drawing.Point(152, 154);
            this.btnDevolver.Name = "btnDevolver";
            this.btnDevolver.Size = new System.Drawing.Size(72, 20);
            this.btnDevolver.TabIndex = 11;
            this.btnDevolver.Text = "Devolver";
            // 
            // cbMotivo
            // 
            this.cbMotivo.Location = new System.Drawing.Point(152, 106);
            this.cbMotivo.Name = "cbMotivo";
            this.cbMotivo.Size = new System.Drawing.Size(197, 22);
            this.cbMotivo.TabIndex = 10;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(355, 106);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(72, 20);
            this.btnEnviar.TabIndex = 9;
            this.btnEnviar.Text = "Tomar";
            // 
            // dgPendienteOperario
            // 
            this.dgPendienteOperario.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgPendienteOperario.Location = new System.Drawing.Point(0, 36);
            this.dgPendienteOperario.Name = "dgPendienteOperario";
            this.dgPendienteOperario.Size = new System.Drawing.Size(430, 53);
            this.dgPendienteOperario.TabIndex = 8;
            // 
            // lblMotivo
            // 
            this.lblMotivo.Location = new System.Drawing.Point(37, 108);
            this.lblMotivo.Name = "lblMotivo";
            this.lblMotivo.Size = new System.Drawing.Size(95, 20);
            this.lblMotivo.Text = "Motivo Demora";
            // 
            // ConsultaPendienteOperario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblMotivo);
            this.Controls.Add(this.btnDetalle);
            this.Controls.Add(this.btnDevolver);
            this.Controls.Add(this.cbMotivo);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.dgPendienteOperario);
            this.Menu = this.mainMenu1;
            this.Name = "ConsultaPendienteOperario";
            this.Text = "ConsultaPendienteOperario";
            this.Load += new System.EventHandler(this.ConsultaPendienteOperario_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDetalle;
        private System.Windows.Forms.Button btnDevolver;
        private System.Windows.Forms.ComboBox cbMotivo;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.DataGrid dgPendienteOperario;
        private System.Windows.Forms.Label lblMotivo;
    }
}