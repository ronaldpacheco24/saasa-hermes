﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    public partial class RegistrarConcamos : Form
    {

        ManifiestoBE ManifiestoActual = new ManifiestoBE();
        List<UldBE> ListaUlds = new List<UldBE>();

        public RegistrarConcamos()
        {
            InitializeComponent();
        }
        public RegistrarConcamos(ManifiestoBE objmanifiesto)
        {
            InitializeComponent(); 
            ManifiestoActual = objmanifiesto;

            foreach (var item in ManifiestoActual.ListaUlds)
            {

                //Agregar a la clase
                UldBE NuevoUld = new UldBE();
                if (NuevoUld.Tipo == 'M')
                {
                    txtconcamo.Text = Convert.ToString(item.Cantidad);
                }
                else
                {
                    NuevoUld.Tipo = item.Tipo;
                    NuevoUld.Codigo = item.Codigo;
                    NuevoUld.Cantidad = item.Cantidad;
                    NuevoUld.EstadoUld = item.EstadoUld;
                    ListaUlds.Add(NuevoUld);
                }
            }
        }
        private void menuItem1_Click(object sender, EventArgs e)
        {
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto();
            RegistroCarga.Show();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            UldBE NuevoUld = new UldBE();
            NuevoUld.Tipo = 'C';
            NuevoUld.Cantidad = Convert.ToInt32(txtconcamo.Text);
            ListaUlds.Add(NuevoUld);

            ManifiestoActual.ListaUlds = ListaUlds;
            
            //ManifiestoActual.Concamos = Int32.Parse(txtconcamo.Text);
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }
    }
}