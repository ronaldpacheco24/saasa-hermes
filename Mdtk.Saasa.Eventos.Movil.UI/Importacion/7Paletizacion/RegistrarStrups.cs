﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    public partial class RegistrarStrups : Form
    {
        ManifiestoBE ManifiestoActual = new ManifiestoBE();
        List<UldBE> ListaUlds = new List<UldBE>();
        
        public RegistrarStrups()
        {
            InitializeComponent();
        }
        public RegistrarStrups(ManifiestoBE objmanifiesto)
        {
            InitializeComponent();
            ManifiestoActual = objmanifiesto;

            foreach (var item in ManifiestoActual.ListaUlds)
            {

                UldBE NuevoUld = new UldBE();
                //Agregar a la clase
                if (NuevoUld.Tipo == 'S')
                {
                    txtStrups.Text = Convert.ToString(item.Cantidad);
                }
                else
                {

                    NuevoUld.Tipo = item.Tipo;
                    NuevoUld.Codigo = item.Codigo;
                    NuevoUld.Cantidad = item.Cantidad;
                    NuevoUld.EstadoUld = item.EstadoUld;
                    ListaUlds.Add(NuevoUld);
                }
            }
        }
        

        private void btnGrabar_Click(object sender, EventArgs e)
        {

        }

        private void btnGrabar_Click_1(object sender, EventArgs e)
        {
            UldBE NuevoUld = new UldBE();
            NuevoUld.Tipo = 'S';
            NuevoUld.Cantidad = Convert.ToInt32(txtStrups.Text);
            ListaUlds.Add(NuevoUld);

            ManifiestoActual.ListaUlds = ListaUlds;
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }
    }
}