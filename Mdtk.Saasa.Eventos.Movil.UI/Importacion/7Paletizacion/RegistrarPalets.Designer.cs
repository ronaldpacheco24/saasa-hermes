﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    partial class RegistrarPalets
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.txtEstado = new System.Windows.Forms.ComboBox();
            this.lvConteiners = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAgregarPalet = new System.Windows.Forms.Button();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Regresar";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // txtEstado
            // 
            this.txtEstado.Items.Add("Bueno");
            this.txtEstado.Items.Add("Malo");
            this.txtEstado.Location = new System.Drawing.Point(62, 51);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(124, 22);
            this.txtEstado.TabIndex = 17;
            // 
            // lvConteiners
            // 
            this.lvConteiners.Location = new System.Drawing.Point(62, 82);
            this.lvConteiners.Name = "lvConteiners";
            this.lvConteiners.Size = new System.Drawing.Size(124, 142);
            this.lvConteiners.TabIndex = 16;
            this.lvConteiners.View = System.Windows.Forms.View.List;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.Text = "Estado";
            // 
            // btnAgregarPalet
            // 
            this.btnAgregarPalet.Location = new System.Drawing.Point(195, 20);
            this.btnAgregarPalet.Name = "btnAgregarPalet";
            this.btnAgregarPalet.Size = new System.Drawing.Size(31, 22);
            this.btnAgregarPalet.TabIndex = 15;
            this.btnAgregarPalet.Text = " +";
            this.btnAgregarPalet.Click += new System.EventHandler(this.btnAgregarPalet_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(62, 20);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(124, 21);
            this.txtCodigo.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 20);
            this.label1.Text = "Codigo";
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(129, 239);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(97, 26);
            this.btnGrabar.TabIndex = 20;
            this.btnGrabar.Text = "Grabar";
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click_1);
            // 
            // RegistrarPalets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.lvConteiners);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAgregarPalet);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "RegistrarPalets";
            this.Text = "RegistrarPalets";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox txtEstado;
        private System.Windows.Forms.ListView lvConteiners;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAgregarPalet;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Button btnGrabar;
    }
}