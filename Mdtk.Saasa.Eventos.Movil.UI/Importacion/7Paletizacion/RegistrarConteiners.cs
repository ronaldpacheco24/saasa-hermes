﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    public partial class RegistrarConteiners : Form
    {
        ManifiestoBE ManifiestoActual = new ManifiestoBE();
        List<UldBE> ListaUlds = new List<UldBE>();
        
        public RegistrarConteiners()
        {
            InitializeComponent();
            txtEstado.Text = "Bueno";
        }

        public RegistrarConteiners(ManifiestoBE objmanifiesto)
        {
            InitializeComponent();
            txtEstado.Text = "Bueno";
            ManifiestoActual = objmanifiesto;

            foreach (var item in ManifiestoActual.ListaUlds)
            {
                ListViewItem NuevoItems = new ListViewItem();
                if (item.Tipo == 'N')
                {
                    NuevoItems.Text = item.Codigo;
                    lvConteiners.Items.Add(NuevoItems);
                }
                //Agregar a la clase
                UldBE NuevoUld = new UldBE();
                NuevoUld.Tipo = item.Tipo;
                NuevoUld.Codigo = item.Codigo;
                NuevoUld.Cantidad = item.Cantidad;
                NuevoUld.EstadoUld = item.EstadoUld;
                ListaUlds.Add(NuevoUld);
            }
        }

        private void btnAgregarConteiner_Click(object sender, EventArgs e)
        {
            ListViewItem NuevoItems = new ListViewItem();
            NuevoItems.Text = txtCodigo.Text;
            lvConteiners.Items.Add(NuevoItems);
            UldBE NuevoUld = new UldBE();
            NuevoUld.Tipo = 'N';
            NuevoUld.Codigo = Convert.ToString(txtCodigo.Text);
            NuevoUld.EstadoUld = txtEstado.Text;
            ListaUlds.Add(NuevoUld);
            txtCodigo.Text = "";
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            ManifiestoActual.ListaUlds = ListaUlds;
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }
    }
}