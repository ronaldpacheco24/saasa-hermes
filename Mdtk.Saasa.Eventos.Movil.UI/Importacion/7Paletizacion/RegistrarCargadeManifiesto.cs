﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    public partial class RegistrarCargadeManifiesto : Form
    {
        ManifiestoBE ManifiestoActual = new ManifiestoBE();
        List<UldBE> ListaUlds = new List<UldBE>();
        List<PaletBE> ListaPalets = new List<PaletBE>();
        
        public RegistrarCargadeManifiesto()
        {
            InitializeComponent();
            ManifiestoActual.ListaUlds = ListaUlds;
        }

        public RegistrarCargadeManifiesto(ManifiestoBE objmanifiesto)
        {
            InitializeComponent();
            ManifiestoActual = objmanifiesto;

            txtManifiesto.Text = ManifiestoActual.NumeroManifiesto;
            int NumeroConteiners = 0;
            int NumeroPalets = 0;
            int NumeroMayas = 0;
            int NumeroConcamos = 0;
            int NumeroStrups = 0;

           foreach (var item in ManifiestoActual.ListaUlds){
                if (item.Tipo == 'M'){
                    NumeroMayas = item.Cantidad;
                }
                if (item.Tipo == 'C')
                {
                    NumeroConcamos =  item.Cantidad;
                }
                if (item.Tipo == 'P')
                {
                    NumeroPalets++;
                } 
                if (item.Tipo == 'N')
                {
                    NumeroConteiners++;
                } 
                if (item.Tipo == 'S')
                {
                    NumeroStrups = item.Cantidad;
                }


            }
            lblmayas.Text = Convert.ToString(NumeroMayas);
            lblconcamos.Text = Convert.ToString(NumeroConcamos);
            lblcontainers.Text = Convert.ToString(NumeroConteiners);
            lblpalets.Text = Convert.ToString(NumeroPalets);
            lblstrups.Text = Convert.ToString(NumeroStrups);

        }

        private void btnMayas_Click(object sender, EventArgs e)
        {
            ManifiestoActual.NumeroManifiesto = txtManifiesto.Text;
            RegistrarMayas NuevoRegistro = new RegistrarMayas(ManifiestoActual);
            NuevoRegistro.Show();
        }

        private void btnConcamos_Click(object sender, EventArgs e)
        {
            ManifiestoActual.NumeroManifiesto = txtManifiesto.Text;
            RegistrarConcamos NuevoConcamo = new RegistrarConcamos(ManifiestoActual);
            NuevoConcamo.Show();
        }

        private void btnContainers_Click(object sender, EventArgs e)
        {
            ManifiestoActual.NumeroManifiesto = txtManifiesto.Text;
            RegistrarConteiners NuevoConteiner = new RegistrarConteiners(ManifiestoActual);
            NuevoConteiner.Show();
        }

        private void btnPalets_Click(object sender, EventArgs e)
        {
            ManifiestoActual.NumeroManifiesto = txtManifiesto.Text;
            RegistrarPalets NuevoPalet = new RegistrarPalets(ManifiestoActual);
            NuevoPalet.Show();
        }

        private void label2_ParentChanged(object sender, EventArgs e)
        {

        }

        private void RegistrarCargadeManifiesto_Load(object sender, EventArgs e)
        {

        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            //Registramos al servicio 
            var manifiesto = ManifiestoBL.Instancia.RegistrarCargaDeManifiesto(ManifiestoActual); 
        }

        private void btnStrups_Click(object sender, EventArgs e)
        {
            ManifiestoActual.NumeroManifiesto = txtManifiesto.Text;
            RegistrarStrups NuevoPalet = new RegistrarStrups(ManifiestoActual);
            NuevoPalet.Show();
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {
            ManifiestoActual = ManifiestoBL.Instancia.ObtenerCargaDeManifiesto(txtManifiesto.Text);
        }
    }
}