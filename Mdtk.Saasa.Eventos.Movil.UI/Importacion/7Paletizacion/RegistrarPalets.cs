﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    public partial class RegistrarPalets : Form
    {
        ManifiestoBE ManifiestoActual = new ManifiestoBE();
        List<UldBE> ListaUld = new List<UldBE>();

        public RegistrarPalets()
        {
            InitializeComponent();
            txtEstado.Text = "Bueno";
        }
        public RegistrarPalets(ManifiestoBE objmanifiesto)
        {
            InitializeComponent();
            txtEstado.Text = "Bueno";
            ManifiestoActual = objmanifiesto;

            foreach (var item in ManifiestoActual.ListaUlds)
            {
                ListViewItem NuevoItems = new ListViewItem();
                if (item.Tipo == 'P')
                {
                    NuevoItems.Text = item.Codigo;
                    lvConteiners.Items.Add(NuevoItems);
                }
                //Agregar a la clase
                UldBE NuevoUld = new UldBE();
                NuevoUld.Tipo = item.Tipo;
                NuevoUld.Codigo = item.Codigo;
                NuevoUld.EstadoUld = item.EstadoUld;
                NuevoUld.Cantidad = item.Cantidad;
                ListaUld.Add(NuevoUld);
                //NuevoPalet.CodigoPalet = item.Codigo;
                //NuevoPalet.EstadoPalet = item.EstadoUld;
                //ListaPalets.Add(NuevoPalet);
            }

        }
        private void btnAgregarConteiner_Click(object sender, EventArgs e)
        {
            
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }

        private void btnAgregarConteiner_Click_1(object sender, EventArgs e)
        {
            
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {

        }

        private void btnGrabar_Click_1(object sender, EventArgs e)
        {
            ManifiestoActual.ListaUlds = ListaUld;
            RegistrarCargadeManifiesto RegistroCarga = new RegistrarCargadeManifiesto(ManifiestoActual);
            RegistroCarga.Show();
        }

        private void btnAgregarPalet_Click(object sender, EventArgs e)
        {
            ListViewItem NuevoItems = new ListViewItem();
            NuevoItems.Text = txtCodigo.Text;
            lvConteiners.Items.Add(NuevoItems);
            //agregar a la clase
            UldBE NuevoUld = new UldBE();
            NuevoUld.Tipo = 'P';
            NuevoUld.Codigo = txtCodigo.Text;
            NuevoUld.EstadoUld = txtEstado.Text;

            ListaUld.Add(NuevoUld);
            txtCodigo.Text = "";
        }
    }
}