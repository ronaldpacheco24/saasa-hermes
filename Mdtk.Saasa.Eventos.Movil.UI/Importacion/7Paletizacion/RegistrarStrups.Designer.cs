﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    partial class RegistrarStrups
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.txtStrups = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(142, 210);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(80, 26);
            this.btnGrabar.TabIndex = 5;
            this.btnGrabar.Text = "Grabar";
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click_1);
            // 
            // txtStrups
            // 
            this.txtStrups.Location = new System.Drawing.Point(103, 32);
            this.txtStrups.Name = "txtStrups";
            this.txtStrups.Size = new System.Drawing.Size(119, 21);
            this.txtStrups.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(19, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.Text = "N. Strups";
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Regresar";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // RegistrarStrups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.txtStrups);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "RegistrarStrups";
            this.Text = "RegistrarStrups";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.TextBox txtStrups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuItem menuItem1;
    }
}