﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._7Paletizacion
{
    partial class RegistrarCargadeManifiesto
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnMayas = new System.Windows.Forms.Button();
            this.lblManifiesto = new System.Windows.Forms.Label();
            this.txtManifiesto = new System.Windows.Forms.TextBox();
            this.btnConcamos = new System.Windows.Forms.Button();
            this.btnContainers = new System.Windows.Forms.Button();
            this.btnPalets = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblmayas = new System.Windows.Forms.Label();
            this.lblconcamos = new System.Windows.Forms.Label();
            this.lblcontainers = new System.Windows.Forms.Label();
            this.lblpalets = new System.Windows.Forms.Label();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.lblstrups = new System.Windows.Forms.Label();
            this.strup = new System.Windows.Forms.Label();
            this.btnStrups = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMayas
            // 
            this.btnMayas.Location = new System.Drawing.Point(188, 49);
            this.btnMayas.Name = "btnMayas";
            this.btnMayas.Size = new System.Drawing.Size(36, 28);
            this.btnMayas.TabIndex = 0;
            this.btnMayas.Text = " + ";
            this.btnMayas.Click += new System.EventHandler(this.btnMayas_Click);
            // 
            // lblManifiesto
            // 
            this.lblManifiesto.Location = new System.Drawing.Point(10, 17);
            this.lblManifiesto.Name = "lblManifiesto";
            this.lblManifiesto.Size = new System.Drawing.Size(67, 20);
            this.lblManifiesto.Text = "Manifiesto";
            // 
            // txtManifiesto
            // 
            this.txtManifiesto.Location = new System.Drawing.Point(74, 15);
            this.txtManifiesto.Name = "txtManifiesto";
            this.txtManifiesto.Size = new System.Drawing.Size(87, 21);
            this.txtManifiesto.TabIndex = 2;
            // 
            // btnConcamos
            // 
            this.btnConcamos.Location = new System.Drawing.Point(188, 84);
            this.btnConcamos.Name = "btnConcamos";
            this.btnConcamos.Size = new System.Drawing.Size(36, 28);
            this.btnConcamos.TabIndex = 3;
            this.btnConcamos.Text = " + ";
            this.btnConcamos.Click += new System.EventHandler(this.btnConcamos_Click);
            // 
            // btnContainers
            // 
            this.btnContainers.Location = new System.Drawing.Point(188, 120);
            this.btnContainers.Name = "btnContainers";
            this.btnContainers.Size = new System.Drawing.Size(36, 28);
            this.btnContainers.TabIndex = 4;
            this.btnContainers.Text = " + ";
            this.btnContainers.Click += new System.EventHandler(this.btnContainers_Click);
            // 
            // btnPalets
            // 
            this.btnPalets.Location = new System.Drawing.Point(188, 155);
            this.btnPalets.Name = "btnPalets";
            this.btnPalets.Size = new System.Drawing.Size(36, 28);
            this.btnPalets.TabIndex = 5;
            this.btnPalets.Text = " + ";
            this.btnPalets.Click += new System.EventHandler(this.btnPalets_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.Text = "Mayas: ";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(10, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.Text = "Concamos:";
            this.label2.ParentChanged += new System.EventHandler(this.label2_ParentChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(10, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 20);
            this.label3.Text = "Containers:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(10, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.Text = "Palets:";
            // 
            // lblmayas
            // 
            this.lblmayas.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblmayas.Location = new System.Drawing.Point(84, 57);
            this.lblmayas.Name = "lblmayas";
            this.lblmayas.Size = new System.Drawing.Size(35, 20);
            this.lblmayas.Text = "0";
            // 
            // lblconcamos
            // 
            this.lblconcamos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblconcamos.Location = new System.Drawing.Point(83, 92);
            this.lblconcamos.Name = "lblconcamos";
            this.lblconcamos.Size = new System.Drawing.Size(35, 20);
            this.lblconcamos.Text = "0";
            // 
            // lblcontainers
            // 
            this.lblcontainers.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblcontainers.Location = new System.Drawing.Point(84, 128);
            this.lblcontainers.Name = "lblcontainers";
            this.lblcontainers.Size = new System.Drawing.Size(35, 20);
            this.lblcontainers.Text = "0";
            // 
            // lblpalets
            // 
            this.lblpalets.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblpalets.Location = new System.Drawing.Point(84, 163);
            this.lblpalets.Name = "lblpalets";
            this.lblpalets.Size = new System.Drawing.Size(35, 20);
            this.lblpalets.Text = "0";
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(131, 230);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(93, 25);
            this.btnGrabar.TabIndex = 22;
            this.btnGrabar.Text = "Grabar";
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // lblstrups
            // 
            this.lblstrups.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblstrups.Location = new System.Drawing.Point(84, 199);
            this.lblstrups.Name = "lblstrups";
            this.lblstrups.Size = new System.Drawing.Size(35, 20);
            this.lblstrups.Text = "0";
            // 
            // strup
            // 
            this.strup.Location = new System.Drawing.Point(10, 199);
            this.strup.Name = "strup";
            this.strup.Size = new System.Drawing.Size(67, 20);
            this.strup.Text = "Strups:";
            // 
            // btnStrups
            // 
            this.btnStrups.Location = new System.Drawing.Point(188, 191);
            this.btnStrups.Name = "btnStrups";
            this.btnStrups.Size = new System.Drawing.Size(36, 28);
            this.btnStrups.TabIndex = 34;
            this.btnStrups.Text = " + ";
            this.btnStrups.Click += new System.EventHandler(this.btnStrups_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.Location = new System.Drawing.Point(167, 13);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(59, 28);
            this.btnValidar.TabIndex = 44;
            this.btnValidar.Text = "Validar";
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // RegistrarCargadeManifiesto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.lblstrups);
            this.Controls.Add(this.strup);
            this.Controls.Add(this.btnStrups);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.lblpalets);
            this.Controls.Add(this.lblcontainers);
            this.Controls.Add(this.lblconcamos);
            this.Controls.Add(this.lblmayas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPalets);
            this.Controls.Add(this.btnContainers);
            this.Controls.Add(this.btnConcamos);
            this.Controls.Add(this.txtManifiesto);
            this.Controls.Add(this.lblManifiesto);
            this.Controls.Add(this.btnMayas);
            this.Menu = this.mainMenu1;
            this.Name = "RegistrarCargadeManifiesto";
            this.Text = "Registrar Carga";
            this.Load += new System.EventHandler(this.RegistrarCargadeManifiesto_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMayas;
        private System.Windows.Forms.Label lblManifiesto;
        private System.Windows.Forms.TextBox txtManifiesto;
        private System.Windows.Forms.Button btnConcamos;
        private System.Windows.Forms.Button btnContainers;
        private System.Windows.Forms.Button btnPalets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblmayas;
        private System.Windows.Forms.Label lblconcamos;
        private System.Windows.Forms.Label lblcontainers;
        private System.Windows.Forms.Label lblpalets;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Label lblstrups;
        private System.Windows.Forms.Label strup;
        private System.Windows.Forms.Button btnStrups;
        private System.Windows.Forms.Button btnValidar;
    }
}