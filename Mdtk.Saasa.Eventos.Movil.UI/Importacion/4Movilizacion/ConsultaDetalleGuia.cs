﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion
{
    public partial class ConsultaDetalleGuia : Form
    {
        public ConsultaDetalleGuia()
        {
            InitializeComponent();
        }
        private string NumeroGuia = "";
        private string UbicacionFinal = "";
        public ConsultaDetalleGuia(string NumGuia,string UbiFinal)
        {
            NumeroGuia = NumGuia;
            UbicacionFinal = UbiFinal;
            InitializeComponent();
        }

        public List<DataGridDetalleGuiaBE> CargarDatos()
        {
            List<UnidadAlmacenamientoBE> ListUA = new List<UnidadAlmacenamientoBE>();
            List<DataGridDetalleGuiaBE> ListDataGrid = new List<DataGridDetalleGuiaBE>();
            UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();
            UA.NumeroGuia = NumeroGuia;
            ListUA = UnidadAlmacenamientoBL.Instancia.BuscarUnidadAlmacenamientoList(UA);

            foreach (var item in ListUA)
            {
                DataGridDetalleGuiaBE DataGrid = new DataGridDetalleGuiaBE();
                DataGrid.NroUA = item.CodigoUA;
                DataGrid.Ubicacion = item.UbicacionActual;
                DataGrid.Bls = item.Bultos;
                DataGrid.Peso = item.Peso;
                ListDataGrid.Add(DataGrid);
            }
            return ListDataGrid;
        }

        public void CargarDataGrid()
        {
            txtUbicacionFinal.Text = UbicacionFinal;
            dgDetalleGuia.DataSource = CargarDatos();
            int alto = CargarDatos().Count;
            dgDetalleGuia.Height = (alto * 18) +21;
            dgDetalleGuia.Width = 227;
            btnUbicar.Location = new Point(155, dgDetalleGuia.Height + dgDetalleGuia.Location.Y + 10);
        }

        private void ConsultaDetalleGuia_Load(object sender, EventArgs e)
        {
            CargarDataGrid();
        }

        private void dgDetalleGuia_CurrentCellChanged(object sender, EventArgs e)
        {
            dgDetalleGuia.Select(dgDetalleGuia.CurrentRowIndex);
        }

        private void btnUbicar_Click(object sender, EventArgs e)
        {
            int fila = dgDetalleGuia.CurrentCell.RowNumber;
            List<DataGridDetalleGuiaBE> DGPO = new List<DataGridDetalleGuiaBE>();
            DGPO = CargarDatos();
            int count = 0;
            string NumeroUA = "";
            string UbicacionFinal = txtUbicacionFinal.Text;
            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    NumeroUA = item.NroUA;
                }
                count++;
            }

            AsignarUbicacionUA FormAsignarUbicacion = new AsignarUbicacionUA(UbicacionFinal, NumeroUA);
            FormAsignarUbicacion.Show();
        }
    }
}