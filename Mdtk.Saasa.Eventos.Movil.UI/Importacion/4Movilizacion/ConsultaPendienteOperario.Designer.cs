﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion
{
    partial class ConsultaPendienteOperario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.dgPendienteOperario = new System.Windows.Forms.DataGrid();
            this.lblMotivo = new System.Windows.Forms.Label();
            this.cbMotivo = new System.Windows.Forms.ComboBox();
            this.btnDetalle = new System.Windows.Forms.Button();
            this.btnDevolver = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(255, 132);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(72, 20);
            this.btnEnviar.TabIndex = 3;
            this.btnEnviar.Text = "Ok";
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // dgPendienteOperario
            // 
            this.dgPendienteOperario.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgPendienteOperario.Location = new System.Drawing.Point(0, 29);
            this.dgPendienteOperario.Name = "dgPendienteOperario";
            this.dgPendienteOperario.Size = new System.Drawing.Size(330, 91);
            this.dgPendienteOperario.TabIndex = 2;
            // 
            // lblMotivo
            // 
            this.lblMotivo.Location = new System.Drawing.Point(3, 132);
            this.lblMotivo.Name = "lblMotivo";
            this.lblMotivo.Size = new System.Drawing.Size(100, 20);
            this.lblMotivo.Text = "Motivo Demora";
            // 
            // cbMotivo
            // 
            this.cbMotivo.Location = new System.Drawing.Point(98, 130);
            this.cbMotivo.Name = "cbMotivo";
            this.cbMotivo.Size = new System.Drawing.Size(142, 22);
            this.cbMotivo.TabIndex = 5;
            // 
            // btnDetalle
            // 
            this.btnDetalle.Location = new System.Drawing.Point(187, 177);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(72, 20);
            this.btnDetalle.TabIndex = 6;
            this.btnDetalle.Text = "ir Detalle";
            // 
            // btnDevolver
            // 
            this.btnDevolver.Location = new System.Drawing.Point(70, 177);
            this.btnDevolver.Name = "btnDevolver";
            this.btnDevolver.Size = new System.Drawing.Size(72, 20);
            this.btnDevolver.TabIndex = 7;
            this.btnDevolver.Text = "Devolver";
            // 
            // ConsultaPendienteOperario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnDevolver);
            this.Controls.Add(this.btnDetalle);
            this.Controls.Add(this.cbMotivo);
            this.Controls.Add(this.lblMotivo);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.dgPendienteOperario);
            this.Menu = this.mainMenu1;
            this.Name = "ConsultaPendienteOperario";
            this.Text = "ConsultaPendienteOperario";
            this.Load += new System.EventHandler(this.ConsultaPendienteOperario_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.DataGrid dgPendienteOperario;
        private System.Windows.Forms.Label lblMotivo;
        private System.Windows.Forms.ComboBox cbMotivo;
        private System.Windows.Forms.Button btnDetalle;
        private System.Windows.Forms.Button btnDevolver;
    }
}