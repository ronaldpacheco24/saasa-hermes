﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion
{
    partial class ConsultaPendienteMovilizacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.dgPendienteMovilizacion = new System.Windows.Forms.DataGrid();
            this.btnTomar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dgPendienteMovilizacion
            // 
            this.dgPendienteMovilizacion.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgPendienteMovilizacion.Location = new System.Drawing.Point(0, 45);
            this.dgPendienteMovilizacion.Name = "dgPendienteMovilizacion";
            this.dgPendienteMovilizacion.Size = new System.Drawing.Size(330, 137);
            this.dgPendienteMovilizacion.TabIndex = 0;
            this.dgPendienteMovilizacion.CurrentCellChanged += new System.EventHandler(this.dgPendienteMovilizacion_CurrentCellChanged);
            // 
            // btnTomar
            // 
            this.btnTomar.Location = new System.Drawing.Point(255, 188);
            this.btnTomar.Name = "btnTomar";
            this.btnTomar.Size = new System.Drawing.Size(72, 20);
            this.btnTomar.TabIndex = 1;
            this.btnTomar.Text = "Tomar";
            this.btnTomar.Click += new System.EventHandler(this.btnTomar_Click);
            // 
            // ConsultaPendienteMovilizacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnTomar);
            this.Controls.Add(this.dgPendienteMovilizacion);
            this.Menu = this.mainMenu1;
            this.Name = "ConsultaPendienteMovilizacion";
            this.Text = "ConsultaPendienteMovilizacion";
            this.Load += new System.EventHandler(this.ConsultaPendienteMovilizacion_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dgPendienteMovilizacion;
        private System.Windows.Forms.Button btnTomar;
    }
}