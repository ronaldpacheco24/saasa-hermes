﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion
{
    public partial class ConsultaPendienteOperario : Form
    {
        public ConsultaPendienteOperario()
        {
            InitializeComponent();
        }

        public int ObtenerMinutos(DateTime FechaInicial, DateTime FechaFinal)
        {
            int Mes, Dia, Hora, Minuto;

            Mes = FechaFinal.Month - FechaInicial.Month;
            Dia = FechaFinal.Day - FechaInicial.Day;
            Hora = FechaFinal.Hour - FechaInicial.Hour;
            Minuto = FechaFinal.Minute - FechaInicial.Minute;

            int MesFinal = FechaFinal.Month;
            int MesInicial = FechaInicial.Month;
            int ObtenerDiasMeses = 0;
            int DiasxMeses = 0;
            for (int i = 0; i < Mes; i++)
            {
                if (MesFinal > MesInicial)
                {
                    ObtenerDiasMeses = DateTime.DaysInMonth(FechaInicial.Year, FechaInicial.Month);
                    DiasxMeses = ObtenerDiasMeses + DiasxMeses;
                    MesInicial++;
                }
            }
            int DiaxHora = 0;
            int HoraxMinuto = 0;
            Dia = DiasxMeses + Dia;
            DiaxHora = (Dia * 24);
            HoraxMinuto = DiaxHora + Hora;
            HoraxMinuto = HoraxMinuto * 60;
            Minuto = HoraxMinuto + Minuto;

            return Minuto;
        }

        public List<DataGridPendienteOperarioImpoBE> CargarDatos()
        {
            List<DataGridPendienteOperarioImpoBE> DataGridList = new List<DataGridPendienteOperarioImpoBE>();
            List<GuiaBE> GuiaPendienteOperarioList = new List<GuiaBE>();
            GuiaPendienteOperarioList = MovilizacionBL.Instancia.BuscarPendientesMovilizacion();
            int TotalBultos = 0;
            decimal TotalPeso = 0;
            foreach (var item in GuiaPendienteOperarioList)
            {
                DataGridPendienteOperarioImpoBE DataGrid = new DataGridPendienteOperarioImpoBE();
                TotalBultos = 0;
                TotalPeso = 0;
                foreach (var item2 in item.ListaUA)
                {
                    DataGrid.NroGuia = item2.NumeroGuia;
                    TotalBultos = TotalBultos + item2.Bultos;
                    DataGrid.Bts = TotalBultos;
                    TotalPeso = TotalPeso + item2.Peso;
                    DataGrid.Peso = TotalPeso;
                    DataGrid.UAs = Convert.ToString(item.ListaUA.Count);
                    DataGrid.UbicacionFinal = item2.UbicacionDestino;
                }
                DataGridList.Add(DataGrid);
            }
            return DataGridList;
        }
        public void CargarDataGrid()
        {
            dgPendienteOperario.DataSource = CargarDatos();
            int alto = CargarDatos().Count;
            dgPendienteOperario.Height = (alto * 18) + 21;
            dgPendienteOperario.Width = 430;
            btnEnviar.Location = new Point(355, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);
            lblMotivo.Location = new Point(51, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);
            cbMotivo.Location = new Point(152, dgPendienteOperario.Height + dgPendienteOperario.Location.Y + 10);

            btnDetalle.Location = new Point(261, cbMotivo.Height + cbMotivo.Location.Y + 20);
            btnDevolver.Location = new Point(152, cbMotivo.Height + cbMotivo.Location.Y + 20);
        }

        private void ConsultaPendienteOperario_Load(object sender, EventArgs e)
        {
            CargarDataGrid();
        }

        private void dgPendienteOperario_CurrentCellChanged(object sender, EventArgs e)
        {
            dgPendienteOperario.Select(dgPendienteOperario.CurrentRowIndex);
        }

        private void btnDevolver_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteOperario.CurrentCell.RowNumber;
            List<DataGridPendienteOperarioImpoBE> DGPO = new List<DataGridPendienteOperarioImpoBE>();
            DGPO = CargarDatos();
            int count = 0;

            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    MovilizacionBL.Instancia.DevolverGuia(item.NroGuia,"Pendiente por Movilizar");
                }
                count++;
            }
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteOperario.CurrentCell.RowNumber;
            List<DataGridPendienteOperarioImpoBE> DGPO = new List<DataGridPendienteOperarioImpoBE>();
            DGPO = CargarDatos();
            int count = 0;
            string NumeroGuia = "";
            string UbicacionFinal = "";
            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    NumeroGuia = item.NroGuia;
                    UbicacionFinal = item.UbicacionFinal;
                }
                count++;
            }
            ConsultaDetalleGuia FormConsulta = new ConsultaDetalleGuia(NumeroGuia,UbicacionFinal);
            FormConsulta.Show();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteOperario.CurrentCell.RowNumber;
            List<DataGridPendienteOperarioImpoBE> DGPO = new List<DataGridPendienteOperarioImpoBE>();
            DGPO = CargarDatos();
            int count = 0;
            string Motivo = cbMotivo.Text;

            foreach (var item in DGPO)
            {
                if (count == fila)
                {
                    MovilizacionBL.Instancia.RegistrarMotivoDemora(item.NroGuia,Motivo,"Demorado");
                }
                count++;
            }
        }

    }
}