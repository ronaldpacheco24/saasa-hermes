﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._4Movilizacion
{
    public partial class ConsultaPendienteMovilizacion : Form
    {
        public List<DataGridPendienteMovilizacionImpoBE> ListaGuiasPendientesMovilizacion { get; set; }
        public List<DataGridPendienteMovilizacionImpoBE> ListaGuiasTomadas { get; set; }

        public ConsultaPendienteMovilizacion()
        {
            InitializeComponent();
        }

        public int ObtenerMinutos(DateTime FechaInicial, DateTime FechaFinal)
        {
            int Mes, Dia, Hora, Minuto;

            Mes = FechaFinal.Month - FechaInicial.Month;
            Dia = FechaFinal.Day - FechaInicial.Day;
            Hora = FechaFinal.Hour - FechaInicial.Hour;
            Minuto = FechaFinal.Minute - FechaInicial.Minute;

            int MesFinal = FechaFinal.Month;
            int MesInicial = FechaInicial.Month;
            int ObtenerDiasMeses = 0;
            int DiasxMeses = 0;
            for (int i = 0; i < Mes; i++)
            {
                if (MesFinal > MesInicial)
                {
                    ObtenerDiasMeses = DateTime.DaysInMonth(FechaInicial.Year, FechaInicial.Month);
                    DiasxMeses = ObtenerDiasMeses + DiasxMeses;
                    MesInicial++;
                }
            }
            int DiaxHora = 0;
            int HoraxMinuto = 0;
            Dia = DiasxMeses + Dia;
            DiaxHora = (Dia * 24);
            HoraxMinuto = DiaxHora + Hora;
            HoraxMinuto = HoraxMinuto * 60;
            Minuto = HoraxMinuto + Minuto;

            return Minuto;
        }

        public void CargarDatos()
        {
            List<DataGridPendienteMovilizacionImpoBE> DataGridList = new List<DataGridPendienteMovilizacionImpoBE>();
            List<GuiaBE> GuiaPendienteMovilizacionList = new List<GuiaBE>();
            GuiaPendienteMovilizacionList = MovilizacionBL.Instancia.BuscarPendientesMovilizacion();
            ListaGuiasTomadas = new List<DataGridPendienteMovilizacionImpoBE>();
            //Mostrar Guias
            //Usar metodo de cargar datos de Edgar
            int TotalBultos = 0;
            decimal TotalPeso = 0;
            ListaGuiasPendientesMovilizacion = new List<DataGridPendienteMovilizacionImpoBE>();
            foreach (var item in GuiaPendienteMovilizacionList)
            {
                DataGridPendienteMovilizacionImpoBE DataGrid = new DataGridPendienteMovilizacionImpoBE();
                TotalBultos = 0;
                TotalPeso = 0;
                DataGrid.NGuia = item.NumeroGuia;
                foreach(var item2 in item.ListaUA)
                {
                    TotalBultos = TotalBultos + item2.Bultos;
                    DataGrid.Bultos = TotalBultos;
                    TotalPeso = TotalPeso + item2.Peso;
                    DataGrid.Peso = TotalPeso;
                    DataGrid.UAs = Convert.ToString(item.ListaUA.Count);
                    DataGrid.UbicacionFinal = item2.UbicacionDestino;
                }
                ListaGuiasPendientesMovilizacion.Add(DataGrid);
            }
        }
        
        public void CargarDataGrid()
        {
            dgPendienteMovilizacion.DataSource = ListaGuiasPendientesMovilizacion;
            dgPendienteMovilizacion.Update();
            dgPendienteMovilizacion.Refresh();
            dgPendienteMovilizacion.Hide();
            dgPendienteMovilizacion.Show();
            
        }
        private void ConsultaPendienteMovilizacion_Load(object sender, EventArgs e)
        {
            CargarDatos();
            CargarDataGrid();
        }

        private void btnTomar_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteMovilizacion.CurrentCell.RowNumber;
            List<DataGridPendienteMovilizacionImpoBE> DGPM = new List<DataGridPendienteMovilizacionImpoBE>();
            int count = 0;
            DGPM = ListaGuiasPendientesMovilizacion;
            DataGridPendienteMovilizacionImpoBE UAr = new DataGridPendienteMovilizacionImpoBE();
            foreach (var itemPM in DGPM)
            {
                if (count == fila)
                {
                    //MovilizacionBL.Instancia.RegistrarTomaGuia(itemPM.NGuia,0,"Tomado");
                    UAr = ListaGuiasPendientesMovilizacion.Select(z => z).Where(item => item.NGuia == itemPM.NGuia).SingleOrDefault();
                }
                count++;
            }
            ListaGuiasTomadas.Add(UAr);
            ListaGuiasPendientesMovilizacion.Remove(UAr);
            CargarDataGrid();
        }

        private void dgPendienteMovilizacion_CurrentCellChanged(object sender, EventArgs e)
        {
            dgPendienteMovilizacion.Select(dgPendienteMovilizacion.CurrentRowIndex);
        }

    }
}