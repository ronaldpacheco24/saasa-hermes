﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion
{
    partial class LoginAutorizacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lblCodigoUA = new System.Windows.Forms.Label();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.txtCodigoUA = new System.Windows.Forms.TextBox();
            this.txtUbicacionUA = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCodigoUA
            // 
            this.lblCodigoUA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblCodigoUA.Location = new System.Drawing.Point(3, 83);
            this.lblCodigoUA.Name = "lblCodigoUA";
            this.lblCodigoUA.Size = new System.Drawing.Size(78, 20);
            this.lblCodigoUA.Text = "Contraceña";
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUbicacion.Location = new System.Drawing.Point(25, 53);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(56, 20);
            this.lblUbicacion.Text = "Usuario";
            // 
            // txtCodigoUA
            // 
            this.txtCodigoUA.Location = new System.Drawing.Point(87, 82);
            this.txtCodigoUA.Name = "txtCodigoUA";
            this.txtCodigoUA.Size = new System.Drawing.Size(127, 21);
            this.txtCodigoUA.TabIndex = 58;
            // 
            // txtUbicacionUA
            // 
            this.txtUbicacionUA.Location = new System.Drawing.Point(87, 52);
            this.txtUbicacionUA.Name = "txtUbicacionUA";
            this.txtUbicacionUA.Size = new System.Drawing.Size(127, 21);
            this.txtUbicacionUA.TabIndex = 57;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(130, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 20);
            this.button1.TabIndex = 61;
            this.button1.Text = "Autorizar";
            // 
            // LoginAutorizacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtCodigoUA);
            this.Controls.Add(this.txtUbicacionUA);
            this.Controls.Add(this.lblCodigoUA);
            this.Controls.Add(this.lblUbicacion);
            this.Menu = this.mainMenu1;
            this.Name = "LoginAutorizacion";
            this.Text = "LoginAutorizacion";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCodigoUA;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.TextBox txtCodigoUA;
        private System.Windows.Forms.TextBox txtUbicacionUA;
        private System.Windows.Forms.Button button1;
    }
}