﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion
{
    public partial class AsignarManual : Form
    {
        public AsignarManual()
        {
            InitializeComponent();
        }

        private void btnEnvio_Click_1(object sender, EventArgs e)
        {
            string Ubicacion = cbUbicacion.Text;
            if (Ubicacion.Equals("NINGUNA") || Ubicacion.Equals(""))
            {
                string almacen = txtAlmacen.Text;
                string rack = txtRack.Text;
                string nivel = txtNivel.Text;
                string columna = txtColumna.Text;
                Ubicacion = almacen + "-" + rack + "-" + columna + "-" + nivel;
                if (almacen == "" & rack == "" & nivel == "" & columna == "")
                {
                    Ubicacion = "";
                }
                
            }
            AsignarUbicacionUA FormAsignar = new AsignarUbicacionUA(Ubicacion);
            FormAsignar.Show();
        }

        private void cbUbicacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUbicacion.Text.Equals("NINGUNA") || cbUbicacion.Text.Equals(""))
            {
                txtAlmacen.Enabled = true;
                txtNivel.Enabled = true;
                txtRack.Enabled = true;
                txtColumna.Enabled = true;
            }
            else
            {
                txtAlmacen.Enabled = false;
                txtNivel.Enabled = false;
                txtRack.Enabled = false;
                txtColumna.Enabled = false;
            }
        }

        private void AsignarManual_Load(object sender, EventArgs e)
        {
            cbUbicacion.Items.Add("PRE-ALMACENAJE");
            cbUbicacion.Items.Add("ZONA INTERMEDIA");
            cbUbicacion.Items.Add("ZONA AFORO I");
            cbUbicacion.Items.Add("RAMPA I");
            cbUbicacion.Items.Add("RAMPA II");
            cbUbicacion.Items.Add("RAMPA III");
            cbUbicacion.Items.Add("NINGUNA");
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            PaginaPrincipal Menu = new PaginaPrincipal();
            Menu.Show();
        }
    }
}