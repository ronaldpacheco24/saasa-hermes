﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion
{
    partial class ConsultaUA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.btnScan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "AsignarUbicacionUA";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(155, 236);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(72, 20);
            this.btnScan.TabIndex = 25;
            this.btnScan.Text = "SCAN";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // ConsultaUA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnScan);
            this.Menu = this.mainMenu1;
            this.Name = "ConsultaUA";
            this.Text = "ConsultaUA";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.MenuItem menuItem1;

    }
}