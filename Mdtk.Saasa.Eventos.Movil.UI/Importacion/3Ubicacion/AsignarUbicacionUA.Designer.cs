﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion
{
    partial class AsignarUbicacionUA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.txtCodigoUA = new System.Windows.Forms.TextBox();
            this.txtUbicacionUA = new System.Windows.Forms.TextBox();
            this.btnAforo = new System.Windows.Forms.Button();
            this.btnZonaInterna = new System.Windows.Forms.Button();
            this.btnPreAlmacen = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            this.lblCodigoUA = new System.Windows.Forms.Label();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.btnUbicar = new System.Windows.Forms.Button();
            this.lblBultos = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.lblUbicaciones = new System.Windows.Forms.Label();
            this.ScanUbi = new System.Windows.Forms.Button();
            this.ScanUA = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Volver Menu";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // txtCodigoUA
            // 
            this.txtCodigoUA.Location = new System.Drawing.Point(38, 42);
            this.txtCodigoUA.Name = "txtCodigoUA";
            this.txtCodigoUA.Size = new System.Drawing.Size(100, 21);
            this.txtCodigoUA.TabIndex = 56;
            // 
            // txtUbicacionUA
            // 
            this.txtUbicacionUA.Location = new System.Drawing.Point(39, 14);
            this.txtUbicacionUA.Name = "txtUbicacionUA";
            this.txtUbicacionUA.Size = new System.Drawing.Size(100, 21);
            this.txtUbicacionUA.TabIndex = 55;
            // 
            // btnAforo
            // 
            this.btnAforo.Location = new System.Drawing.Point(116, 119);
            this.btnAforo.Name = "btnAforo";
            this.btnAforo.Size = new System.Drawing.Size(72, 20);
            this.btnAforo.TabIndex = 52;
            this.btnAforo.Text = "AFORO";
            this.btnAforo.Click += new System.EventHandler(this.btnAforo_Click);
            // 
            // btnZonaInterna
            // 
            this.btnZonaInterna.Location = new System.Drawing.Point(38, 119);
            this.btnZonaInterna.Name = "btnZonaInterna";
            this.btnZonaInterna.Size = new System.Drawing.Size(72, 20);
            this.btnZonaInterna.TabIndex = 51;
            this.btnZonaInterna.Text = "ZON-I";
            this.btnZonaInterna.Click += new System.EventHandler(this.btnZonaInterna_Click);
            // 
            // btnPreAlmacen
            // 
            this.btnPreAlmacen.Location = new System.Drawing.Point(116, 93);
            this.btnPreAlmacen.Name = "btnPreAlmacen";
            this.btnPreAlmacen.Size = new System.Drawing.Size(72, 20);
            this.btnPreAlmacen.TabIndex = 50;
            this.btnPreAlmacen.Text = "PRE-A";
            this.btnPreAlmacen.Click += new System.EventHandler(this.btnPreAlmacen_Click);
            // 
            // btnManual
            // 
            this.btnManual.Location = new System.Drawing.Point(38, 93);
            this.btnManual.Name = "btnManual";
            this.btnManual.Size = new System.Drawing.Size(72, 20);
            this.btnManual.TabIndex = 49;
            this.btnManual.Text = "Manual";
            this.btnManual.Click += new System.EventHandler(this.btnManual_Click);
            // 
            // lblCodigoUA
            // 
            this.lblCodigoUA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblCodigoUA.Location = new System.Drawing.Point(2, 46);
            this.lblCodigoUA.Name = "lblCodigoUA";
            this.lblCodigoUA.Size = new System.Drawing.Size(30, 20);
            this.lblCodigoUA.Text = "UA";
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUbicacion.Location = new System.Drawing.Point(2, 16);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(30, 20);
            this.lblUbicacion.Text = "Ub.";
            // 
            // btnUbicar
            // 
            this.btnUbicar.Location = new System.Drawing.Point(149, 181);
            this.btnUbicar.Name = "btnUbicar";
            this.btnUbicar.Size = new System.Drawing.Size(72, 20);
            this.btnUbicar.TabIndex = 59;
            this.btnUbicar.Text = "Ubicar";
            this.btnUbicar.Click += new System.EventHandler(this.btnUbicar_Click);
            // 
            // lblBultos
            // 
            this.lblBultos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblBultos.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblBultos.Location = new System.Drawing.Point(149, 44);
            this.lblBultos.Name = "lblBultos";
            this.lblBultos.Size = new System.Drawing.Size(39, 20);
            this.lblBultos.Text = " ";
            // 
            // lblPeso
            // 
            this.lblPeso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPeso.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPeso.Location = new System.Drawing.Point(192, 44);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(39, 20);
            this.lblPeso.Text = " ";
            // 
            // lblUbicaciones
            // 
            this.lblUbicaciones.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUbicaciones.ForeColor = System.Drawing.Color.Red;
            this.lblUbicaciones.Location = new System.Drawing.Point(21, 70);
            this.lblUbicaciones.Name = "lblUbicaciones";
            this.lblUbicaciones.Size = new System.Drawing.Size(210, 20);
            this.lblUbicaciones.Text = " ";
            this.lblUbicaciones.Visible = false;
            // 
            // ScanUbi
            // 
            this.ScanUbi.Location = new System.Drawing.Point(38, 169);
            this.ScanUbi.Name = "ScanUbi";
            this.ScanUbi.Size = new System.Drawing.Size(72, 20);
            this.ScanUbi.TabIndex = 62;
            this.ScanUbi.Text = "Scan Ubi";
            this.ScanUbi.Click += new System.EventHandler(this.ScanUbi_Click);
            // 
            // ScanUA
            // 
            this.ScanUA.Location = new System.Drawing.Point(38, 206);
            this.ScanUA.Name = "ScanUA";
            this.ScanUA.Size = new System.Drawing.Size(72, 20);
            this.ScanUA.TabIndex = 63;
            this.ScanUA.Text = "Scan UA";
            this.ScanUA.Click += new System.EventHandler(this.ScanUA_Click);
            // 
            // AsignarUbicacionUA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.ScanUA);
            this.Controls.Add(this.ScanUbi);
            this.Controls.Add(this.lblUbicaciones);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.lblBultos);
            this.Controls.Add(this.btnUbicar);
            this.Controls.Add(this.txtCodigoUA);
            this.Controls.Add(this.txtUbicacionUA);
            this.Controls.Add(this.btnAforo);
            this.Controls.Add(this.btnZonaInterna);
            this.Controls.Add(this.btnPreAlmacen);
            this.Controls.Add(this.btnManual);
            this.Controls.Add(this.lblCodigoUA);
            this.Controls.Add(this.lblUbicacion);
            this.Menu = this.mainMenu1;
            this.Name = "AsignarUbicacionUA";
            this.Text = "AsignarUbicacionUA";
            this.Load += new System.EventHandler(this.AsignarUbicacionUA_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCodigoUA;
        private System.Windows.Forms.TextBox txtUbicacionUA;
        private System.Windows.Forms.Button btnAforo;
        private System.Windows.Forms.Button btnZonaInterna;
        private System.Windows.Forms.Button btnPreAlmacen;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Label lblCodigoUA;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Button btnUbicar;
        private System.Windows.Forms.Label lblBultos;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblUbicaciones;
        private System.Windows.Forms.Button ScanUbi;
        private System.Windows.Forms.Button ScanUA;
    }
}