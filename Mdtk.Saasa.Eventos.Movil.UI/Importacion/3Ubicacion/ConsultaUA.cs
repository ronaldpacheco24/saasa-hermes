﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion
{
    public partial class ConsultaUA : Form
    {
        public ConsultaUA()
        {
            InitializeComponent();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            AsignarUbicacionUA AsignarUbicacion = new AsignarUbicacionUA();
            AsignarUbicacion.Show();
        }

        private TextBox[] txtCodigo = new TextBox[4];
        private Label[] lblNombreUA = new Label[4];
        private Label[] lblBultos = new Label[4];
        private Label[] lblPeso = new Label[4];
        private Label[] lblUbicaciones = new Label[4];
        private int CountEvent = 0;
        private int Location_Y = 15;
        private int Location_Y2 = 40;

        private void btnScan_Click(object sender, EventArgs e)
        {
            CrearConsultaUA();
        }

        //CREADO POR RONALD PACHECO ALARCON

        private void CrearConsultaUA()
        {
            if (CountEvent == 4)
            {
                CountEvent = 0;
                Location_Y = 15;
                Location_Y2 = 40;
            }

            Controls.Remove(txtCodigo[CountEvent]);
            Controls.Remove(lblNombreUA[CountEvent]);
            Controls.Remove(lblBultos[CountEvent]);
            Controls.Remove(lblPeso[CountEvent]);
            Controls.Remove(lblUbicaciones[CountEvent]);

            txtCodigo[CountEvent] = new TextBox();
            lblNombreUA[CountEvent] = new Label();
            lblBultos[CountEvent] = new Label();
            lblPeso[CountEvent] = new Label();
            lblUbicaciones[CountEvent] = new Label();

            UnidadAlmacenamientoBE UnidadAlmacenamiento = new UnidadAlmacenamientoBE();
            UnidadAlmacenamiento = UnidadAlmacenamientoBL.Instancia.BuscarUnidadAlmacenamientoCodigo("UA1");

            string UbicacionActual = UnidadAlmacenamiento.UbicacionActual;
            string UbicacionAsignada = UnidadAlmacenamiento.UbicacionAsignada;
            string UbicacionDestino = UnidadAlmacenamiento.UbicacionDestino;

            txtCodigo[CountEvent].Location = new System.Drawing.Point(36, Location_Y);
            txtCodigo[CountEvent].Name = "txtCodigo " + CountEvent;
            txtCodigo[CountEvent].Size = new System.Drawing.Size(97, 21);
            txtCodigo[CountEvent].TabIndex = 22;
            txtCodigo[CountEvent].Text = UnidadAlmacenamiento.CodigoUA;

            this.lblNombreUA[CountEvent].Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblNombreUA[CountEvent].Location = new System.Drawing.Point(11, Location_Y);
            this.lblNombreUA[CountEvent].Name = "lblNombreUA" + CountEvent;
            this.lblNombreUA[CountEvent].Size = new System.Drawing.Size(33, 20);
            this.lblNombreUA[CountEvent].Text = "UA";

            this.lblBultos[CountEvent].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblBultos[CountEvent].Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblBultos[CountEvent].Location = new System.Drawing.Point(144, Location_Y);
            this.lblBultos[CountEvent].Name = "lblBultos" + CountEvent;
            this.lblBultos[CountEvent].Size = new System.Drawing.Size(39, 20);
            this.lblBultos[CountEvent].Text = Convert.ToString(UnidadAlmacenamiento.Bultos) + "b";

            this.lblPeso[CountEvent].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPeso[CountEvent].Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPeso[CountEvent].Location = new System.Drawing.Point(184, Location_Y);
            this.lblPeso[CountEvent].Name = "lblPeso" + CountEvent;
            this.lblPeso[CountEvent].Size = new System.Drawing.Size(39, 20);
            this.lblPeso[CountEvent].Text = Convert.ToString(UnidadAlmacenamiento.Peso) + "k";

            if (UbicacionActual.Length >= 10)
            {
                UbicacionActual = UbicacionActual.Substring(0, 10);
            }
            if (UbicacionAsignada.Length >= 10)
            {
                UbicacionAsignada = UbicacionAsignada.Substring(0, 10);
            }
            if (UbicacionDestino.Length >= 10)
            {
                UbicacionDestino = UbicacionDestino.Substring(0, 10);
            }

            this.lblUbicaciones[CountEvent].Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblUbicaciones[CountEvent].ForeColor = System.Drawing.Color.Red;
            this.lblUbicaciones[CountEvent].Location = new System.Drawing.Point(11, Location_Y2);
            this.lblUbicaciones[CountEvent].Name = "lblUbicaciones" + CountEvent;
            this.lblUbicaciones[CountEvent].Size = new System.Drawing.Size(212, 20);
            this.lblUbicaciones[CountEvent].Text = UbicacionActual + " / " + UbicacionAsignada + " / " + UbicacionDestino;
            this.lblUbicaciones[CountEvent].Visible = true;

            Controls.Add(txtCodigo[CountEvent]);
            Controls.Add(lblNombreUA[CountEvent]);
            Controls.Add(lblBultos[CountEvent]);
            Controls.Add(lblPeso[CountEvent]);
            Controls.Add(lblUbicaciones[CountEvent]);

            Location_Y2 = Location_Y2 + 45;
            Location_Y = Location_Y + 45;
            CountEvent++;
        }


    }
}