﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._2DescargaManifiesto
{
    public partial class RecepcionGuias : Form
    {
        public ManifiestoBE Manifiesto { get; set; }

        private List<GuiaBE> ListaGuiasRegistradas { get; set; }
        private List<GuiaBE> ListaGuiasManfiestadas { get; set; }
        
        public RecepcionGuias()
        {
            InitializeComponent();
        }
        
        private void RecepcionGuias_Load(object sender, EventArgs e)
        {
            Manifiesto = ManifiestoBL.Instancia.BuscarManifiesto(new ManifiestoBE() { NumeroManifiesto = "8074" });

            if (Manifiesto != null)
            {
                txtmanifiesto.Text = Manifiesto.NumeroManifiesto;
                txtLineaAerea.Text = Manifiesto.LineaArea;
                txtFechaArribo.Text = Manifiesto.FechaArribo.ToString("yyyy/MM/dd HH:mm:ss");
                txtNumeroVuelo.Text = Manifiesto.NumeroVuelo;
                txtTBultos.Text = Manifiesto.ListaGuias.Sum(x => x.Bultos).ToString();                                
                txtTPeso.Text = Manifiesto.ListaGuias.Sum(x => x.Peso).ToString("N2");                
                txtTGuia.Text = Manifiesto.ListaGuias.Count().ToString();

                ListaGuiasManfiestadas = Manifiesto.ListaGuias;
                ListaGuiasRegistradas = new List<GuiaBE>();

                LLenarGrillaGuias(0);                
                
                txtestado.Text = "Bueno";
                txtcompleto.Text = "Si";


                dgListadoGuias.DataSource = ListaGuiasManfiestadas;
                //txtTGuiaR.Text = Convert.ToString(NumeroGuiasTotal);

                //foreach (var item in Manifiesto.ListaGuias)
                //{
                //    GuiaBE GuiaUnitaria = new GuiaBE();
                //    GuiaUnitaria.NumeroGuia = item.NumeroGuia;
                //    GuiaUnitaria.PesoManifestado = item.PesoManifestado;
                //    GuiaUnitaria.PesoReal = item.PesoReal;
                //    GuiaUnitaria.NumeroBultosManifestado = item.NumeroBultosManifestado;
                //    GuiaUnitaria.NumeroBultosReal = item.NumeroBultosReal;
                //    GuiaUnitaria.Estado = item.Estado;
                //    //GuiaList.Add(GuiaUnitaria);
                //    TotalPesoreal = TotalPesoreal + item.PesoManifestado;
                //    TotalBultosReal = TotalBultosReal + item.NumeroBultosReal;
                //    TotalPesoManifestado = TotalPesoManifestado + GuiaUnitaria.PesoManifestado;
                //    TotalBultosManifestados = TotalBultosManifestados + GuiaUnitaria.NumeroBultosManifestado;
                //    NumeroGuiasTotal++;
                //}
    
                //alto = NuevoManifiesto.ListaGuias.Count;
                //alto = 0;
                //dataGrid1.Height = (alto * 21) + 20;
               
                //lblcompleto.Location = new Point(109, (alto * 19) + 40 + 240);
                //lblestado.Location = new Point(0, (alto * 19) + 40 + 240);
                //txtcompleto.Location = new Point(172, (alto * 19) + 40 + 240);
                //txtestado.Location = new Point(40, (alto * 19) + 40 + 240);
                //btegistrar.Location = new Point(137, (alto * 19) + 70 + 240);
            }
        }

        private void btnRegistrarRecepcion_Click(object sender, EventArgs e)
        {
            //Seleccionamos la GUIA            
            GuiaBE GuiaSeleccionada = new GuiaBE();

            //Identificamos la Guia y le asginamos los valores.
            GuiaSeleccionada.Ident_Guia = 2;
            GuiaSeleccionada.EstadoCarga = 10;//Bueno
            GuiaSeleccionada.I003_CargaCompleto = 11;//Si

            bool result = GuiaBL.Instancia.ActualizarRecepcionGuia(GuiaSeleccionada);
            if (result)
                LLenarGrillaGuias(GuiaSeleccionada.Ident_Guia);          

            //dataGrid1.DataSource = GuiaBL.Instancia.RegistrarGuia(GuiaSeleccionada);
            //alto = GuiaBL.Instancia.RegistrarGuia(GuiaSeleccionada).Count();
            //lblcompleto.Location = new Point(109, (alto * 19) + 120);
            //lblestado.Location = new Point(0, (alto * 19) + 120);
            //txtcompleto.Location = new Point(172, (alto * 19) + 120);
            //txtestado.Location = new Point(40, (alto * 19) + 120);
            //btegistrar.Location = new Point(137, (alto * 19) + 30 + 120);
            //dataGrid1.Height = (alto * 21) + 21;

        }
       

        //private void txtNumerodeGuia_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (txtNumerodeGuia.Text.Length > 2)
        //    {
                //GuiaBE NuevaGuia = new GuiaBE();
                //NuevaGuia.NumeroGuia = txtNumerodeGuia.Text;
                //List<GuiaBE> ListaDeGuias = new List<GuiaBE>();
                //ListaDeGuias = GuiaBL.Instancia.BuscarGuia(NuevaGuia);
                /*
                foreach (var item in ListaDeGuias)
                {
                    GuiaBE GuiaUnitaria = new GuiaBE();
                    GuiaUnitaria.NumeroGuia = item.NumeroGuia;
                    GuiaUnitaria.PesoManifestado = item.PesoManifestado;
                    GuiaUnitaria.PesoReal = item.PesoReal;
                    GuiaUnitaria.NumeroBultosManifestado = item.NumeroBultosManifestado;
                    GuiaUnitaria.NumeroBultosReal = item.NumeroBultosReal;
                    GuiaUnitaria.Estado = item.Estado;
                    GuiaList.Add(GuiaUnitaria);
                 
                }*/
                //dataGrid1.DataSource = ListaDeGuias;
                //alto = ListaDeGuias.Count();
                //lblcompleto.Location = new Point(109, (alto * 19)  + 240);
                //lblestado.Location = new Point(0, (alto * 19)  + 240);
                //txtcompleto.Location = new Point(172, (alto * 19)  + 240);
                //txtestado.Location = new Point(40, (alto * 19)  + 240);
                //btegistrar.Location = new Point(137, (alto * 19) + 30 + 240);
        //        dataGrid1.Height = (alto * 20) + 20;
        //        dataGrid1.Show();
        //    }
        //}

        //private void menuItem1_Click(object sender, EventArgs e)
        //{
        //    DescargaManifiesto FormularioDescarga = new DescargaManifiesto();
        //    FormularioDescarga.Show();
        //}

        private void LLenarGrillaGuias(int Ident_Guia)
        {
            ListaGuiasRegistradas.Add(ListaGuiasManfiestadas.Select(x=>x).Where(x=>x.Ident_Guia==Ident_Guia).SingleOrDefault());
            
            //txtTBultosR.Text = ListaGuiasRegistradas.Sum(x => x.Bultos).ToString();
            //txtTPesoR.Text = ListaGuiasRegistradas.Sum(x => x.Peso).ToString("N2");
            //txtTGuiaR.Text = ListaGuiasRegistradas.Count().ToString();   


        }

        private void dgListadoGuias_CurrentCellChanged(object sender, EventArgs e)
        {

        }

        private void dgListadoGuias_Paint(object sender, PaintEventArgs e)
        {

        }        
    }
}