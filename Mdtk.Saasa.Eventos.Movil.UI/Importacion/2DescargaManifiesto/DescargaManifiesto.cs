﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._2DescargaManifiesto
{
    public partial class DescargaManifiesto : Form
    {
        public DescargaManifiesto()
        {
            InitializeComponent();
        }

        private void IniciarDescargaManifiesto_Click(object sender, EventArgs e)
        {
            //Se obtienen la informacion del Manifiesto.
            var numeroManifiesto = txtManifiesto.Text.Trim();
            var result = ManifiestoBL.Instancia.RegistrarInicioDescargaManifiesto(new ManifiestoBE() { NumeroManifiesto = numeroManifiesto });
            if (result)
            {
                var manifiesto = ManifiestoBL.Instancia.BuscarManifiesto(new ManifiestoBE() { NumeroManifiesto = numeroManifiesto });
                this.Close();

                RecepcionGuias formRecepcionGuias = new RecepcionGuias();
                formRecepcionGuias.Manifiesto = manifiesto;
                formRecepcionGuias.Show();

            }                                    
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            PaginaPrincipal NuevaPagina = new PaginaPrincipal();
            NuevaPagina.Show();
        }
        
    }
}