﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._2DescargaManifiesto
{
    partial class RecepcionGuias
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgListadoGuias = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.Ident_Guia = new System.Windows.Forms.DataGridTextBoxColumn();
            this.NumeroGuia = new System.Windows.Forms.DataGridTextBoxColumn();
            this.Peso = new System.Windows.Forms.DataGridTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridTextBoxColumn();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.lblmanifiesto = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtmanifiesto = new System.Windows.Forms.TextBox();
            this.txtLineaAerea = new System.Windows.Forms.TextBox();
            this.txtNumeroVuelo = new System.Windows.Forms.TextBox();
            this.txtFechaArribo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTGuia = new System.Windows.Forms.TextBox();
            this.txtTPeso = new System.Windows.Forms.TextBox();
            this.txtTBultos = new System.Windows.Forms.TextBox();
            this.txtTBultosR = new System.Windows.Forms.TextBox();
            this.txtTPesoR = new System.Windows.Forms.TextBox();
            this.txtTGuiaR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNumerodeGuia = new System.Windows.Forms.TextBox();
            this.DgGuias = new System.Windows.Forms.DataGrid();
            this.dataSet1 = new System.Data.DataSet();
            this.lblestado = new System.Windows.Forms.Label();
            this.lblcompleto = new System.Windows.Forms.Label();
            this.txtestado = new System.Windows.Forms.ComboBox();
            this.txtcompleto = new System.Windows.Forms.ComboBox();
            this.btnRegistrarRecepcion = new System.Windows.Forms.Button();
            this.btnCerrarRecepcion = new System.Windows.Forms.Button();
            this.dataGridTableStyle2 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgListadoGuias
            // 
            this.dgListadoGuias.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgListadoGuias.Location = new System.Drawing.Point(-1, 202);
            this.dgListadoGuias.Name = "dgListadoGuias";
            this.dgListadoGuias.Size = new System.Drawing.Size(228, 116);
            this.dgListadoGuias.TabIndex = 30;
            this.dgListadoGuias.TableStyles.Add(this.dataGridTableStyle2);
            this.dgListadoGuias.CurrentCellChanged += new System.EventHandler(this.dgListadoGuias_CurrentCellChanged);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.GridColumnStyles.Add(this.Ident_Guia);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.NumeroGuia);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.Peso);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.Descripcion);
            this.dataGridTableStyle1.MappingName = "ListaGuias";
            // 
            // Ident_Guia
            // 
            this.Ident_Guia.Format = "";
            this.Ident_Guia.FormatInfo = null;
            this.Ident_Guia.HeaderText = "Ident_Guia";
            this.Ident_Guia.MappingName = "Ident_Gaui";
            this.Ident_Guia.Width = 0;
            // 
            // NumeroGuia
            // 
            this.NumeroGuia.Format = "";
            this.NumeroGuia.FormatInfo = null;
            this.NumeroGuia.HeaderText = "Numero Guia";
            this.NumeroGuia.MappingName = "NumeroGuia";
            // 
            // Peso
            // 
            this.Peso.Format = "N2";
            this.Peso.FormatInfo = null;
            this.Peso.HeaderText = "Peso";
            this.Peso.MappingName = "Peso";
            // 
            // Descripcion
            // 
            this.Descripcion.Format = "Descripcion";
            this.Descripcion.FormatInfo = null;
            this.Descripcion.MappingName = "Descripcion";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Regresar";
            // 
            // lblmanifiesto
            // 
            this.lblmanifiesto.Location = new System.Drawing.Point(5, 10);
            this.lblmanifiesto.Name = "lblmanifiesto";
            this.lblmanifiesto.Size = new System.Drawing.Size(62, 20);
            this.lblmanifiesto.Text = "Manifiesto";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(119, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 20);
            this.label2.Text = "Linea Aerea";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 20);
            this.label3.Text = "Nro. Vuelo";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(117, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.Text = "Fecha Arribo";
            // 
            // txtmanifiesto
            // 
            this.txtmanifiesto.Location = new System.Drawing.Point(5, 25);
            this.txtmanifiesto.Name = "txtmanifiesto";
            this.txtmanifiesto.ReadOnly = true;
            this.txtmanifiesto.Size = new System.Drawing.Size(102, 21);
            this.txtmanifiesto.TabIndex = 7;
            // 
            // txtLineaAerea
            // 
            this.txtLineaAerea.Location = new System.Drawing.Point(117, 26);
            this.txtLineaAerea.Name = "txtLineaAerea";
            this.txtLineaAerea.ReadOnly = true;
            this.txtLineaAerea.Size = new System.Drawing.Size(100, 21);
            this.txtLineaAerea.TabIndex = 8;
            // 
            // txtNumeroVuelo
            // 
            this.txtNumeroVuelo.Location = new System.Drawing.Point(5, 68);
            this.txtNumeroVuelo.Name = "txtNumeroVuelo";
            this.txtNumeroVuelo.ReadOnly = true;
            this.txtNumeroVuelo.Size = new System.Drawing.Size(102, 21);
            this.txtNumeroVuelo.TabIndex = 9;
            // 
            // txtFechaArribo
            // 
            this.txtFechaArribo.Location = new System.Drawing.Point(117, 68);
            this.txtFechaArribo.Name = "txtFechaArribo";
            this.txtFechaArribo.ReadOnly = true;
            this.txtFechaArribo.Size = new System.Drawing.Size(100, 21);
            this.txtFechaArribo.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 20);
            this.label5.Text = "T. Guias";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.Text = "T. Peso";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 20);
            this.label7.Text = "T. Bultos";
            // 
            // txtTGuia
            // 
            this.txtTGuia.Location = new System.Drawing.Point(57, 94);
            this.txtTGuia.Name = "txtTGuia";
            this.txtTGuia.ReadOnly = true;
            this.txtTGuia.Size = new System.Drawing.Size(45, 21);
            this.txtTGuia.TabIndex = 14;
            // 
            // txtTPeso
            // 
            this.txtTPeso.Location = new System.Drawing.Point(57, 121);
            this.txtTPeso.Name = "txtTPeso";
            this.txtTPeso.ReadOnly = true;
            this.txtTPeso.Size = new System.Drawing.Size(45, 21);
            this.txtTPeso.TabIndex = 15;
            // 
            // txtTBultos
            // 
            this.txtTBultos.Location = new System.Drawing.Point(57, 148);
            this.txtTBultos.Name = "txtTBultos";
            this.txtTBultos.ReadOnly = true;
            this.txtTBultos.Size = new System.Drawing.Size(45, 21);
            this.txtTBultos.TabIndex = 16;
            // 
            // txtTBultosR
            // 
            this.txtTBultosR.Location = new System.Drawing.Point(169, 148);
            this.txtTBultosR.Name = "txtTBultosR";
            this.txtTBultosR.ReadOnly = true;
            this.txtTBultosR.Size = new System.Drawing.Size(48, 21);
            this.txtTBultosR.TabIndex = 22;
            // 
            // txtTPesoR
            // 
            this.txtTPesoR.Location = new System.Drawing.Point(169, 121);
            this.txtTPesoR.Name = "txtTPesoR";
            this.txtTPesoR.ReadOnly = true;
            this.txtTPesoR.Size = new System.Drawing.Size(48, 21);
            this.txtTPesoR.TabIndex = 21;
            // 
            // txtTGuiaR
            // 
            this.txtTGuiaR.Location = new System.Drawing.Point(169, 94);
            this.txtTGuiaR.Name = "txtTGuiaR";
            this.txtTGuiaR.ReadOnly = true;
            this.txtTGuiaR.Size = new System.Drawing.Size(48, 21);
            this.txtTGuiaR.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(106, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.Text = "T. Bultos R";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(106, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.Text = "T. Peso R";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(106, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.Text = "T. Guias R";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 20);
            this.label11.Text = "Nro. Guia";
            // 
            // txtNumerodeGuia
            // 
            this.txtNumerodeGuia.Location = new System.Drawing.Point(70, 175);
            this.txtNumerodeGuia.Name = "txtNumerodeGuia";
            this.txtNumerodeGuia.Size = new System.Drawing.Size(147, 21);
            this.txtNumerodeGuia.TabIndex = 27;
            // 
            // DgGuias
            // 
            this.DgGuias.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DgGuias.DataSource = this.dataSet1;
            this.DgGuias.Location = new System.Drawing.Point(5, 207);
            this.DgGuias.Name = "DgGuias";
            this.DgGuias.Size = new System.Drawing.Size(282, 67);
            this.DgGuias.TabIndex = 30;
            this.DgGuias.Visible = false;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Namespace = "";
            this.dataSet1.Prefix = "";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblestado
            // 
            this.lblestado.Location = new System.Drawing.Point(-3, 347);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(48, 20);
            this.lblestado.Text = "Estado";
            // 
            // lblcompleto
            // 
            this.lblcompleto.Location = new System.Drawing.Point(106, 348);
            this.lblcompleto.Name = "lblcompleto";
            this.lblcompleto.Size = new System.Drawing.Size(68, 20);
            this.lblcompleto.Text = "Completo?";
            // 
            // txtestado
            // 
            this.txtestado.Items.Add("Bueno");
            this.txtestado.Items.Add("Malo");
            this.txtestado.Location = new System.Drawing.Point(37, 345);
            this.txtestado.Name = "txtestado";
            this.txtestado.Size = new System.Drawing.Size(66, 22);
            this.txtestado.TabIndex = 44;
            this.txtestado.TabStop = false;
            // 
            // txtcompleto
            // 
            this.txtcompleto.Items.Add("Si");
            this.txtcompleto.Items.Add("No");
            this.txtcompleto.Location = new System.Drawing.Point(169, 345);
            this.txtcompleto.Name = "txtcompleto";
            this.txtcompleto.Size = new System.Drawing.Size(52, 22);
            this.txtcompleto.TabIndex = 45;
            // 
            // btnRegistrarRecepcion
            // 
            this.btnRegistrarRecepcion.Location = new System.Drawing.Point(5, 379);
            this.btnRegistrarRecepcion.Name = "btnRegistrarRecepcion";
            this.btnRegistrarRecepcion.Size = new System.Drawing.Size(84, 20);
            this.btnRegistrarRecepcion.TabIndex = 46;
            this.btnRegistrarRecepcion.Text = "Registrar";
            this.btnRegistrarRecepcion.Click += new System.EventHandler(this.btnRegistrarRecepcion_Click);
            // 
            // btnCerrarRecepcion
            // 
            this.btnCerrarRecepcion.Location = new System.Drawing.Point(95, 379);
            this.btnCerrarRecepcion.Name = "btnCerrarRecepcion";
            this.btnCerrarRecepcion.Size = new System.Drawing.Size(122, 20);
            this.btnCerrarRecepcion.TabIndex = 60;
            this.btnCerrarRecepcion.Text = "Cerrar Recepción";

            // 
            // dataGridTableStyle2
            // 
            this.dataGridTableStyle2.GridColumnStyles.Add(this.dataGridTextBoxColumn1);
            this.dataGridTableStyle2.GridColumnStyles.Add(this.dataGridTextBoxColumn2);
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.HeaderText = "ID";
            this.dataGridTextBoxColumn1.MappingName = "Ident_Guia";
            this.dataGridTextBoxColumn1.NullText = "";
            // 
            // dataGridTextBoxColumn2
            // 
            this.dataGridTextBoxColumn2.Format = "";
            this.dataGridTextBoxColumn2.FormatInfo = null;
            this.dataGridTextBoxColumn2.HeaderText = "Numero Guia";
            this.dataGridTextBoxColumn2.MappingName = "NumeroGuia";
            this.dataGridTextBoxColumn2.NullText = "";

            // 
            // RecepcionGuias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnCerrarRecepcion);
            this.Controls.Add(this.btnRegistrarRecepcion);
            this.Controls.Add(this.txtcompleto);
            this.Controls.Add(this.txtestado);
            this.Controls.Add(this.lblcompleto);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.DgGuias);
            this.Controls.Add(this.txtNumerodeGuia);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTBultosR);
            this.Controls.Add(this.txtTPesoR);
            this.Controls.Add(this.txtTGuiaR);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTBultos);
            this.Controls.Add(this.txtTPeso);
            this.Controls.Add(this.txtTGuia);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFechaArribo);
            this.Controls.Add(this.txtNumeroVuelo);
            this.Controls.Add(this.txtLineaAerea);
            this.Controls.Add(this.txtmanifiesto);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblmanifiesto);
            this.Menu = this.mainMenu1;
            this.Name = "RecepcionGuias";
            this.Text = "Recepcion de Guias";
            this.Load += new System.EventHandler(this.RecepcionGuias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblmanifiesto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtmanifiesto;
        private System.Windows.Forms.TextBox txtLineaAerea;
        private System.Windows.Forms.TextBox txtNumeroVuelo;
        private System.Windows.Forms.TextBox txtFechaArribo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTGuia;
        private System.Windows.Forms.TextBox txtTPeso;
        private System.Windows.Forms.TextBox txtTBultos;
        private System.Windows.Forms.TextBox txtTBultosR;
        private System.Windows.Forms.TextBox txtTPesoR;
        private System.Windows.Forms.TextBox txtTGuiaR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNumerodeGuia;
        private System.Windows.Forms.DataGrid DgGuias;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Label lblcompleto;
        private System.Windows.Forms.ComboBox txtestado;
        private System.Windows.Forms.ComboBox txtcompleto;
        private System.Windows.Forms.Button btnRegistrarRecepcion;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.Button btnCerrarRecepcion;

        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn Ident_Guia;
        private System.Windows.Forms.DataGridTextBoxColumn NumeroGuia;
        private System.Windows.Forms.DataGridTextBoxColumn Peso;
        private System.Windows.Forms.DataGridTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGrid dgListadoGuias;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle2;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
    }
}