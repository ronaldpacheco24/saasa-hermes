﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._2DescargaManifiesto
{
    partial class DescargaManifiesto
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.IniciarDescargarManifiesto = new System.Windows.Forms.Button();
            this.txtManifiesto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Regresar";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // IniciarDescargarManifiesto
            // 
            this.IniciarDescargarManifiesto.Location = new System.Drawing.Point(14, 220);
            this.IniciarDescargarManifiesto.Name = "IniciarDescargarManifiesto";
            this.IniciarDescargarManifiesto.Size = new System.Drawing.Size(209, 28);
            this.IniciarDescargarManifiesto.TabIndex = 5;
            this.IniciarDescargarManifiesto.Text = "Iniciar descarga manifiesto";
            this.IniciarDescargarManifiesto.Click += new System.EventHandler(this.IniciarDescargaManifiesto_Click);
            // 
            // txtManifiesto
            // 
            this.txtManifiesto.Location = new System.Drawing.Point(14, 57);
            this.txtManifiesto.Name = "txtManifiesto";
            this.txtManifiesto.Size = new System.Drawing.Size(189, 21);            
            this.txtManifiesto.TabIndex = 4;            
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.Text = "Nro. Manifiesto";
            // 
            // DescargaManifiesto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.IniciarDescargarManifiesto);
            this.Controls.Add(this.txtManifiesto);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "DescargaManifiesto";
            this.Text = "DescargaManifiesto";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button IniciarDescargarManifiesto;
        private System.Windows.Forms.TextBox txtManifiesto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuItem menuItem1;
    }
}