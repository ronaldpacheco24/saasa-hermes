﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._1LlegadaManifiesto
{
    partial class RecepcionManifiesto
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.lblmanifiesto = new System.Windows.Forms.Label();
            this.txtCodigoManifiesto = new System.Windows.Forms.TextBox();
            this.btnRegistrarLlegadaManifiesto = new System.Windows.Forms.Button();
            this.lblresultado = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Regresar";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // lblmanifiesto
            // 
            this.lblmanifiesto.Location = new System.Drawing.Point(14, 20);
            this.lblmanifiesto.Name = "lblmanifiesto";
            this.lblmanifiesto.Size = new System.Drawing.Size(100, 20);
            this.lblmanifiesto.Text = "Nro. Manifiesto";
            // 
            // txtCodigoManifiesto
            // 
            this.txtCodigoManifiesto.Location = new System.Drawing.Point(14, 55);
            this.txtCodigoManifiesto.Name = "txtCodigoManifiesto";
            this.txtCodigoManifiesto.Size = new System.Drawing.Size(189, 21);
            this.txtCodigoManifiesto.TabIndex = 1;
            // 
            // btnRegistrarLlegadaManifiesto
            // 
            this.btnRegistrarLlegadaManifiesto.Location = new System.Drawing.Point(57, 216);
            this.btnRegistrarLlegadaManifiesto.Name = "btnRegistrarLlegadaManifiesto";
            this.btnRegistrarLlegadaManifiesto.Size = new System.Drawing.Size(170, 28);
            this.btnRegistrarLlegadaManifiesto.TabIndex = 2;
            this.btnRegistrarLlegadaManifiesto.Text = "Registrar Llegada";
            this.btnRegistrarLlegadaManifiesto.Click += new System.EventHandler(this.btnRegistrarLlegadaManifiesto_Click);
            // 
            // lblresultado
            // 
            this.lblresultado.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblresultado.ForeColor = System.Drawing.Color.Red;
            this.lblresultado.Location = new System.Drawing.Point(25, 96);
            this.lblresultado.Name = "lblresultado";
            this.lblresultado.Size = new System.Drawing.Size(162, 38);
            // 
            // RecepcionManifiesto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblresultado);
            this.Controls.Add(this.btnRegistrarLlegadaManifiesto);
            this.Controls.Add(this.txtCodigoManifiesto);
            this.Controls.Add(this.lblmanifiesto);
            this.Menu = this.mainMenu1;
            this.Name = "RecepcionManifiesto";
            this.Text = "Recepcion Manifiesto";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblmanifiesto;
        private System.Windows.Forms.TextBox txtCodigoManifiesto;
        private System.Windows.Forms.Button btnRegistrarLlegadaManifiesto;
        private System.Windows.Forms.Label lblresultado;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuItem menuItem1;
    }
}