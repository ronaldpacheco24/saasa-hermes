﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._1LlegadaManifiesto
{
    public partial class RecepcionManifiesto : Form
    {
        public RecepcionManifiesto()
        {
            InitializeComponent();
        }

        private void btnRegistrarLlegadaManifiesto_Click(object sender, EventArgs e)
        {
            ManifiestoBE objManifiesto= new ManifiestoBE();
            objManifiesto.NumeroManifiesto = txtCodigoManifiesto.Text.Trim();
            var manifiesto = ManifiestoBL.Instancia.RegistrarLlegadaManifiesto(objManifiesto);            
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            //ManifiestoBE objManifiesto = new ManifiestoBE();
            //objManifiesto.CodigoManifiesto = txtCodigoManifiesto.Text.Trim();
            //var manifiesto = ManifiestoBL.Instancia.RegitrarManifiesto(objManifiesto);
            //lblresultado.Text = manifiesto.Estado;

            //PaginaPrincipal paginaPrincipal = new PaginaPrincipal();
            //paginaPrincipal.Show();
        }
    }
}