﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.UI.Importacion._3Ubicacion;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._5Devolucion
{
    public partial class PendienteDevolucion : Form
    {

        public List<DataGridPendienteDevolucionImpoBE> ListaDevueltos { get; set; }
        public List<DataGridPendienteDevolucionImpoBE> ListaPendienteDevolucion { get; set; }

        public PendienteDevolucion()
        {
            InitializeComponent();
        }

        public int ObtenerMinutos(DateTime FechaInicial, DateTime FechaFinal)
        {
            int Mes, Dia, Hora, Minuto;

            Mes = FechaFinal.Month - FechaInicial.Month;
            Dia = FechaFinal.Day - FechaInicial.Day;
            Hora = FechaFinal.Hour - FechaInicial.Hour;
            Minuto = FechaFinal.Minute - FechaInicial.Minute;

            int MesFinal = FechaFinal.Month;
            int MesInicial = FechaInicial.Month;
            int ObtenerDiasMeses = 0;
            int DiasxMeses = 0;
            for (int i = 0; i < Mes; i++)
            {
                if (MesFinal > MesInicial)
                {
                    ObtenerDiasMeses = DateTime.DaysInMonth(FechaInicial.Year, FechaInicial.Month);
                    DiasxMeses = ObtenerDiasMeses + DiasxMeses;
                    MesInicial++;
                }
            }
            int DiaxHora = 0;
            int HoraxMinuto = 0;
            Dia = DiasxMeses + Dia;
            DiaxHora = (Dia * 24);
            HoraxMinuto = DiaxHora + Hora;
            HoraxMinuto = HoraxMinuto * 60;
            Minuto = HoraxMinuto + Minuto;

            return Minuto;
        }

        public void CargarDatos()
        {
            List<DataGridPendienteDevolucionImpoBE> DataGridList = new List<DataGridPendienteDevolucionImpoBE>();
            List<UnidadAlmacenamientoBE> DevolucionUAList = new List<UnidadAlmacenamientoBE>();
            DevolucionUAList = MovilizacionBL.Instancia.BuscarPendientesDevolucion();

            foreach (var item in DevolucionUAList)
            {
                DataGridPendienteDevolucionImpoBE DataGrid = new DataGridPendienteDevolucionImpoBE();
                DataGrid.NGuia = item.NumeroGuia;
                DataGrid.NUA = item.CodigoUA;
                DataGrid.UbiActual = item.UbicacionActual;
                DataGrid.UbiAsignada = item.UbicacionAsignada;
                DataGrid.Bultos = item.Bultos;
                DataGrid.Peso = item.Peso;
                DataGridList.Add(DataGrid);
            }
            ListaPendienteDevolucion = DataGridList;
        }

        public void CargarDataGrid()
        {
            dgPendienteDevolucion.DataSource = ListaPendienteDevolucion;
        }

        private void PendienteDevoluciones_Load(object sender, EventArgs e)
        {
            CargarDataGrid();
        }

        private void dgPendienteDevolucion_CurrentCellChanged(object sender, EventArgs e)
        {
            dgPendienteDevolucion.Select(dgPendienteDevolucion.CurrentRowIndex);
        }

        private void btnTomar_Click(object sender, EventArgs e)
        {
            int fila = dgPendienteDevolucion.CurrentCell.RowNumber;
            List<DataGridPendienteDevolucionImpoBE> DGPM = new List<DataGridPendienteDevolucionImpoBE>();
            int count = 0;

            foreach (var itemLP in ListaPendienteDevolucion)
            {
                if (count == fila)
                {
                    MovilizacionBL.Instancia.RegistrarTomaUA(itemLP.NUA, 0, "Tomado");
                    DataGridPendienteDevolucionImpoBE UAr = ListaPendienteDevolucion.Select(z => z).Where(item => item.NUA == itemLP.NUA).SingleOrDefault();
                    ListaPendienteDevolucion.Remove(UAr);
                }
                count++;
            }
            CargarDataGrid();
        }

    }
}