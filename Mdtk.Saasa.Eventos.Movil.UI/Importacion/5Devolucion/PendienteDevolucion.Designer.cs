﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._5Devolucion
{
    partial class PendienteDevolucion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.dgPendienteDevolucion = new System.Windows.Forms.DataGrid();
            this.btnTomar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dgPendienteDevolucion
            // 
            this.dgPendienteDevolucion.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgPendienteDevolucion.Location = new System.Drawing.Point(3, 28);
            this.dgPendienteDevolucion.Name = "dgPendienteDevolucion";
            this.dgPendienteDevolucion.Size = new System.Drawing.Size(237, 89);
            this.dgPendienteDevolucion.TabIndex = 0;
            this.dgPendienteDevolucion.CurrentCellChanged += new System.EventHandler(this.dgPendienteDevolucion_CurrentCellChanged);
            // 
            // btnTomar
            // 
            this.btnTomar.Location = new System.Drawing.Point(165, 123);
            this.btnTomar.Name = "btnTomar";
            this.btnTomar.Size = new System.Drawing.Size(72, 20);
            this.btnTomar.TabIndex = 2;
            this.btnTomar.Text = "Tomar";
            // 
            // PendienteDevolucion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnTomar);
            this.Controls.Add(this.dgPendienteDevolucion);
            this.Menu = this.mainMenu1;
            this.Name = "PendienteDevolucion";
            this.Text = "PendienteDevoluciones";
            this.Load += new System.EventHandler(this.PendienteDevoluciones_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dgPendienteDevolucion;
        private System.Windows.Forms.Button btnTomar;
    }
}