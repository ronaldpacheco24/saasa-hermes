﻿namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._6Salida
{
    partial class SalidaUA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.Data = new System.Windows.Forms.DataGrid();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.btnValidar = new System.Windows.Forms.Button();
            this.btnParcial = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtBultos = new System.Windows.Forms.TextBox();
            this.lblBultos = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.btnParcializar = new System.Windows.Forms.Button();
            this.btnNova = new System.Windows.Forms.Button();
            this.lblCodigoUA = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Actualizar";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Volver";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // Data
            // 
            this.Data.BackgroundColor = System.Drawing.Color.White;
            this.Data.Location = new System.Drawing.Point(0, 48);
            this.Data.Name = "Data";
            this.Data.Size = new System.Drawing.Size(281, 118);
            this.Data.TabIndex = 0;
            this.Data.Visible = false;
            this.Data.CurrentCellChanged += new System.EventHandler(this.Data_CurrentCellChanged);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(53, 12);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(97, 21);
            this.txtCodigo.TabIndex = 24;
            // 
            // lblPlaca
            // 
            this.lblPlaca.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPlaca.Location = new System.Drawing.Point(3, 12);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(44, 20);
            this.lblPlaca.Text = "Placa";
            // 
            // btnValidar
            // 
            this.btnValidar.Location = new System.Drawing.Point(168, 13);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(101, 20);
            this.btnValidar.TabIndex = 26;
            this.btnValidar.Text = "Validar Placa";
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // btnParcial
            // 
            this.btnParcial.Location = new System.Drawing.Point(78, 202);
            this.btnParcial.Name = "btnParcial";
            this.btnParcial.Size = new System.Drawing.Size(101, 20);
            this.btnParcial.TabIndex = 27;
            this.btnParcial.Text = "Retiro Parcial";
            this.btnParcial.Click += new System.EventHandler(this.btnParcial_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(148, 172);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(101, 20);
            this.btnConfirmar.TabIndex = 28;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(148, 198);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(97, 21);
            this.txtPeso.TabIndex = 30;
            this.txtPeso.Visible = false;
            // 
            // txtBultos
            // 
            this.txtBultos.Location = new System.Drawing.Point(148, 171);
            this.txtBultos.Name = "txtBultos";
            this.txtBultos.Size = new System.Drawing.Size(97, 21);
            this.txtBultos.TabIndex = 31;
            this.txtBultos.Visible = false;
            // 
            // lblBultos
            // 
            this.lblBultos.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblBultos.Location = new System.Drawing.Point(58, 171);
            this.lblBultos.Name = "lblBultos";
            this.lblBultos.Size = new System.Drawing.Size(79, 20);
            this.lblBultos.Text = "Nro Bultos";
            this.lblBultos.Visible = false;
            // 
            // lblPeso
            // 
            this.lblPeso.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblPeso.Location = new System.Drawing.Point(93, 199);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(44, 20);
            this.lblPeso.Text = "Peso";
            this.lblPeso.Visible = false;
            // 
            // btnParcializar
            // 
            this.btnParcializar.Location = new System.Drawing.Point(148, 225);
            this.btnParcializar.Name = "btnParcializar";
            this.btnParcializar.Size = new System.Drawing.Size(97, 20);
            this.btnParcializar.TabIndex = 33;
            this.btnParcializar.Text = "Ok";
            this.btnParcializar.Visible = false;
            this.btnParcializar.Click += new System.EventHandler(this.btnParcializar_Click);
            // 
            // btnNova
            // 
            this.btnNova.Location = new System.Drawing.Point(15, 172);
            this.btnNova.Name = "btnNova";
            this.btnNova.Size = new System.Drawing.Size(101, 20);
            this.btnNova.TabIndex = 37;
            this.btnNova.Text = "No va";
            this.btnNova.Visible = false;
            this.btnNova.Click += new System.EventHandler(this.btnNova_Click);
            // 
            // lblCodigoUA
            // 
            this.lblCodigoUA.Location = new System.Drawing.Point(37, 103);
            this.lblCodigoUA.Name = "lblCodigoUA";
            this.lblCodigoUA.Size = new System.Drawing.Size(100, 20);
            this.lblCodigoUA.Visible = false;
            // 
            // SalidaUA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnNova);
            this.Controls.Add(this.btnParcializar);
            this.Controls.Add(this.btnParcial);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.lblBultos);
            this.Controls.Add(this.txtBultos);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.lblPlaca);
            this.Controls.Add(this.Data);
            this.Controls.Add(this.lblCodigoUA);
            this.Menu = this.mainMenu1;
            this.Name = "SalidaUA";
            this.Text = "SalidaUA";
            this.Load += new System.EventHandler(this.SalidaUA_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid Data;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.Button btnParcial;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.TextBox txtBultos;
        private System.Windows.Forms.Label lblBultos;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Button btnParcializar;
        private System.Windows.Forms.Button btnNova;
        private System.Windows.Forms.Label lblCodigoUA;

    }
}