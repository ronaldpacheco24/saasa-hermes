﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Mdtk.Saasa.Eventos.Movil.BL;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.UI.Importacion._6Salida
{
    public partial class SalidaUA : Form
    {
        public List<UnidadAlmacenamientoBE> ListaUA { get; set; }

        public List<DataGridSalidaUABE> ListaOrdenes { get; set; }

        public SalidaUA()
        {
            InitializeComponent();
        }

        public void CargarDataGrid()
        {
            ObtenerDatos(txtCodigo.Text);
            Data.DataSource = ListaOrdenes;
        }

        private void ObtenerDatos(string NumeroPlaca)
        {
            List<OrdenRetiroBE> OrdenRetiroList = new List<OrdenRetiroBE>();
            OrdenRetiroList = OrdenRetiroBL.Instancia.BuscarOrdenRetiro(NumeroPlaca);
            List<UnidadAlmacenamientoBE> UAList = new List<UnidadAlmacenamientoBE>();
            //List<DataGridSalidaUABE> DataGridSalida = new List<DataGridSalidaUABE>();

            foreach (var item in OrdenRetiroList)
            {
                DataGridSalidaUABE DataGrid = new DataGridSalidaUABE();
                OrdenRetiroBE mvOrdenRetiro = new OrdenRetiroBE();
                DataGrid.OrdenRetiro = item.CodigoRetiro;

                foreach (var item2 in item.GuiaList)
                {
                    GuiaBE mvGuia = new GuiaBE();
                    mvGuia.NumeroGuia = item2.NumeroGuia;

                    foreach (var item3 in item2.ListaUA)
                    {
                        UnidadAlmacenamientoBE mvUA = new UnidadAlmacenamientoBE();
                        DataGrid.OrdenRetiro = item.CodigoRetiro;
                        DataGrid.CodigoUA = item3.CodigoUA;
                        DataGrid.NumeroBultos = item3.Bultos;
                        DataGrid.Peso = item3.Peso;
                        DataGrid.UbicacionActual = item3.UbicacionActual;
                        DataGrid.Estado = "C";

                        mvUA.CodigoUA = item3.CodigoUA;
                        mvUA.Estado = "Completo";
                        mvUA.EstadoSalida = "Salio";
                        mvUA.Peso = item3.Peso;
                        mvUA.Bultos = item3.Bultos;
                        mvUA.PesoRetirado = 0;
                        mvUA.UbicacionActual = item3.UbicacionActual;
                        mvUA.BultosRetirados = 0;
                        UAList.Add(mvUA);
                    }
                    mvOrdenRetiro.GuiaList.Add(mvGuia);
                }
                //DataGridSalida.Add(DataGrid);
                ListaUA = UAList;
                ListaOrdenes.Add(DataGrid);
            }
        }

        private void SalidaUA_Load(object sender, EventArgs e)
        {
            Data.Hide();
            btnConfirmar.Hide();
            btnParcial.Hide();
            btnParcializar.Hide();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            SalidaUA salida = new SalidaUA();
            salida.Show();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            PaginaPrincipal form = new PaginaPrincipal();
            form.Show();
        }

        private void btnParcial_Click(object sender, EventArgs e)
        {
            var IndiceFilaSeleccionada = Data.CurrentRowIndex;

            if (IndiceFilaSeleccionada < 0)
            {
                MessageBox.Show("Debe Seleccionar una UA");
            }
            else
            {
                btnConfirmar.Hide();
                btnParcial.Hide();
                btnNova.Hide();
                lblBultos.Show();
                lblPeso.Show();
                txtBultos.Show();
                txtPeso.Show();
                btnParcializar.Show();

                int count = 0;
                foreach (var item in ListaOrdenes)
                {
                    if (IndiceFilaSeleccionada == count)
                    {
                        txtBultos.Text = Convert.ToString(item.NumeroBultos);
                        txtPeso.Text = Convert.ToString(item.Peso);
                        lblCodigoUA.Text = item.CodigoUA;
                    }
                    count++;
                }
            }
            
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            foreach (var item in ListaUA)
            {
                UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();
                UA.CodigoUA = item.CodigoUA;
                UA.Estado = item.Estado;
                UA.BultosRetirados = item.BultosRetirados;
                UA.PesoRetirado = item.PesoRetirado;
                UA.Peso = item.Peso;
                UA.Bultos = item.Bultos;
                UA.UbicacionActual = item.UbicacionActual;
                UA.EstadoSalida = item.EstadoSalida;
                UnidadAlmacenamientoBL.Instancia.ActualizarUnidadAlmacenamiento(UA);
            }

            MessageBox.Show("Las UA´s Salieron correctamente.");        
        }

        private void btnParcializar_Click(object sender, EventArgs e)
        {
            lblBultos.Hide();
            lblPeso.Hide();
            txtBultos.Hide();
            txtPeso.Hide();
            btnParcializar.Hide();
            btnConfirmar.Show();
            btnParcial.Show();
            btnNova.Show();

            var IndiceFilaSeleccionada = Data.CurrentRowIndex;
            List<DataGridSalidaUABE> DGSUA = new List<DataGridSalidaUABE>();
            //List<UnidadAlmacenamientoBE> UAList = new List<UnidadAlmacenamientoBE>();


            foreach (var itemLO in ListaOrdenes)
            {
                DataGridSalidaUABE DGOrden = new DataGridSalidaUABE();
                UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();

                DGOrden.CodigoUA = itemLO.CodigoUA;
                DGOrden.OrdenRetiro = itemLO.OrdenRetiro;
                DGOrden.UbicacionActual = itemLO.Estado;
                if (lblCodigoUA.Equals(itemLO.CodigoUA))
                {
                    if (itemLO.Estado != "N")
                    {
                        DGOrden.NumeroBultos = Convert.ToInt32(txtBultos.Text);
                        DGOrden.Peso = Convert.ToDecimal(itemLO.Peso);

                        if (DGOrden.NumeroBultos != itemLO.NumeroBultos | DGOrden.Peso != itemLO.Peso)
                        {
                            DGOrden.Estado = "P";

                            UnidadAlmacenamientoBE UAr = ListaUA.Select(z => z).Where(item => item.CodigoUA == itemLO.CodigoUA).SingleOrDefault();
                            ListaUA.Remove(UAr);
                            UA.CodigoUA = itemLO.CodigoUA;
                            UA.Bultos = itemLO.NumeroBultos;
                            UA.Peso = itemLO.Peso;
                            UA.UbicacionActual = itemLO.UbicacionActual;
                            UA.PesoRetirado = DGOrden.Peso;
                            UA.BultosRetirados = DGOrden.NumeroBultos;
                            UA.EstadoSalida = "Salio";
                            UA.Estado = "Parcial";
                            ListaUA.Add(UA);
                        }
                        else
                        {
                            DGOrden.Estado = itemLO.Estado;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Esta UA No va a Salir");
                    }
                }
                else
                {
                    DGOrden.NumeroBultos = itemLO.NumeroBultos;
                    DGOrden.Peso = itemLO.Peso;
                    DGOrden.Estado = itemLO.Estado;

                    UA.PesoRetirado = 0;
                    UA.BultosRetirados = 0;
                    UA.Estado = "Completo";
                }
                //UAList.Add(UA);
                DGSUA.Add(DGOrden);
            }
            //ListaUA.Clear();
            //ListaUA = UAList;
            ListaOrdenes.Clear();
            ListaOrdenes = DGSUA;
            CargarDataGrid();
            
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {
            CargarDataGrid();
            Data.Show();
            btnParcial.Show();
            btnConfirmar.Show();
            btnNova.Show();

        }
        private void Data_CurrentCellChanged(object sender, EventArgs e)
        {
            Data.Select(Data.CurrentRowIndex);
        }

        private void btnNova_Click(object sender, EventArgs e)
        {
            var IndiceFilaSeleccionada = Data.CurrentRowIndex;
            if (IndiceFilaSeleccionada < 0)
            {
                MessageBox.Show("Debe Seleccionar una UA");
            }
            else
            {
                List<DataGridSalidaUABE> DGSUA = new List<DataGridSalidaUABE>();
                //List<UnidadAlmacenamientoBE> UAList = new List<UnidadAlmacenamientoBE>();

                int count = 0;
                foreach (var itemLO in ListaOrdenes)
                {
                    DataGridSalidaUABE DGOrden = new DataGridSalidaUABE();
                    UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();

                    DGOrden.CodigoUA = itemLO.CodigoUA;
                    DGOrden.NumeroBultos = itemLO.NumeroBultos;
                    DGOrden.OrdenRetiro = itemLO.OrdenRetiro;
                    DGOrden.Peso = itemLO.Peso;
                    DGOrden.UbicacionActual = itemLO.Estado;

                    
                    if (IndiceFilaSeleccionada == count)
                    {
                        UnidadAlmacenamientoBE UAr = ListaUA.Select(z => z).Where(item =>  item.CodigoUA == itemLO.CodigoUA).SingleOrDefault();
                        DGOrden.Estado = "N";                        
                        UA.EstadoSalida = "No va";
                        UA.CodigoUA = itemLO.CodigoUA;
                        UA.Bultos = itemLO.NumeroBultos;
                        UA.Peso = itemLO.Peso;
                        UA.PesoRetirado = UAr.PesoRetirado;
                        UA.BultosRetirados = UAr.BultosRetirados;
                        UA.UbicacionActual = itemLO.UbicacionActual;
                        ListaUA.Remove(UAr);
                        ListaUA.Add(UA);                        
                    }
                    else
                    {
                        //UnidadAlmacenamientoBE UAr = ListaUA.Select(z => z).Where(item => item.CodigoUA == itemLO.CodigoUA).SingleOrDefault();
                        UA.EstadoSalida = "Salio";
                        DGOrden.Estado = itemLO.Estado;
                    }
                    

                    //UAList.Add(UA);
                    DGSUA.Add(DGOrden);
                    count++;
                }
                //ListaUA.Clear();
                //ListaUA = UAList;
                ListaOrdenes.Clear();
                ListaOrdenes = DGSUA;
                CargarDataGrid();
            }
        }
    }
}