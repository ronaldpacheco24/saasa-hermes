﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.DO;
using Mdtk.Saasa.Eventos.Servicio.BE;

namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class EventoBL
    {
        #region Singleton
        private static EventoBL _Instancia = null;
        public static EventoBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new EventoBL(); return _Instancia; }
        }
        #endregion

        public void RegistrarEvento(EventoBE evento)
        {
            EventoDO.Instancia.RegistrarEvento(evento);
        }
    }
}
