﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.DO;
namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class SalidaBL
    {
        #region Singleton
        private static SalidaBL _Instancia = null;
        public static SalidaBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new SalidaBL(); return _Instancia; }
        }
        #endregion

        public List<OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            return SalidaDO.Instancia.BuscarOrdenRetiro(NumeroPlaca);
        }
    }
}
