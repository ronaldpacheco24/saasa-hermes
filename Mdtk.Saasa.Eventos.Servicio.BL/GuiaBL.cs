﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.DO;

namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class GuiaBL
    {
        #region Singleton
        private static GuiaBL _Instancia = null;
        public static GuiaBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new GuiaBL(); return _Instancia; }
        }
        #endregion

        #region Registro
        public void RegistrarGuia(List<GuiaBE> listaGuias)
        {
            foreach(var item in listaGuias)
            {
                RegistrarGuia(item);
            }
        }
        public void RegistrarGuia(GuiaBE guia)
        {
            GuiaDO.Instancia.RegistrarGuia(guia);
        }
        #endregion



        public bool ActualizarRecepcionGuia(GuiaBE guia)
        {
            var affest = GuiaDO.Instancia.ActualizarRecepcionGuia(guia);
            if (affest > 0)
                return true;
            else
                return false;
        }
    }
}
