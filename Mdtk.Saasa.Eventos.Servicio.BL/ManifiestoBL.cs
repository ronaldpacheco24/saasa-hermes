﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.DO;

namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class ManifiestoBL
    {

        #region Singleton
        private static ManifiestoBL _Instancia = null;
        public static ManifiestoBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new ManifiestoBL(); return _Instancia; }
        }
        #endregion

        public ManifiestoBE BuscarManifiestoIMPO(string NumeroManifiesto)
        {
            return BuscarManifiestoIMPO(NumeroManifiesto, "", "");
        }
        public ManifiestoBE BuscarManifiestoIMPO(string NumeroManifiesto, string FechaArribo, string LineaArea)
        {
            var strFechaArribo = "01/01/1900";
            if (FechaArribo == "")
                FechaArribo = strFechaArribo;
            return ManifiestoDO.Instancia.BuscarManifiestoIMPO(NumeroManifiesto, Convert.ToDateTime(FechaArribo), LineaArea);
        }

        public ManifiestoBE BuscarManifiesto(string NumeroManifiesto)
        {
            return ManifiestoDO.Instancia.BuscarManifiesto(NumeroManifiesto);
        }

        public int RegistrarLlegadaManifiesto(string NumeroManifiesto)
        {            
            ManifiestoBE manifiesto = null;

            //Validamos que Manifiesto en Moviles
            manifiesto =  BuscarManifiesto(NumeroManifiesto);           
            if (manifiesto!=null)
                return manifiesto.Ident_Manifiesto;

            //Buscamos que el Manifiesto en el operativo            
            manifiesto = BuscarManifiestoIMPO(NumeroManifiesto);            
            if (manifiesto != null)
                return RegistrarManifiesto(manifiesto);

            //Agregar Evento LLegada de Manifiesto
            return 0; //El Manifiesto no Existe.            
        }

        public bool RegistrarInicioDescargaManifiesto(string NumeroManifiesto)
        { 
            //Actualizamos Estado del Manifiesto
            //Buscamos Manifiesto: Existe Registramos - No Existe return false
            ManifiestoBE manifiesto = BuscarManifiesto(NumeroManifiesto);
            manifiesto.Estado = 10; //Actualizamos 
            if (manifiesto != null)
            {
                return ManifiestoDO.Instancia.ActualizarEstado(manifiesto);
            }
            else {
                return false;
            }
            //Registra evento el inicio de descarga del Manifiesto.
        }
        public bool RegistrarCargaDeManifiesto(string NumeroManifiesto, char tipo, string codigo, int cantidad)
        {
            //Actualizamos Estado del Manifiesto
            //Buscamos Manifiesto: Existe Registramos - No Existe return false
            ManifiestoBE manifiesto = BuscarManifiesto(NumeroManifiesto);
            //manifiesto.Estado = 10; //Actualizamos 
            if (NumeroManifiesto != null)
            {
                return ManifiestoDO.Instancia.RegistrarCargaDeManifiesto(NumeroManifiesto,tipo,codigo,cantidad);
            }
            else
            {
                return false;
            }
            //Registra evento el inicio de descarga del Manifiesto.
        }
        public ManifiestoBE ObtenerCargaDeManifiesto(string NumeroManifiesto)
        {
            //Obtener  Manifiesto
            //Buscamos Manifiesto: Existe Registramos - No Existe return false
            ManifiestoBE manifiesto = new ManifiestoBE();
            if (NumeroManifiesto != null)
            {
                return manifiesto = BuscarManifiesto(NumeroManifiesto);
            }
            else
            {
                return manifiesto;
            }
        }
        public int RegistrarManifiesto(ManifiestoBE manifiesto)
        {
            var Ident_Manifiesto = ManifiestoDO.Instancia.RegistrarManifiesto(manifiesto);           
            GuiaBL.Instancia.RegistrarGuia(manifiesto.ListaGuias);
            return Ident_Manifiesto;
        }

        public void RegistrarEvento(EventoBE Evento)
        { 

        }
    }
}
