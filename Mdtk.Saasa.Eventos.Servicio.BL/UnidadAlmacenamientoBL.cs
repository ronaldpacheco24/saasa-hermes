﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.DO;

namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class UnidadAlmacenamientoBL
    {
        #region Singleton
        private static UnidadAlmacenamientoBL _Instancia = null;
        public static UnidadAlmacenamientoBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new UnidadAlmacenamientoBL(); return _Instancia; }
        }
        #endregion

        public List<UnidadAlmacenamientoBE> BuscarUnidadAlmacenamiento(string NumeroGuia)
        {
            return UnidadAlmacenamientoDO.Instancia.BuscarUnidadAlmacenamiento(NumeroGuia);
        }

        public bool ActualizarUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        {
            return UnidadAlmacenamientoDO.Instancia.ActualizarUnidadAlmacenamiento(UnidadAlmacenamiento);
        }
        //public bool ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        //{
        //    return UnidadAlmacenamientoDO.Instancia.ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamiento);
        //}
    }
}
