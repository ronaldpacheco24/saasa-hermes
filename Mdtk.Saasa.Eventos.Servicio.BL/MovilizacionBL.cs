﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.DO;

namespace Mdtk.Saasa.Eventos.Servicio.BL
{
    public class MovilizacionBL
    {
        #region Singleton
        private static MovilizacionBL _Instancia = null;
        public static MovilizacionBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new MovilizacionBL(); return _Instancia; }
        }
        #endregion

        public List<GuiaBE> BuscarPendientesMovilizacion()
        {
            return MovilizacionDO.Instancia.BuscarPendientesMovilizacion();
        }

        public List<GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            return MovilizacionDO.Instancia.BuscarPendientesMovilizacionOperario(CodigoOperario);
        }

        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            return MovilizacionDO.Instancia.RegistrarTomaGuia(NumeroGuia, idenOperario, Estado);
        }

        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            return MovilizacionDO.Instancia.RegistrarTomaUA(CodigoUA, idenOperario, Estado);
        }

        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            return MovilizacionDO.Instancia.RegistrarMotivoDemora(NumeroGuia, MotivoDemora, Estado);
        }

        public bool DevolverGuia(string NumeroGuia,string Estado)
        {
            return MovilizacionDO.Instancia.DevolverGuia(NumeroGuia, Estado);
        }

        public List<UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            return MovilizacionDO.Instancia.BuscarPendientesDevolucion();
        }
    }
}
