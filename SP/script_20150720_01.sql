USE [Moviles]
GO
/****** Object:  Table [dbo].[PAR_Tabla_Definicion_Campo]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PAR_Tabla_Definicion_Campo](
	[Ident_Campo] [int] IDENTITY(1,1) NOT NULL,
	[CodlCampo] [varchar](30) NULL,
	[CodcCampo] [char](6) NULL,
	[DescmCampo] [varchar](90) NOT NULL,
	[CantCampo] [int] NOT NULL,
	[ValoCampo] [decimal](18, 7) NOT NULL,
	[Ident_Tabla] [int] NOT NULL,
	[CodiOrden] [int] NOT NULL,
	[IndVisibilidad] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PAR_Tabla_Definicion_Campo] ON
INSERT [dbo].[PAR_Tabla_Definicion_Campo] ([Ident_Campo], [CodlCampo], [CodcCampo], [DescmCampo], [CantCampo], [ValoCampo], [Ident_Tabla], [CodiOrden], [IndVisibilidad]) VALUES (1, N'IMPORTACION', N'IMPO  ', N'IMPORTACION', 0, CAST(0.0000000 AS Decimal(18, 7)), 1, 0, 1)
INSERT [dbo].[PAR_Tabla_Definicion_Campo] ([Ident_Campo], [CodlCampo], [CodcCampo], [DescmCampo], [CantCampo], [ValoCampo], [Ident_Tabla], [CodiOrden], [IndVisibilidad]) VALUES (2, N'EXPORTACION', N'EXPO  ', N'EXPORTACION', 0, CAST(0.0000000 AS Decimal(18, 7)), 1, 1, 1)
SET IDENTITY_INSERT [dbo].[PAR_Tabla_Definicion_Campo] OFF
/****** Object:  Table [dbo].[PAR_Tabla_Definicion]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PAR_Tabla_Definicion](
	[Ident_Tabla] [int] IDENTITY(1,1) NOT NULL,
	[CodcTabla] [varchar](6) NULL,
	[DescmTabla] [varchar](90) NOT NULL,
	[IndVisibilidad] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PAR_Tabla_Definicion] ON
INSERT [dbo].[PAR_Tabla_Definicion] ([Ident_Tabla], [CodcTabla], [DescmTabla], [IndVisibilidad]) VALUES (1, N'T_OPE', N'TipoOperacion', 1)
SET IDENTITY_INSERT [dbo].[PAR_Tabla_Definicion] OFF
/****** Object:  Table [dbo].[MOV_SeguimientoUAS]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOV_SeguimientoUAS](
	[Ident_SeguimientoUAS] [int] IDENTITY(1,1) NOT NULL,
	[Ident_UAS] [int] NULL,
	[TipoEvento] [varchar](1) NULL,
	[CodigoEvento] [varchar](10) NULL,
	[FechaEvento] [datetime] NULL,
 CONSTRAINT [PK_MOV_SeguimientoUAS] PRIMARY KEY CLUSTERED 
(
	[Ident_SeguimientoUAS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MOV_SeguimientoManifiesto]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOV_SeguimientoManifiesto](
	[Ident_SeguimientoManifiesto] [int] IDENTITY(1,1) NOT NULL,
	[Ident_Manifiesto] [int] NULL,
	[TipoEvento] [varchar](1) NULL,
	[CodigoEvento] [varchar](10) NULL,
	[FechaEvento] [datetime] NULL,
 CONSTRAINT [PK_MOV_SeguimientoManifiesto_1] PRIMARY KEY CLUSTERED 
(
	[Ident_SeguimientoManifiesto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MOV_SeguimientoGuia]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOV_SeguimientoGuia](
	[Ident_SeguimientoGuia] [int] IDENTITY(1,1) NOT NULL,
	[Ident_Guia] [int] NULL,
	[TipoEvento] [varchar](1) NULL,
	[CodigoEvento] [varchar](40) NULL,
	[FechaEvento] [datetime] NULL,
 CONSTRAINT [PK_MOV_SeguimientoGuia] PRIMARY KEY CLUSTERED 
(
	[Ident_SeguimientoGuia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MOV_Manifiesto]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOV_Manifiesto](
	[Ident_Manifiesto] [int] IDENTITY(1,1) NOT NULL,
	[NumeroManifiesto] [varchar](10) NULL,
	[LineaAerea] [varchar](40) NULL,
	[NumeroVuelo] [varchar](10) NULL,
	[FechaArribo] [datetime] NULL,
	[Estado] [int] NULL,
 CONSTRAINT [PK_MOV_Manifiesto] PRIMARY KEY CLUSTERED 
(
	[Ident_Manifiesto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MOV_Guia]    Script Date: 07/20/2015 09:10:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOV_Guia](
	[Ident_Guia] [int] IDENTITY(1,1) NOT NULL,
	[NumeroManifiesto] [varchar](10) NULL,
	[NumeroGuia] [varchar](10) NULL,
	[I001_TipoOperacion] [int] NULL,
	[NumeroDocumento] [varchar](10) NULL,
	[Descripcion] [varchar](100) NULL,
	[Bultos] [int] NULL,
	[Peso] [decimal](18, 2) NULL,
	[I002_EstadoCarga] [int] NULL,
	[Completo] [bit] NULL,
 CONSTRAINT [PK_MOV_Guia] PRIMARY KEY CLUSTERED 
(
	[Ident_Guia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_RegistrarGuia]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_RegistrarGuia]

@NumeroManifiesto    VARCHAR(10),
@NumeroGuia          VARCHAR(12),
@TipoDocumento       VARCHAR(40),
@NumeroDocumento     VARCHAR(10),
@Descripcion         VARCHAR(200),
@CantidadBultos		 INT,
@Peso				 DECIMAL,
@EstadoCarga		 INT,
@Completo			 VARCHAR(4)
AS
BEGIN
	INSERT INTO MOV_Guias(NumeroManifiesto, NumeroGuia, TipoDocumento, NumeroDocumento, Descripcion, CantidadBultos, Peso, EstadoCarga, Completo)
	   VALUES(@NumeroManifiesto,@NumeroGuia,@TipoDocumento,@NumeroDocumento,@Descripcion,@CantidadBultos,@Peso,@EstadoCarga,@Completo )
END
GO
/****** Object:  StoredProcedure [dbo].[SP_OPE_ObtenerManifiesto]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_OPE_ObtenerManifiesto]
@MANIFIESTO VARCHAR(10)
AS
BEGIN
	Exec Terminal.dbo.IM_Manifiesto_Consultar @MANIFIESTO
END
GO
/****** Object:  StoredProcedure [dbo].[SP_OPE_ObtenerGuia]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_OPE_ObtenerGuia]
@MANIFIESTO VARCHAR(10),
@GUIA VARCHAR(12)
AS
BEGIN
	Exec Terminal.dbo.IM_Guia_Consultar @MANIFIESTO,@GUIA
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_RegistrarManifiesto]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_RegistrarManifiesto]  
@NumeroManifiesto    VARCHAR(10),  
@LineaArea           VARCHAR(40),  
@NumeroVuelo         VARCHAR(10),  
@FechaArribo         DATETIME  
AS  
BEGIN  
  
 INSERT INTO MOV_Manifiesto(NumeroManifiesto, LineaAerea, NumeroVuelo, FechaArribo)  
    VALUES(@NumeroManifiesto,@LineaArea,@NumeroVuelo,@FechaArribo)  
      
 SELECT IDENT_CURRENT('MOV_Manifiesto') AS Ident_Manifiesto  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_RegistraEvento]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_RegistraEvento]
@Ident_Documento int,
@TipoEvento   varchar(1), -- M:Manifiesto - G:Guias - U:UAs
@CodigoEvento varchar(10),
@FechaEvento  datetime
AS
BEGIN

DECLARE @Ident_Evento int
	
	SET @TipoEvento=LTRIM(RTRIM(@TipoEvento))

	IF @TipoEvento='M' -- Manifiesto
	BEGIN
	
		INSERT INTO MOV_SeguimientoManifiesto(Ident_Manifiesto, TipoEvento, CodigoEvento, FechaEvento)
		VALUES(@Ident_Documento, @TipoEvento, @CodigoEvento, @FechaEvento)	   
	
		SELECT @Ident_Evento = IDENT_CURRENT('MOV_SeguimientoManifiesto')	   
	END

	IF @TipoEvento='G' -- Guia
	BEGIN
		INSERT INTO MOV_SeguimientoGuia(Ident_Guia, TipoEvento, CodigoEvento, FechaEvento)
		VALUES(@Ident_Documento, @TipoEvento, @CodigoEvento, @FechaEvento)
		
		SELECT @Ident_Evento = IDENT_CURRENT('MOV_SeguimientoGuia')
	END	
	
	IF @TipoEvento='U' --Guia
	BEGIN
		INSERT INTO MOV_SeguimientoUAS(Ident_UAS, TipoEvento, CodigoEvento, FechaEvento)
		VALUES(@Ident_Documento, @TipoEvento, @CodigoEvento, @FechaEvento)

		SELECT @Ident_Evento = IDENT_CURRENT('MOV_SeguimientoUAS')						
	END				
	
	SELECT @Ident_Evento As Ident_Evento
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_ObtenerParametros]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author    : Edgard Moras  
-- Description   : Obtiene Parametros  
-- Valores de Prueba : '3'  
-- Fecha de Creacion : 15/07/2015  
-- Fecha de Modificacion:  
-- =============================================  
--select * from PAR_Tabla_Definicion  
--select * from PAR_Tabla_Generica  
  
CREATE PROCEDURE [dbo].[SP_MOV_ObtenerParametros]   
 @Ident_Tabla varchar(6)  
   AS  
BEGIN  
 SET NOCOUNT ON;  
 SELECT GE.Ident_Campo,  
     GE.DescmCampo  
 FROM   dbo.PAR_Tabla_Definicion_Campo GE    
 WHERE  GE.Ident_Tabla=@Ident_Tabla AND  
 GE.IndVisibilidad=1   
 ORDER BY CodiOrden  
 SET NOCOUNT OFF;   
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_ObtenerManifiesto]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_ObtenerManifiesto]  
@MANIFIESTO VARCHAR(10)  
AS  
BEGIN  
 SELECT * FROM dbo.MOV_Manifiesto (NoLock)
 WHERE NumeroManifiesto = @MANIFIESTO
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_MANIFIESTO_ActualizarEstado]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_MANIFIESTO_ActualizarEstado]    
@Ident_Manifiesto int,    
@Estado int
AS
BEGIN        
 UPDATE MOV_Manifiesto
	SET Estado = @Estado
 WHERE Ident_Manifiesto = @Ident_Manifiesto
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_GUIA_INSERTAR]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_GUIA_INSERTAR] (
@NumeroManifiesto varchar(10),
@NumeroGuia varchar(10),
@I001_TipoOperacion int,
@NumeroDocumento varchar(10),
@Descripcion varchar(100),
@Bultos int,
@Peso decimal(18,2)
)
AS
BEGIN
  INSERT INTO MOV_GUIA (NumeroManifiesto,NumeroGuia,I001_TipoOperacion,NumeroDocumento,Descripcion,Bultos, Peso)
  VALUES (@NumeroManifiesto, @NumeroGuia, @I001_TipoOperacion, @NumeroDocumento, @Descripcion, @Bultos, @Peso)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MOV_GUIA_ActualizarRecepcion]    Script Date: 07/20/2015 09:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MOV_GUIA_ActualizarRecepcion]
@Ident_Guia int,
@EstadoCarga int,
@CargaCompleta int
AS
BEGIN
	UPDATE MOV_GUIA
	SET I002_EstadoCarga = @EstadoCarga,
	Completo = @CargaCompleta
	WHERE Ident_Guia = @Ident_Guia	
END
GO
