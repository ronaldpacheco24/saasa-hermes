﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class OrdenRetiroDataAccess
    {
        #region Singleton
        private static OrdenRetiroDataAccess _Instancia = null;
        public static OrdenRetiroDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new OrdenRetiroDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public List<OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            return ImportacionAccess.Instancia.BuscarOrdenRetiro(NumeroPlaca);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
