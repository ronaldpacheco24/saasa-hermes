﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using mv = Mdtk.Saasa.Eventos.Movil.BE;
using ws = Mdtk.Saasa.Eventos.Movil.Access.wsImportacionReference;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class UldDataAccess
    {
        #region Singleton
        private static UldDataAccess _Instancia = null;
        public static UldDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new UldDataAccess(); return _Instancia; }
        }
        #endregion

        #region Metodos Registro
        public int RegistrarUld(mv.ManifiestoBE objManifiestoBE)
        {
            return ImportacionAccess.Instancia.RegistrarUld(objManifiestoBE);
        }
        #endregion
    }
}
