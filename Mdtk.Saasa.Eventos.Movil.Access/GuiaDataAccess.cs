﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.Access;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class GuiaDataAccess
    {
        #region Singleton
        private static GuiaDataAccess _Instancia = null;
        public static GuiaDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new GuiaDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        //public GuiaBE BuscarGuia(GuiaBE objGuia)
        //{
        //    return ImportacionAccess.Instancia.BuscarGuia(objGuia);
        //}
        public List<GuiaBE> BuscarGuia(GuiaBE objGuia)
        {
            return ImportacionAccess.Instancia.BuscarGuia(objGuia);
        }
        #endregion

        #region Metodos Registro

        public List<GuiaBE> RegistrarGuia(GuiaBE objGuia)
        {
            return ImportacionAccess.Instancia.RegistrarGuia(objGuia);
        }

        #endregion

        #region Metodos Actualizacion
        public bool ActualizarRecepcionGuia(GuiaBE guia)
        {
            return ImportacionAccess.Instancia.ActualizarRecepcionGuia(guia);            
        }
        #endregion        
    }
}
