﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class UbicacionDataAccess
    {
        #region Singleton
        private static UbicacionDataAccess _Instancia = null;
        public static UbicacionDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new UbicacionDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public UbicacionBE BuscarUbicacion(string CodigoUbicacion)
        {
            return ImportacionAccess.Instancia.BuscarUbicacion(CodigoUbicacion);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
