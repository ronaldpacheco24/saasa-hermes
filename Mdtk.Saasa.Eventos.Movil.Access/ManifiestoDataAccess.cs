﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class ManifiestoDataAccess
    {
        #region Singleton
        private static ManifiestoDataAccess _Instancia = null;
        public static ManifiestoDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new ManifiestoDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public ManifiestoBE BuscarManifiesto(ManifiestoBE objManifiestoBE)
        {
            return ImportacionAccess.Instancia.BuscarManifiesto(objManifiestoBE);
        }
        #endregion

        #region Metodos Registro

        public int RegistrarLlegadaManifiesto(ManifiestoBE objManifiestoBE)
        {            
            return ImportacionAccess.Instancia.RegistrarLlegadaManifiesto(objManifiestoBE.NumeroManifiesto);
        }

        public bool RegistrarInicioDescargaManifiesto(ManifiestoBE objManifiestoBE)
        {
            return ImportacionAccess.Instancia.RegistrarInicioDescargaManifiesto(objManifiestoBE);
        }
        public bool RegistrarCargaDeManifiesto(ManifiestoBE objManifiestoBE)
        {
            return ImportacionAccess.Instancia.RegistrarCargaDeManifiesto(objManifiestoBE);
        }
        public ManifiestoBE ObtenerCargaDeManifiesto(string txtManifiesto)
        {
            return ImportacionAccess.Instancia.ObtenerCargaDeManifiesto(txtManifiesto);
        }
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
