﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class OperadorDataAccess
    {
        #region Singleton
        private static OperadorDataAccess _Instancia = null;
        public static OperadorDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new OperadorDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public OperadorBE ObtenerGuiasOperador(OperadorBE objOperador)
        {
            return ImportacionAccess.Instancia.ObtenerGuiasOperador(objOperador);
        }

        #endregion

        #region Metodos Registro

        public OperadorBE TomarGuiaOperario(OperadorBE objOperador)
        {
            return ImportacionAccess.Instancia.TomarGuiaOperario(objOperador);
        }

        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
