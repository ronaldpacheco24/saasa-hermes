﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class SalidaDataAccess
    {
        #region Singleton
        private static SalidaDataAccess _Instancia = null;
        public static SalidaDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new SalidaDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public SalidaBE BuscarSalida(SalidaBE SalidaBE)
        {
            return ImportacionAccess.Instancia.BuscarSalida(SalidaBE);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
