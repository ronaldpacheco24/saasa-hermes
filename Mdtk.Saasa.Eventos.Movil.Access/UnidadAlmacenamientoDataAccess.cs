﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class UnidadAlmacenamientoDataAccess
    {
        #region Singleton
        private static UnidadAlmacenamientoDataAccess _Instancia = null;
        public static UnidadAlmacenamientoDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new UnidadAlmacenamientoDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public UnidadAlmacenamientoBE BuscarUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return ImportacionAccess.Instancia.BuscarUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        }

        public List<UnidadAlmacenamientoBE> BuscarUnidadAlmacenamientoList(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return ImportacionAccess.Instancia.BuscarUnidadAlmacenamientoList(objUnidadAlmacenamientoBE);
        }

        public UnidadAlmacenamientoBE BuscarUnidadAlmacenamientoCodigo(string CodigoUA)
        {
            return ImportacionAccess.Instancia.BuscarUnidadAlmacenamientoCodigo(CodigoUA);
        }
        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion

        //public bool ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        //{
        //    return ImportacionAccess.Instancia.ActualizarEstadoUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        //}

        public bool ActualizarUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return ImportacionAccess.Instancia.ActualizarUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        }
        #endregion
    }
}
