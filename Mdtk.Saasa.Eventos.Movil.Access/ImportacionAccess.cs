﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using System.ServiceModel;

using Mdtk.Saasa.Eventos.Movil.Access;
using mv = Mdtk.Saasa.Eventos.Movil.BE;
using ws = Mdtk.Saasa.Eventos.Movil.Access.wsImportacionReference;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class ImportacionAccess
    {
        #region Singleton
        private static ImportacionAccess _Instancia = null;
        public static ImportacionAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new ImportacionAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public mv.ManifiestoBE BuscarManifiesto(mv.ManifiestoBE objManifiestoBE)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            ws.ManifiestoBE wsManifiesto = ws.BuscarManifiesto(objManifiestoBE.NumeroManifiesto, objManifiestoBE.FechaArribo.ToShortDateString(), objManifiestoBE.LineaArea);
            return Movil_ObtenerManifiesto(wsManifiesto);
        }


        public List<mv.GuiaBE> tmpBuscarGuia(mv.GuiaBE objGuiaBE)
        {
            List<mv.GuiaBE> ListaGuias = new List<mv.GuiaBE>();


            mv.GuiaBE NuevaGuia1 = new mv.GuiaBE();
            NuevaGuia1.NumeroGuia = "1233";
            //NuevaGuia1.NumeroBultosReal = Int32.Parse("12");
            //NuevaGuia1.PesoReal = Int32.Parse("18");
            //NuevaGuia1.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia1.PesoManifestado = Int32.Parse("12");
            //NuevaGuia1.Descripcion = "Algo en la guia";
            //NuevaGuia1.Estado = "1";

            List<mv.UnidadAlmacenamientoBE> UAList = new List<mv.UnidadAlmacenamientoBE>();
            mv.UnidadAlmacenamientoBE UA = new mv.UnidadAlmacenamientoBE();
            UA.CodigoUA = "UA1";
            //UA.Bultos = 2;
            UA.Peso = 14.3m;
            UA.UbicacionActual = "ZONA INTERMEDIA";
            UA.UbicacionAsignada = "ZONA INTERMEDIA";
            UA.UbicacionDestino = "ZONA INTERMEDIA";
            UA.TipoCarga = "Normal";
            UA.NumeroGuia = "1211822";
            UA.Estado = "Normal";
            UA.FechaUbicacion = Convert.ToDateTime("07/12/2015 01:23:13");
            NuevaGuia1.ListaUA.Add(UA);

            mv.UnidadAlmacenamientoBE UA1 = new mv.UnidadAlmacenamientoBE();
            UA1.CodigoUA = "UA2";
            //UA1.Bultos = 4;
            UA1.Peso = 13.35m;
            UA1.UbicacionActual = "ZONA INTERMEDIA";
            UA1.UbicacionAsignada = "ZONA AFORO";
            UA1.UbicacionDestino = "ZONA AFORO";
            UA1.TipoCarga = "Normal";
            UA1.NumeroGuia = "11992233";
            UA1.Estado = "Normal";
            UA1.FechaUbicacion = Convert.ToDateTime("07/13/2015 05:12:34");
            NuevaGuia1.ListaUA.Add(UA1);

            ListaGuias.Add(NuevaGuia1);

            mv.GuiaBE NuevaGuia2 = new mv.GuiaBE();
            NuevaGuia2.NumeroGuia = "1238";
            //NuevaGuia2.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia2.PesoManifestado = Int32.Parse("43");
            //NuevaGuia2.NumeroBultosReal = Int32.Parse("22");
            //NuevaGuia2.PesoReal = Int32.Parse("186");
            NuevaGuia2.Descripcion = "Algo en la guia otra ";
            NuevaGuia2.Estado = "1";
            ListaGuias.Add(NuevaGuia2);

            mv.GuiaBE NuevaGuia3 = new mv.GuiaBE();
            NuevaGuia3.NumeroGuia = "77";
            //NuevaGuia3.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia3.PesoManifestado = Int32.Parse("43");
            //NuevaGuia3.NumeroBultosReal = Int32.Parse("19");
            //NuevaGuia3.PesoReal = Int32.Parse("58");
            NuevaGuia3.Descripcion = "Algo en la guia otra x";
            NuevaGuia3.Estado = "3";
            ListaGuias.Add(NuevaGuia3);

            mv.GuiaBE NuevaGuia = new mv.GuiaBE();
            NuevaGuia.NumeroGuia = "176";
            //NuevaGuia.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia.PesoManifestado = Int32.Parse("43");
            NuevaGuia.Descripcion = "Algo en la guia otrax ";
            //NuevaGuia.NumeroBultosReal = Int32.Parse("13");
            //NuevaGuia.PesoReal = Int32.Parse("48");
            NuevaGuia.Estado = "2";
            ListaGuias.Add(NuevaGuia);
            return ListaGuias;
        }
        public List<mv.GuiaBE> BuscarGuia(mv.GuiaBE objGuiaBE)
        {
            var wsGuia = tmpBuscarGuia(objGuiaBE);
            return wsGuia;
        }
        public List<mv.GuiaBE> RegistrarGuia(mv.GuiaBE objGuiaBE)
        {
            List<mv.GuiaBE> ListaGuias = new List<mv.GuiaBE>();

            mv.GuiaBE NuevaGuia2 = new mv.GuiaBE();
            NuevaGuia2.NumeroGuia = "1238";
            //NuevaGuia2.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia2.PesoManifestado = Int32.Parse("43");
            //NuevaGuia2.NumeroBultosReal = Int32.Parse("22");
            //NuevaGuia2.PesoReal = Int32.Parse("186");
            NuevaGuia2.Descripcion = "Algo en la guia otra ";
            NuevaGuia2.Estado = "1";
            ListaGuias.Add(NuevaGuia2);

            mv.GuiaBE NuevaGuia3 = new mv.GuiaBE();
            NuevaGuia3.NumeroGuia = "77";
            //NuevaGuia3.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia3.PesoManifestado = Int32.Parse("43");
            //NuevaGuia3.NumeroBultosReal = Int32.Parse("19");
            //NuevaGuia3.PesoReal = Int32.Parse("58");
            NuevaGuia3.Descripcion = "Algo en la guia otra x";
            NuevaGuia3.Estado = "3";
            ListaGuias.Add(NuevaGuia3);
            mv.GuiaBE NuevaGuia = new mv.GuiaBE();
            NuevaGuia.NumeroGuia = "176";
            //NuevaGuia.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia.PesoManifestado = Int32.Parse("43");
            NuevaGuia.Descripcion = "Algo en la guia otrax ";
            //NuevaGuia.NumeroBultosReal = Int32.Parse("13");
            //NuevaGuia.PesoReal = Int32.Parse("48");
            NuevaGuia.Estado = "2";
            ListaGuias.Add(NuevaGuia);
            return ListaGuias;
        }
        public List<mv.UnidadAlmacenamientoBE> BuscarUnidadAlmacenamientoList(mv.UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            ws.UnidadAlmacenamientoBE[] wsUnidadAlmacenamiento = ws.BuscarUnidadAlmacenamiento(objUnidadAlmacenamientoBE.NumeroGuia);
            List<ws.UnidadAlmacenamientoBE> wsUnidadAlmacenamientoList = new List<ws.UnidadAlmacenamientoBE>(wsUnidadAlmacenamiento);
            List<mv.UnidadAlmacenamientoBE> mvUnidadAlmacenamientoList = new List<mv.UnidadAlmacenamientoBE>();

            foreach (var item in wsUnidadAlmacenamientoList)
            {
                mv.UnidadAlmacenamientoBE UA = new mv.UnidadAlmacenamientoBE();
                UA.CodigoUA = item.CodigoUA;
                UA.NumeroGuia = item.NumeroGuia;
                UA.Bultos = item.Bultos;
                UA.Peso = item.Peso;
                UA.UbicacionActual = item.UbicacionActual;
                UA.UbicacionAsignada = item.UbicacionAsignada;
                mvUnidadAlmacenamientoList.Add(UA);
            }
            return mvUnidadAlmacenamientoList;
        }
        public mv.UnidadAlmacenamientoBE BuscarUnidadAlmacenamiento(mv.UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            mv.UnidadAlmacenamientoBE movilUnidadAlmacenamiento = new mv.UnidadAlmacenamientoBE();
            movilUnidadAlmacenamiento.CodigoUA = "UA1";
            //movilUnidadAlmacenamiento.Bultos = 2;
            movilUnidadAlmacenamiento.Peso = 14.3m;
            movilUnidadAlmacenamiento.UbicacionActual = "ZONA INTERMEDIA";
            movilUnidadAlmacenamiento.UbicacionAsignada = "ZONA INTERMEDIA";
            movilUnidadAlmacenamiento.UbicacionDestino = "ZONA INTERMEDIA";
            movilUnidadAlmacenamiento.TipoCarga = "Normal";
            movilUnidadAlmacenamiento.NumeroGuia = "1211822";
            movilUnidadAlmacenamiento.Estado = "Normal";
            movilUnidadAlmacenamiento.FechaUbicacion = Convert.ToDateTime("07/12/2015 01:23:13");

            return movilUnidadAlmacenamiento;
        }
        public mv.UnidadAlmacenamientoBE BuscarUnidadAlmacenamientoCodigo(string CodigoUA)
        {
            mv.UnidadAlmacenamientoBE movilUnidadAlmacenamiento = new mv.UnidadAlmacenamientoBE();
            if (CodigoUA.Equals("UA1"))
            {
                movilUnidadAlmacenamiento.CodigoUA = "UA1";
                //movilUnidadAlmacenamiento.Bultos = 2;
                movilUnidadAlmacenamiento.Peso = 14.3m;
                movilUnidadAlmacenamiento.UbicacionActual = "ZONA INTERMEDIA";
                movilUnidadAlmacenamiento.UbicacionAsignada = "ZONA INTERMEDIA";
                movilUnidadAlmacenamiento.UbicacionDestino = "ZONA INTERMEDIA";
                movilUnidadAlmacenamiento.TipoCarga = "Normal";
                movilUnidadAlmacenamiento.NumeroGuia = "1211822";
                movilUnidadAlmacenamiento.Estado = "Normal";
            }
            return movilUnidadAlmacenamiento;
        }

        public mv.UbicacionBE BuscarUbicacion(string CodigoUbicacion)
        {
            mv.UbicacionBE movilUbicacion = new mv.UbicacionBE();

            movilUbicacion.CodigoUbicacion = "ALM_21-R30-C12-N43";
            movilUbicacion.Almacen = "ALM_21";
            movilUbicacion.Rack = "R30";
            movilUbicacion.Columna = "C12";
            movilUbicacion.Nivel = "N43";

            return movilUbicacion;
        }

        //public GuiaBE BuscarGuia(GuiaBE objGuia)
        //{
        //    GuiaBE Guia = new GuiaBE();
        //    List<UnidadAlmacenamientoBE> UnidadAlmacenamientoList = new List<UnidadAlmacenamientoBE>();

        //    UnidadAlmacenamientoBE UnidadAlmacenamiento = new UnidadAlmacenamientoBE();
        //    UnidadAlmacenamiento.CodigoUA = "UA1";
        //    UnidadAlmacenamiento.NumeroBultos = 2;
        //    UnidadAlmacenamiento.Peso = 14.3m;
        //    UnidadAlmacenamiento.UbicacionActual = "ZONA INTERMEDIA";
        //    UnidadAlmacenamiento.UbicacionAsignada = "ALMACEN B";
        //    UnidadAlmacenamiento.UbicacionDestino = "ZONA AFORO";
        //    UnidadAlmacenamiento.TipoCarga = "Normal";
        //    UnidadAlmacenamiento.NumeroGuia = "1211822";
        //    UnidadAlmacenamiento.Estado = "Normal";
        //    UnidadAlmacenamientoList.Add(UnidadAlmacenamiento);

        //    UnidadAlmacenamiento.CodigoUA = "UA2";
        //    UnidadAlmacenamiento.NumeroBultos = 5;
        //    UnidadAlmacenamiento.Peso = 11.6m;
        //    UnidadAlmacenamiento.UbicacionActual = "ZONA AFORO";
        //    UnidadAlmacenamiento.UbicacionAsignada = "ALMACEN B";
        //    UnidadAlmacenamiento.UbicacionDestino = "RAMPA II";
        //    UnidadAlmacenamiento.TipoCarga = "Normal";
        //    UnidadAlmacenamiento.NumeroGuia = "1211822";
        //    UnidadAlmacenamiento.Estado = "Normal";
        //    UnidadAlmacenamientoList.Add(UnidadAlmacenamiento);

        //    UnidadAlmacenamiento.CodigoUA = "UA3";
        //    UnidadAlmacenamiento.NumeroBultos = 8;
        //    UnidadAlmacenamiento.Peso = 32;
        //    UnidadAlmacenamiento.UbicacionActual = "ZONA VISTA";
        //    UnidadAlmacenamiento.UbicacionAsignada = "ALMACEN A";
        //    UnidadAlmacenamiento.UbicacionDestino = "RAMPA II";
        //    UnidadAlmacenamiento.TipoCarga = "Normal";
        //    UnidadAlmacenamiento.NumeroGuia = "1211822";
        //    UnidadAlmacenamiento.Estado = "Normal";
        //    UnidadAlmacenamientoList.Add(UnidadAlmacenamiento);

        //    /*

        //    Guia.UnidadAlmacenamiento = UnidadAlmacenamientoList;
        //    Guia.NumeroGuia = NumeroGuia;
        //    Guia.PesoManifestado = 50.4m;
        //    Guia.NumeroBultosManifestado = 20;
        //    Guia.PesoReal = 50;
        //    Guia.NumeroBultosReal = 19;
        //    Guia.Descripcion = "Celulares Chinos";
        //    Guia.Estado = "Normal";
        //    */
        //    return Guia;
        //}

        public List<mv.GuiaBE> BuscarPendientesMovilizacion()
        {
            var ws = new wsImportacionReference.ServicioImportacion();

            ws.GuiaBE[] wsPendienteMovilizacion = ws.BuscarPendientesMovilizacion();

            List<ws.GuiaBE> wsGuiaPendienteMovilizacionList = new List<ws.GuiaBE>(wsPendienteMovilizacion);
            List<mv.GuiaBE> mvGuiaPendienteMovilizacionList = new List<mv.GuiaBE>();
            List<mv.UnidadAlmacenamientoBE> mvUAPendienteMovilizacionList = new List<mv.UnidadAlmacenamientoBE>();

            foreach (var item in wsGuiaPendienteMovilizacionList)
            {
                mv.GuiaBE mvGuiaPendienteMovilizacion = new mv.GuiaBE();
                mvGuiaPendienteMovilizacion.NumeroGuia = item.NumeroGuia;
                
                //item.ListaUA = new ws.UnidadAlmacenamientoBE[];
                
                List<ws.UnidadAlmacenamientoBE> UAList = new List<ws.UnidadAlmacenamientoBE>(item.ListaUA);
                //UAList = new List<ws.UnidadAlmacenamientoBE>();
                mvUAPendienteMovilizacionList = new List<mv.UnidadAlmacenamientoBE>();
                foreach (var item2 in UAList)
                {
                    mv.UnidadAlmacenamientoBE UAPendienteMovilizacion = new mv.UnidadAlmacenamientoBE();
                    UAPendienteMovilizacion.CodigoUA = item2.CodigoUA;
                    UAPendienteMovilizacion.NumeroGuia = item2.NumeroGuia;
                    UAPendienteMovilizacion.Bultos = item2.Bultos;
                    UAPendienteMovilizacion.Peso = item2.Peso;
                    UAPendienteMovilizacion.UbicacionDestino = item2.UbicacionDestino;
                    mvUAPendienteMovilizacionList.Add(UAPendienteMovilizacion);                    
                }
                mvGuiaPendienteMovilizacion.ListaUA = mvUAPendienteMovilizacionList;
                mvGuiaPendienteMovilizacionList.Add(mvGuiaPendienteMovilizacion);
            }
            return mvGuiaPendienteMovilizacionList;
        }

        public List<mv.GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            var ws = new wsImportacionReference.ServicioImportacion();

            ws.GuiaBE[] wsPendienteOperario = ws.BuscarPendientesMovilizacion();

            List<ws.GuiaBE> wsGuiaPendienteOperarioList = new List<ws.GuiaBE>(wsPendienteOperario);
            List<mv.GuiaBE> mvGuiaPendienteOperarioList = new List<mv.GuiaBE>();
            List<mv.UnidadAlmacenamientoBE> mvUAPendienteOperarioList = new List<mv.UnidadAlmacenamientoBE>();

            foreach (var item in mvGuiaPendienteOperarioList)
            {
                mv.GuiaBE mvGuiaPendienteOperario = new mv.GuiaBE();
                mvGuiaPendienteOperario.NumeroGuia = item.NumeroGuia;
                foreach (var item2 in item.ListaUA)
                {
                    mv.UnidadAlmacenamientoBE UAPendienteOperario = new mv.UnidadAlmacenamientoBE();
                    UAPendienteOperario.CodigoUA = item2.CodigoUA;
                    UAPendienteOperario.NumeroGuia = item2.NumeroGuia;
                    UAPendienteOperario.Bultos = item2.Bultos;
                    UAPendienteOperario.Peso = item2.Peso;
                    UAPendienteOperario.UbicacionDestino = item2.UbicacionDestino;
                    mvUAPendienteOperarioList.Add(UAPendienteOperario);
                }
                item.ListaUA = mvUAPendienteOperarioList;
                mvGuiaPendienteOperarioList.Add(item);
            }
            return mvGuiaPendienteOperarioList;
        }

        public List<mv.UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            var ws = new wsImportacionReference.ServicioImportacion();

            ws.UnidadAlmacenamientoBE[] wsDevolucion = ws.BuscarPendientesDevolucion();

            List<ws.UnidadAlmacenamientoBE> wsDevolucionList = new List<ws.UnidadAlmacenamientoBE>(wsDevolucion);
            List<mv.UnidadAlmacenamientoBE> mvDevolucionList = new List<mv.UnidadAlmacenamientoBE>();

            foreach (var item in wsDevolucionList)
            {
                mv.UnidadAlmacenamientoBE DevolucionUA = new mv.UnidadAlmacenamientoBE();
                DevolucionUA.CodigoUA = item.CodigoUA;
                DevolucionUA.UbicacionActual = item.UbicacionActual;
                DevolucionUA.UbicacionAsignada = item.UbicacionAsignada;
                DevolucionUA.NumeroGuia = item.NumeroGuia;
                DevolucionUA.Bultos = item.Bultos;
                DevolucionUA.Peso = item.Peso;
                DevolucionUA.FechaUbicacion = item.FechaUbicacion;
                mvDevolucionList.Add(DevolucionUA);
            }
            return mvDevolucionList;
        }

        public List<mv.OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            var ws = new ws.ServicioImportacion();
            
            ws.OrdenRetiroBE[] wsOrdenRetiro = ws.BuscarOrdenRetiro(NumeroPlaca);

            List<ws.OrdenRetiroBE> wsOrdenRetiroList = new List<ws.OrdenRetiroBE>(wsOrdenRetiro);
            List<mv.OrdenRetiroBE> mvOrdenRetiroList = new List<mv.OrdenRetiroBE>();

            foreach (var item in wsOrdenRetiroList)
            {
                mv.OrdenRetiroBE mvOrdenRetiro = new mv.OrdenRetiroBE();
                mvOrdenRetiro.CodigoRetiro = item.CodigoOrden;
                mvOrdenRetiro.Estado = item.Estado;

                foreach (var item2 in item.ListaGuia)
                {
                    mv.GuiaBE mvGuia = new mv.GuiaBE();
                    mvGuia.NumeroGuia = item2.NumeroGuia;

                    foreach (var item3 in item2.ListaUA)
                    {
                        mv.UnidadAlmacenamientoBE mvUA = new mv.UnidadAlmacenamientoBE();
                        mvUA.CodigoUA = item3.CodigoUA;
                        mvUA.Bultos = item3.Bultos;
                        mvUA.Peso = item3.Peso;
                        mvUA.UbicacionActual = item3.UbicacionActual;
                        mvGuia.ListaUA.Add(mvUA);
                    }
                    mvOrdenRetiro.GuiaList.Add(mvGuia);
                }
                mvOrdenRetiroList.Add(mvOrdenRetiro);
            }
            return mvOrdenRetiroList;
        }

        public mv.SalidaBE BuscarSalida(mv.SalidaBE objSalidaBE)
        {
            mv.SalidaBE Salida = new mv.SalidaBE();
            Salida.CodigoSalida = objSalidaBE.CodigoSalida;
            Salida.FechaSalida = Convert.ToDateTime("01-01-1993");

            List<mv.OrdenRetiroBE> OrdenRetiroList = new List<mv.OrdenRetiroBE>();

            mv.OrdenRetiroBE OrdenRetiro = new mv.OrdenRetiroBE();
            OrdenRetiro.CodigoRetiro = "OR1";
            OrdenRetiro.Estado = "NORMAL";

            List<mv.GuiaBE> GuiaList = new List<mv.GuiaBE>();

            mv.GuiaBE NuevaGuia1 = new mv.GuiaBE();
            NuevaGuia1.NumeroGuia = "1233";
            //NuevaGuia1.NumeroBultosReal = Int32.Parse("12");
            //NuevaGuia1.PesoReal = Int32.Parse("18");
            //NuevaGuia1.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia1.PesoManifestado = Int32.Parse("12");
            NuevaGuia1.Descripcion = "Algo en la guia";
            NuevaGuia1.Estado = "1";
            GuiaList.Add(NuevaGuia1);

            mv.GuiaBE NuevaGuia2 = new mv.GuiaBE();
            NuevaGuia2.NumeroGuia = "1238";
            //NuevaGuia2.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia2.PesoManifestado = Int32.Parse("43");
            //NuevaGuia2.NumeroBultosReal = Int32.Parse("22");
            //NuevaGuia2.PesoReal = Int32.Parse("186");
            NuevaGuia2.Descripcion = "Algo en la guia otra ";
            NuevaGuia2.Estado = "1";
            GuiaList.Add(NuevaGuia2);

            mv.GuiaBE NuevaGuia3 = new mv.GuiaBE();
            NuevaGuia3.NumeroGuia = "77";
            //NuevaGuia3.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia3.PesoManifestado = Int32.Parse("43");
            //NuevaGuia3.NumeroBultosReal = Int32.Parse("19");
            //NuevaGuia3.PesoReal = Int32.Parse("58");
            NuevaGuia3.Descripcion = "Algo en la guia otra x";
            NuevaGuia3.Estado = "3";
            GuiaList.Add(NuevaGuia3);

            mv.GuiaBE NuevaGuia = new mv.GuiaBE();
            //NuevaGuia.NumeroGuia = "176";
            //NuevaGuia.NumeroBultosManifestado = Int32.Parse("43");
            //NuevaGuia.PesoManifestado = Int32.Parse("43");

            //NuevaGuia.NumeroBultosReal = Int32.Parse("13");
            //NuevaGuia.PesoReal = Int32.Parse("48");
            NuevaGuia.Descripcion = "Algo en la guia otrax ";
            NuevaGuia.Estado = "2";
            GuiaList.Add(NuevaGuia);

            OrdenRetiro.GuiaList = GuiaList;

            OrdenRetiroList.Add(OrdenRetiro);

            Salida.OrdenRetiroList = OrdenRetiroList;

            return Salida;
        }

        #endregion

        #region Metodos Registro


        public int RegistrarLlegadaManifiesto(string NumeroManifiesto)
        {
            var cliente = new ws.ServicioImportacion();
            return cliente.RegistrarLlegadaManifiesto(NumeroManifiesto);
        }
        public bool RegistrarInicioDescargaManifiesto(mv.ManifiestoBE objManifiestoBE)
        {
            var cliente = new ws.ServicioImportacion();
            return cliente.RegistrarIniciarDescargaManifiesto(objManifiestoBE.NumeroManifiesto);
        }
        //public ManifiestoBE TmpRegistrarCargaManifiesto(String CodigoManifiesto, int Mayas,int Concamos, List<ConteinerBE> ListaContainers, List<PaletBE> ListaPalets)
        //{
        //    //var wsManifiesto = TmpRegistrarCargaManifiesto(objManifiestoBE.CodigoManifiesto);
        //    //return wsManifiesto;
        //    ManifiestoBE movilManifiesto = new ManifiestoBE();
        //    return movilManifiesto;
        //}

        public bool RegistrarCargaDeManifiesto(mv.ManifiestoBE objManifiestoBE)
        {
            bool cantidad = true;
            var cliente = new ws.ServicioImportacion();

            foreach (var item in objManifiestoBE.ListaUlds)
            {
                cantidad = cliente.RegistrarCargaDeManifiesto(objManifiestoBE.NumeroManifiesto, item.Tipo, item.Codigo, item.Cantidad);

            }
            //return cliente.RegistrarCargaDeManifiesto(objManifiestoBE.NumeroManifiesto);
            return cantidad;
        }
        public mv.ManifiestoBE ObtenerCargaDeManifiesto(string txtManifiesto)
        {
            mv.ManifiestoBE manifiesto;
            var cliente = new ws.ServicioImportacion();

            manifiesto = Movil_ObtenerManifiesto(cliente.ObtenerCargaDeManifiesto(txtManifiesto));
            return manifiesto;
        }

        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            bool result;
            result = ws.RegistrarTomaGuia(NumeroGuia, idenOperario, Estado);
            return result;
        }

        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            bool result;
            result = ws.RegistrarTomaGuia(CodigoUA, idenOperario, Estado);
            return result;
        }

        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            int result;
            result = ws.RegistrarMotivoDemora(NumeroGuia,MotivoDemora,Estado);
            return result;
        }


        #endregion

        #region Metodos Actualizacion

        //public mv.UnidadAlmacenamientoBE ActualizarEstadoUnidadAlmacenamiento(mv.UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        //{
        //    mv.UnidadAlmacenamientoBE UnidadAlmacenamiento = new mv.UnidadAlmacenamientoBE();
        //    UnidadAlmacenamiento = objUnidadAlmacenamientoBE;

        //    return UnidadAlmacenamiento;
        //}

        //public mv.UnidadAlmacenamientoBE ActualizarUnidadAlmacenamiento(mv.UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        //{
        //    mv.UnidadAlmacenamientoBE UnidadAlmacenamiento = new mv.UnidadAlmacenamientoBE();
        //    UnidadAlmacenamiento = objUnidadAlmacenamientoBE;

        //    return UnidadAlmacenamiento;
        //}

        public bool DevolverGuia(string NumeroGuia, string Estado)
        {
            var ws = new wsImportacionReference.ServicioImportacion();
            bool result = ws.DevolverGuia(NumeroGuia, Estado);
            return result;
        }

        public bool ActualizarUnidadAlmacenamiento(mv.UnidadAlmacenamientoBE UnidadAlmacenamiento)
        {
            var ws = new ws.ServicioImportacion();
            ws.UnidadAlmacenamientoBE wsUnidadAlmacenamiento = new ws.UnidadAlmacenamientoBE();
            wsUnidadAlmacenamiento.CodigoUA = UnidadAlmacenamiento.CodigoUA;
            wsUnidadAlmacenamiento.PesoRetirado = UnidadAlmacenamiento.PesoRetirado;
            wsUnidadAlmacenamiento.BultosRetirados = UnidadAlmacenamiento.BultosRetirados;
            wsUnidadAlmacenamiento.Estado = UnidadAlmacenamiento.Estado;

            return ws.ActualizarUnidadAlmacenamiento(wsUnidadAlmacenamiento);
        }

        //public bool ActualizarEstadoUnidadAlmacenamiento(mv.UnidadAlmacenamientoBE UnidadAlmacenamiento)
        //{
        //    var ws = new ws.ServicioImportacion();
        //    ws.UnidadAlmacenamientoBE wsUnidadAlmacenamiento = new ws.UnidadAlmacenamientoBE();
        //    wsUnidadAlmacenamiento.CodigoUA = UnidadAlmacenamiento.CodigoUA;
        //    wsUnidadAlmacenamiento.Estado = UnidadAlmacenamiento.Estado;

        //    return ws.ActualizarEstadoUnidadAlmacenamiento(wsUnidadAlmacenamiento);
        //}

        #endregion

        #region MetodosAuxiliares
        private mv.ManifiestoBE Movil_ObtenerManifiesto(ws.ManifiestoBE wsManifiestoBE)
        {

            mv.ManifiestoBE movilManifiesto = new mv.ManifiestoBE();
            movilManifiesto.NumeroManifiesto = wsManifiestoBE.NumeroManifiesto;
            movilManifiesto.FechaArribo = wsManifiestoBE.FechaArribo.Value;
            movilManifiesto.LineaArea = wsManifiestoBE.LineaArea;
            movilManifiesto.NumeroVuelo = wsManifiestoBE.NumeroVuelo;
            foreach (var row in wsManifiestoBE.ListaGuias)
            {
                var guia = new mv.GuiaBE();
                guia.NumeroGuia = row.NumeroGuia;
                //guia.PesoManifestado = row.Peso;
                //guia.NumeroBultosManifestado = row.Bultos;
                guia.Descripcion = row.Descripcion;
                movilManifiesto.ListaGuias.Add(guia);

            }
            foreach (var row2 in wsManifiestoBE.ListaUlds)
            {
                var uld = new mv.UldBE();
                uld.Codigo = row2.Codigo;
                uld.Tipo = row2.Tipo;
                uld.Cantidad = row2.Cantidad;
                uld.EstadoUld = row2.EstadoUld;
            }
            return movilManifiesto;
        }
        #endregion

        public bool ActualizarRecepcionGuia(mv.GuiaBE guia)
        {
            var cliente = new ws.ServicioImportacion();
            ws.GuiaBE wsGuias = new ws.GuiaBE();
            wsGuias.Ident_Guia = guia.Ident_Guia;
            wsGuias.I002_EstadoCarga = guia.EstadoCarga;
            wsGuias.I003_CargaCompleto = guia.I003_CargaCompleto;
            return cliente.ActualizarRecepcionGuia(wsGuias);
        }
    }
}
