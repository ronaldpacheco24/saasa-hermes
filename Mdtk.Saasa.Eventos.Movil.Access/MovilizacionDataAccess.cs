﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.Access
{
    public class MovilizacionDataAccess
    {
        #region Singleton
        private static MovilizacionDataAccess _Instancia = null;
        public static MovilizacionDataAccess Instancia
        {
            get { if (_Instancia == null)_Instancia = new MovilizacionDataAccess(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public List<GuiaBE> BuscarPendientesMovilizacion()
        {
            return ImportacionAccess.Instancia.BuscarPendientesMovilizacion();
        }

        public List<GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            return ImportacionAccess.Instancia.BuscarPendientesMovilizacionOperario(CodigoOperario);
        }

        public List<UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            return ImportacionAccess.Instancia.BuscarPendientesDevolucion();
        }
        #endregion

        #region Metodos Registro

        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            return ImportacionAccess.Instancia.RegistrarTomaGuia(NumeroGuia, idenOperario, Estado);
        }

        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            return ImportacionAccess.Instancia.RegistrarTomaUA(CodigoUA, idenOperario, Estado);
        }

        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            return ImportacionAccess.Instancia.RegistrarMotivoDemora(NumeroGuia,MotivoDemora,Estado);
        }

        #endregion

        #region Metodos Actualizacion

        public bool DevolverGuia(string NumeroGuia, string Estado)
        {
            return ImportacionAccess.Instancia.DevolverGuia(NumeroGuia,Estado);
        }

        #endregion
    }
}
