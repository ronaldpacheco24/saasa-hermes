﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class UnidadAlmacenamientoDO
    {
        #region Singleton
        private static UnidadAlmacenamientoDO _Instancia = null;
        public static UnidadAlmacenamientoDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new UnidadAlmacenamientoDO(); return _Instancia; }
        }
        #endregion

        public List<UnidadAlmacenamientoBE> BuscarUnidadAlmacenamiento(string NumeroGuia)
        {
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("SP_BuscarUnidadAlmacenamiento",NumeroGuia);
            List<UnidadAlmacenamientoBE> UnidadAlmacenamientoList = new List<UnidadAlmacenamientoBE>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                UnidadAlmacenamientoBE UA = new UnidadAlmacenamientoBE();
                UA.CodigoUA = Convert.ToString(row["CodigoUA"]).Trim();
                UA.UbicacionActual = Convert.ToString(row["UbicacionActual"]);
                UA.UbicacionAsignada = Convert.ToString(row["UbicacionAsignada"]);
                UA.Bultos = Convert.ToInt32(row["Bultos"]);
                UA.Peso = Convert.ToDecimal(row["Peso"]);
                UA.NumeroGuia = Convert.ToString(row["NumeroGuia"]);
                UnidadAlmacenamientoList.Add(UA);
            }
            return UnidadAlmacenamientoList;
        }

        public bool ActualizarUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affect = db.ExecuteNonQuery("SP_BuscarUnidadAlmacenamiento", UnidadAlmacenamiento.CodigoUA, UnidadAlmacenamiento.BultosRetirados, UnidadAlmacenamiento.PesoRetirado, UnidadAlmacenamiento.Estado);
            return affect > 0 ? true : false;
        }

        //public bool ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        //{
        //    Database db = DatabaseFactory.CreateDatabase();
        //    int affect = db.ExecuteNonQuery("SP_ActualizarEstadoSalida", UnidadAlmacenamiento.CodigoUA, UnidadAlmacenamiento.Estado);
        //    return affect > 0 ? true : false;
        //}
    }
}
