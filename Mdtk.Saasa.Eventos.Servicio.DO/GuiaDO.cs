﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Mdtk.Saasa.Eventos.Servicio.BE;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class GuiaDO
    {
        #region Singleton
        private static GuiaDO _Instancia = null;
        public static GuiaDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new GuiaDO(); return _Instancia; }
        }
        #endregion

        public void RegistrarGuia(GuiaBE guia)
        {
            //Manifiesto.
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("SP_MOV_GUIA_INSERTAR", guia.NumeroManifiesto,guia.NumeroGuia,
                guia.I001_TipoOperacion,guia.NumeroDocumento,guia.Descripcion,guia.Bultos,guia.Peso);            
        }

        public int ActualizarRecepcionGuia(GuiaBE guia)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affects = db.ExecuteNonQuery("SP_MOV_GUIA_ActualizarRecepcion", guia.Ident_Guia, guia.I002_EstadoCarga, guia.I003_CargaCompleto);
            return affects;
        }
    }
}
