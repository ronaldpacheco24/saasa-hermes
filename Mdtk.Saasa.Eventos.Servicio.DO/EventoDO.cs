﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class EventoDO
    {

        #region Singleton
        private static EventoDO _Instancia = null;
        public static EventoDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new EventoDO(); return _Instancia; }
        }
        #endregion

        public void RegistrarEvento(EventoBE evento)
        {
            //Manifiesto.
            Database db = DatabaseFactory.CreateDatabase();
            db.ExecuteNonQuery("SP_MOV_RegistraEvento", evento.IdentDocumento, evento.TipoEvento, evento.CodigoEvento, evento.FechaEvento);            
        }        
    }
}
