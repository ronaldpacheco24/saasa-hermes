﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class MovilizacionDO
    {
        #region Singleton
        private static MovilizacionDO _Instancia = null;
        public static MovilizacionDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new MovilizacionDO(); return _Instancia; }
        }
        #endregion

        public List<GuiaBE> BuscarPendientesMovilizacion()
        {
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("IM_OPE_ListarGuiasPendientesDeMovilizacion");
            GuiaBE itemGuia = null;
            UnidadAlmacenamientoBE itemUA = null;
            List<GuiaBE> ListaGuiaPendienteMovilizacion = new List<GuiaBE>();
            List<UnidadAlmacenamientoBE> ListaUA = new List<UnidadAlmacenamientoBE>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                itemGuia = new GuiaBE();
                itemGuia.NumeroGuia = Convert.ToString(row["NumeroGuia"]).Trim();
                DataSet ds3 = db.ExecuteDataSet("IM_OPE_ListarUAS_NumeroGuia_MODIFICADO", itemGuia.NumeroGuia);
                itemGuia.ListaUA = new List<UnidadAlmacenamientoBE>();
                foreach (DataRow row3 in ds3.Tables[0].Rows)
                {
                    itemUA = new UnidadAlmacenamientoBE();
                    itemUA.CodigoUA = Convert.ToString(row3["Codigo_UA"]).Trim();
                    itemUA.NumeroGuia = itemGuia.NumeroGuia;
                    itemUA.Bultos = Convert.ToInt32(row3["Bultos"]);
                    itemUA.Peso = Convert.ToDecimal(row3["Peso"]);
                    itemUA.UbicacionDestino = Convert.ToString(row["Rampa"]);
                    itemGuia.ListaUA.Add(itemUA);
                }
                //itemGuia.ListaUA = ListaUA;
                ListaGuiaPendienteMovilizacion.Add(itemGuia);
            }
            return ListaGuiaPendienteMovilizacion;
        }

        public List<GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("IM_OPE_ListarGuiasPendientesDeMovilizacion", CodigoOperario);
            //MovilizacionBE item = null;
            GuiaBE itemGuia = null;
            UnidadAlmacenamientoBE itemUA = null;
            List<GuiaBE> ListaGuiaPendienteMovilizacion = new List<GuiaBE>();
            List<UnidadAlmacenamientoBE> ListaUA = new List<UnidadAlmacenamientoBE>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                itemGuia = new GuiaBE();
                itemGuia.NumeroGuia = Convert.ToString(row["NumeroGuia"]).Trim();
                DataSet ds3 = db.ExecuteDataSet("IM_OPE_ListarUAS_NumeroGuia_MODIFICADO", Convert.ToString(row["NumeroGuia"]).Trim());

                foreach (DataRow row3 in ds3.Tables[0].Rows)
                {
                    itemUA = new UnidadAlmacenamientoBE();
                    itemUA.CodigoUA = Convert.ToString(row3["Codigo_UA"]).Trim();
                    itemUA.NumeroGuia = itemGuia.NumeroGuia;
                    itemUA.Bultos = Convert.ToInt32(row3["Bulto"]);
                    itemUA.Peso = Convert.ToDecimal(row3["Peso"]);
                    itemUA.UbicacionDestino = Convert.ToString(row["Rampa"]);
                    ListaUA.Add(itemUA);
                }
                itemGuia.ListaUA = ListaUA;
                ListaGuiaPendienteMovilizacion.Add(itemGuia);
            }
            return ListaGuiaPendienteMovilizacion;
        }

        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affect = db.ExecuteNonQuery("SP_TOMAR_GUIA_OPERARIO", NumeroGuia, idenOperario, Estado);

            return affect > 0 ? true : false;
        }

        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affect = db.ExecuteNonQuery("SP_TOMAR_UA_OPERARIO", CodigoUA, idenOperario, Estado);

            return affect > 0 ? true : false;
        }

        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DataSet ds = db.ExecuteDataSet("SP_REGISTRAR_MOTIVODEMORA",NumeroGuia,MotivoDemora,Estado);

            return 0;
        }

        public bool DevolverGuia(string NumeroGuia,string Estado)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affect = db.ExecuteNonQuery("SP_DEVOLVER GUIA", NumeroGuia, Estado);
            return affect > 0 ? true : false;
        }

        public List<UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("IM_OPE_ListarUAPendientesDevolucion");
            UnidadAlmacenamientoBE item = null;

            List<UnidadAlmacenamientoBE> ListaPendientesDevolucion = new List<UnidadAlmacenamientoBE>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new UnidadAlmacenamientoBE();
                item.CodigoUA = Convert.ToString(row["CodigoUA"]).Trim();
                item.UbicacionActual = Convert.ToString(row["UbicacionActual"]);
                item.UbicacionAsignada = Convert.ToString(row["UbicacionAsignada"]);
                item.NumeroGuia = Convert.ToString(row["NumeroGuia"]).Trim();
                item.Bultos = Convert.ToInt32(row["Bultos"]);
                item.Peso = Convert.ToDecimal(row["Peso"]);
                item.FechaUbicacion = Convert.ToDateTime(row["FechaUbicacion"]);
                ListaPendientesDevolucion.Add(item);
            }
            return ListaPendientesDevolucion;
        }
    }

}
