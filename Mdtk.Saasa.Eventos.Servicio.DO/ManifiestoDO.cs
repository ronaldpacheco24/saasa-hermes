﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class ManifiestoDO
    {
        #region Singleton
        private static ManifiestoDO _Instancia = null;
        public static ManifiestoDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new ManifiestoDO(); return _Instancia; }
        }
        #endregion

        public ManifiestoBE BuscarManifiestoIMPO(string CodigoManifiesto, DateTime FechaArribo, string LineaArea)
        {
            //Manifiesto.
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("SP_OPE_ObtenerManifiesto",CodigoManifiesto);
            ManifiestoBE item = null;

            //Lleno Datos Manifiesto
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new ManifiestoBE();
                item.NumeroManifiesto = Convert.ToString(row["NumeroManifiesto"]).Trim();
                item.FechaArribo = Convert.ToDateTime(row["FechaArribo"]);
                item.LineaArea = Convert.ToString(row["LineaAerea"]).Trim();
                item.NumeroVuelo = Convert.ToString(row["NumeroVuelo"]).Trim();

                //Llenar Lista Guias del Manifiesto
                DataSet dset = db.ExecuteDataSet("SP_OPE_ObtenerGuia", CodigoManifiesto,"");
                item.ListaGuias = new List<GuiaBE>();
                foreach (DataRow row2 in dset.Tables[0].Rows)
                {
                    GuiaBE guia = new GuiaBE();
                    guia.NumeroManifiesto = item.NumeroManifiesto;                    
                    guia.NumeroGuia = Convert.ToString(row2["Guia"]).Trim();
                    guia.I001_TipoOperacion = 0; //Impo - Expo                    
                    guia.Descripcion = Convert.ToString(row2["Descripcion"]).Trim();
                    guia.Bultos = Convert.ToInt32(row2["Bulto"]); 
                    guia.Peso = Convert.ToDecimal(row2["Peso"]);                                       
                    item.ListaGuias.Add(guia);

                }
            }            
            return item;
        }
        public ManifiestoBE BuscarManifiesto(string CodigoManifiesto)
        {
            //Manifiesto.
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("SP_MOV_MANIFIESTO_Obtener", CodigoManifiesto);
            ManifiestoBE item = null;

            //Lleno Datos Manifiesto
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new ManifiestoBE();
                item.Ident_Manifiesto = Convert.ToInt32(row["Ident_Manifiesto"]);
                item.NumeroManifiesto = Convert.ToString(row["NumeroManifiesto"]).Trim();
                item.FechaArribo = Convert.ToDateTime(row["FechaArribo"]);
                item.LineaArea = Convert.ToString(row["LineaAerea"]).Trim();
                item.NumeroVuelo = Convert.ToString(row["NumeroVuelo"]).Trim();

                //Llenar Lista Guias del Manifiesto
                DataSet dset = db.ExecuteDataSet("SP_MOV_GUIA_ListarGuia_Manifiesto", CodigoManifiesto);
                item.ListaGuias = new List<GuiaBE>();

                foreach (DataRow row2 in dset.Tables[0].Rows)
                {
                    GuiaBE guia = new GuiaBE();
                    guia.Ident_Guia = Convert.ToInt32(row2["Ident_Guia"]);
                    guia.NumeroGuia = Convert.ToString(row2["NumeroGuia"]).Trim();
                    guia.Peso = Convert.ToDecimal(row2["Peso"]);
                    guia.Bultos = Convert.ToInt32(row2["Bultos"]);
                    guia.Descripcion = Convert.ToString(row2["Descripcion"]).Trim();
                    guia.I002_EstadoCarga = Convert.ToInt32(row2["I002_EstadoCarga"]);                    
                    item.ListaGuias.Add(guia);
                }
                //Llenar lista de cargas
                DataSet dset2 = db.ExecuteDataSet("SP_MOV_CARGA_ListarCarga_Manifiesto", CodigoManifiesto);
                item.ListaUlds = new List<UldBE>();

                foreach (DataRow row3 in dset.Tables[0].Rows)
                {
                    UldBE uld = new UldBE();
                    uld.Codigo = Convert.ToString(row3["CodigoUld"]).Trim();
                    uld.Tipo = Convert.ToChar(row3["Tipo"]);
                    uld.Cantidad = Convert.ToInt32(row3["Cantidad"]);
                    uld.EstadoUld = Convert.ToString(row3["EstadoUld"]);
                    item.ListaUlds.Add(uld);
                }
               
            }
            return item;
        }
        public int RegistrarManifiesto(ManifiestoBE Manifiesto)
        {            
            Database db = DatabaseFactory.CreateDatabase();            
            DataSet ds = db.ExecuteDataSet("SP_MOV_RegistrarManifiesto", Manifiesto.NumeroManifiesto, Manifiesto.LineaArea, Manifiesto.NumeroVuelo, Manifiesto.FechaArribo);
            int Ident_Manifiesto = 0;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Ident_Manifiesto = Convert.ToInt32(row["Ident_Manifiesto"]);
            }
            return Ident_Manifiesto;
        }

        public bool ActualizarEstado(ManifiestoBE manifiesto)
        {            
            Database db = DatabaseFactory.CreateDatabase();            
            int affect = db.ExecuteNonQuery("SP_MOV_MANIFIESTO_ActualizarEstado", manifiesto.Ident_Manifiesto, manifiesto.Estado);
            return affect > 0 ? true : false;
        }
        public bool RegistrarCargaDeManifiesto(string NumeroManifiesto, char tipo, string codigo, int cantidad)
        {
            Database db = DatabaseFactory.CreateDatabase();
            int affect = db.ExecuteNonQuery("SP_REG_MANIFIESTO_RegistraCarga", NumeroManifiesto, tipo,codigo,cantidad);
            return affect > 0 ? true : false;
        }
    }
}
