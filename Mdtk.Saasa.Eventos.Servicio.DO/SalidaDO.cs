﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Mdtk.Saasa.Eventos.Servicio.DO
{
    public class SalidaDO
    {
        #region Singleton
        private static SalidaDO _Instancia = null;
        public static SalidaDO Instancia
        {
            get { if (_Instancia == null)_Instancia = new SalidaDO(); return _Instancia; }
        }
        #endregion

        public List<OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            Database db = DatabaseFactory.CreateDatabase();

            DataSet ds = db.ExecuteDataSet("SP_BuscarOrdenRetiro", NumeroPlaca);
            OrdenRetiroBE item = null;
            GuiaBE item2 = null;
            UnidadAlmacenamientoBE item3 = null;
            List<OrdenRetiroBE> ListaOrdenRetiro = new List<OrdenRetiroBE>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new OrdenRetiroBE();
                item.CodigoOrden = Convert.ToString(row["CodigoOrden"]).Trim();
                item2 = new GuiaBE();
                item2.NumeroGuia = Convert.ToString(row["NumeroGuia"]).Trim();

                DataSet ds2 = db.ExecuteDataSet("SP_UAxNUMEROGUIA", Convert.ToString(row["NumeroGuia"]).Trim());
                foreach (DataRow row3 in ds2.Tables[0].Rows)
                {
                    item3.CodigoUA = Convert.ToString(row3["NumeroUA"]).Trim();
                    item3.Bultos = Convert.ToInt32(row3["Bultos"]);
                    item3.Peso = Convert.ToDecimal(row3["Peso"]);
                    item3.UbicacionActual = Convert.ToString(row3["UbicacionActual"]);
                    item3.NumeroGuia = Convert.ToString(row3["NumeroGuia"]);
                    item2.ListaUA.Add(item3);
                }
                item.ListaGuia.Add(item2);   
                ListaOrdenRetiro.Add(item);
            }
            return ListaOrdenRetiro;
        }
    }
}
