﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.Access;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class UbicacionBL
    {
        #region Singleton
        private static UbicacionBL _Instancia = null;
        public static UbicacionBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new UbicacionBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public UbicacionBE BuscarUbicacion(string CodigoUbicacion)
        {
            return UbicacionDataAccess.Instancia.BuscarUbicacion(CodigoUbicacion);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion

    }
}
