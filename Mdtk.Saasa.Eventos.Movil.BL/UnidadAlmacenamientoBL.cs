﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.Access;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class UnidadAlmacenamientoBL
    {
        #region Singleton
        private static UnidadAlmacenamientoBL _Instancia = null;
        public static UnidadAlmacenamientoBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new UnidadAlmacenamientoBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public UnidadAlmacenamientoBE BuscarUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return UnidadAlmacenamientoDataAccess.Instancia.BuscarUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        }
        public List<UnidadAlmacenamientoBE> BuscarUnidadAlmacenamientoList(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return UnidadAlmacenamientoDataAccess.Instancia.BuscarUnidadAlmacenamientoList(objUnidadAlmacenamientoBE);
        }
        public UnidadAlmacenamientoBE BuscarUnidadAlmacenamientoCodigo(string CodigoUA)
        {
            return UnidadAlmacenamientoDataAccess.Instancia.BuscarUnidadAlmacenamientoCodigo(CodigoUA);
        }
        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion

        //public bool ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        //{
        //    return UnidadAlmacenamientoDataAccess.Instancia.ActualizarEstadoUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        //}

        public bool ActualizarUnidadAlmacenamiento(UnidadAlmacenamientoBE objUnidadAlmacenamientoBE)
        {
            return UnidadAlmacenamientoDataAccess.Instancia.ActualizarUnidadAlmacenamiento(objUnidadAlmacenamientoBE);
        }
        #endregion
    }
}
