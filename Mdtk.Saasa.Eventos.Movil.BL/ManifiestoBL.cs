﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.Access;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class ManifiestoBL
    {
        #region Singleton
        private static ManifiestoBL _Instancia = null;        
        public static ManifiestoBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new ManifiestoBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public ManifiestoBE BuscarManifiesto(ManifiestoBE objManifiesto)
        {
            return ManifiestoDataAccess.Instancia.BuscarManifiesto(objManifiesto);  
        }
        #endregion

        #region Metodos Registro
        public int RegistrarLlegadaManifiesto(ManifiestoBE objManifiesto)
        {
            return ManifiestoDataAccess.Instancia.RegistrarLlegadaManifiesto(objManifiesto);
        }
        public bool RegistrarInicioDescargaManifiesto(ManifiestoBE manifiesto)
        {
            return ManifiestoDataAccess.Instancia.RegistrarInicioDescargaManifiesto(manifiesto);            
        }
        public bool RegistrarCargaDeManifiesto(ManifiestoBE manifiesto)
        {
            return ManifiestoDataAccess.Instancia.RegistrarCargaDeManifiesto(manifiesto);
        }
        public ManifiestoBE ObtenerCargaDeManifiesto(string txtManifiesto)
        {
            return ManifiestoDataAccess.Instancia.ObtenerCargaDeManifiesto(txtManifiesto);
        }

        #endregion

        #region Metodos Actualizacion
        #endregion

        
    }
}
