﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;
namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class MovilizacionBL
    {
        #region Singleton
        private static MovilizacionBL _Instancia = null;
        public static MovilizacionBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new MovilizacionBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        public List<GuiaBE> BuscarPendientesMovilizacion()
        {
            return MovilizacionDataAccess.Instancia.BuscarPendientesMovilizacion();
        }

        public List<GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            return MovilizacionDataAccess.Instancia.BuscarPendientesMovilizacionOperario(CodigoOperario);
        }

        public List<UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            return MovilizacionDataAccess.Instancia.BuscarPendientesDevolucion();
        }
        #endregion

        #region Metodos Registro

        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            return MovilizacionDataAccess.Instancia.RegistrarTomaGuia(NumeroGuia, idenOperario, Estado);
        }

        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            return MovilizacionDataAccess.Instancia.RegistrarTomaUA(CodigoUA, idenOperario, Estado);
        }

        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            return ImportacionAccess.Instancia.RegistrarMotivoDemora(NumeroGuia, MotivoDemora, Estado);
        }

        #endregion

        #region Metodos Actualizacion

        public bool DevolverGuia(string NumeroGuia, string Estado)
        {
            return ImportacionAccess.Instancia.DevolverGuia(NumeroGuia, Estado);
        }

        #endregion
    }
}
