﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class OperadorBL
    {
        #region Singleton
        private static UbicacionBL _Instancia = null;
        public static UbicacionBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new UbicacionBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public OperadorBE ObtenerGuiasOperador(OperadorBE objOperador)
        {
            return OperadorDataAccess.Instancia.ObtenerGuiasOperador(objOperador);
        }

        #endregion

        #region Metodos Registro

        public OperadorBE TomarGuiaOperario(OperadorBE objOperador)
        {
            return OperadorDataAccess.Instancia.TomarGuiaOperario(objOperador);
        }

        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
