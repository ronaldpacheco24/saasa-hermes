﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class SalidaBL
    {
        #region Singleton
        private static SalidaBL _Instancia = null;
        public static SalidaBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new SalidaBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public SalidaBE BuscarSalida(SalidaBE SalidaBE)
        {
            return SalidaDataAccess.Instancia.BuscarSalida(SalidaBE);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
