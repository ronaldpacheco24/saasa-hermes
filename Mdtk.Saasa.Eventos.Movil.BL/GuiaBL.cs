﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;
using Mdtk.Saasa.Eventos.Movil.Access;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class GuiaBL
    {
        #region Singleton
        private static GuiaBL _Instancia = null;
        public static GuiaBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new GuiaBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas
        //public GuiaBE BuscarGuia(string NumeroGuia)
        //{
        //    return GuiaDataAccess.Instancia.BuscarGuia(NumeroGuia);
        //}
        public List<GuiaBE> BuscarGuia(GuiaBE objGuia)
        {
            return GuiaDataAccess.Instancia.BuscarGuia(objGuia);
        }
        #endregion

        #region Metodos Registro

        public List<GuiaBE> RegistrarGuia(GuiaBE objGuia)
        {
            return GuiaDataAccess.Instancia.RegistrarGuia(objGuia);
        }

        #endregion

        #region Metodos Actualizacion
        public bool ActualizarRecepcionGuia(GuiaBE guia)
        {
            return GuiaDataAccess.Instancia.ActualizarRecepcionGuia(guia);
        }
        #endregion
    }
}
