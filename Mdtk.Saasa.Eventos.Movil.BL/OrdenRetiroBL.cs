﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class OrdenRetiroBL
    {
        #region Singleton
        private static OrdenRetiroBL _Instancia = null;
        public static OrdenRetiroBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new OrdenRetiroBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public List<OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            return OrdenRetiroDataAccess.Instancia.BuscarOrdenRetiro(NumeroPlaca);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
