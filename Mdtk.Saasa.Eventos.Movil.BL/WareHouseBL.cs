﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.Access;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BL
{
    public class WareHouseBL
    {
        #region Singleton
        private static WareHouseBL _Instancia = null;
        public static WareHouseBL Instancia
        {
            get { if (_Instancia == null)_Instancia = new WareHouseBL(); return _Instancia; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Metodos Consultas

        public List<WareHouseBE> BuscarWareHouse(WareHouseBE objWareHouse)
        {
            return WareHouseDataAccess.Instancia.BuscarWareHouse(objWareHouse);
        }

        #endregion

        #region Metodos Registro
        #endregion

        #region Metodos Actualizacion
        #endregion
    }
}
