﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mdtk.Saasa.Eventos.Servicio.BE
{
    [Serializable]
    public class EventoBE
    {
        public int Ident_Evento { get; set; }
        public int IdentDocumento {get;set;}
        public string TipoEvento { get; set; } //M:Manifiesto - G:Guias - U:UAS
        public string CodigoEvento { get; set; }
        public DateTime FechaEvento {get;set;}
    }
}
