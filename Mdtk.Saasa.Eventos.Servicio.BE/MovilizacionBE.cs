﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mdtk.Saasa.Eventos.Servicio.BE
{
    public class MovilizacionBE
    {
        public string ident_Movilizacion { get; set; }
        public List<GuiaBE> ListaGuia { get; set; }
        public string NumeroGuia { get; set; }
        public string TipoUbicacion { get; set; }
        public string UbicacionFinal { get; set; }
        public string TomadoPor { get; set; }
        public string MotivoDemora { get; set; }
        public string Ejecutado { get; set; }

        public int NumeroUAs { get; set; }
        public int TiempoMinutos { get; set; }
        //public int Bultos { get; set; }
        //public decimal Peso { get; set; }
    }
}
