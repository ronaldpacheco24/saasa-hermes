﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mdtk.Saasa.Eventos.Servicio.BE
{
    public class OrdenRetiroBE
    {
        public string CodigoOrden { get; set; }
        public List<GuiaBE> ListaGuia { get; set; }
        public string Estado { get; set; }
    }
}
