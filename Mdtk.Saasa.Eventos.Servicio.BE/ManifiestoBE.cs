﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mdtk.Saasa.Eventos.Servicio.BE
{
    [Serializable]
    public class ManifiestoBE
    {
        public int Ident_Manifiesto { get; set; }
        public string NumeroManifiesto { get; set; }
        public string LineaArea { get; set; }
        public string NumeroVuelo { get; set; }
        public DateTime? FechaArribo { get; set; }
        public List<GuiaBE> ListaGuias { get; set; }
        public List<UldBE> ListaUlds { get; set; }
        public int Estado { get; set; }

        
    }
}
