﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using Mdtk.Saasa.Eventos.Servicio.BE;
using Mdtk.Saasa.Eventos.Servicio.BL;

namespace Mdtk.Saasa.Eventos.Servicios
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServicioImportacion : System.Web.Services.WebService
    {

        [WebMethod]
        public ManifiestoBE BuscarManifiesto(string CodigoManifiesto, string FechaArribo, string LineaArea)
        {
            return ManifiestoBL.Instancia.BuscarManifiesto(CodigoManifiesto);
        }

        [WebMethod]
        public int RegistrarLlegadaManifiesto(string Manifiesto)
        {
            return ManifiestoBL.Instancia.RegistrarLlegadaManifiesto(Manifiesto);
        }

        [WebMethod]
        public bool RegistrarIniciarDescargaManifiesto(string NumeroManifiesto)
        {
            return ManifiestoBL.Instancia.RegistrarInicioDescargaManifiesto(NumeroManifiesto);
        }
        
        [WebMethod]
        public bool RegistrarCargaDeManifiesto(string NumeroManifiesto, char tipo, string codigo, int cantidad)
        {
            return ManifiestoBL.Instancia.RegistrarCargaDeManifiesto(NumeroManifiesto, tipo, codigo, cantidad);
        }
        [WebMethod]
        public ManifiestoBE ObtenerCargaDeManifiesto(string NumeroManifiesto)
        {
            return ManifiestoBL.Instancia.ObtenerCargaDeManifiesto(NumeroManifiesto);
        }


        #region Metodos GUIA
        [WebMethod]
        public bool ActualizarRecepcionGuia(GuiaBE guia)
        {
            return GuiaBL.Instancia.ActualizarRecepcionGuia(guia);
        }

        #endregion

        [WebMethod]
        public List<GuiaBE> BuscarPendientesMovilizacion()
        {
            return MovilizacionBL.Instancia.BuscarPendientesMovilizacion();
        }

        [WebMethod]
        public List<GuiaBE> BuscarPendientesMovilizacionOperario(string CodigoOperario)
        {
            return MovilizacionBL.Instancia.BuscarPendientesMovilizacionOperario(CodigoOperario);
        }

        [WebMethod]
        public bool RegistrarTomaGuia(string NumeroGuia, int idenOperario, string Estado)
        {
            return MovilizacionBL.Instancia.RegistrarTomaGuia(NumeroGuia, idenOperario, Estado);
        }

        [WebMethod]
        public bool RegistrarTomaUA(string CodigoUA, int idenOperario, string Estado)
        {
            return MovilizacionBL.Instancia.RegistrarTomaUA(CodigoUA, idenOperario, Estado);
        }

        [WebMethod]
        public int RegistrarMotivoDemora(string NumeroGuia, string MotivoDemora, string Estado)
        {
            return MovilizacionBL.Instancia.RegistrarMotivoDemora(NumeroGuia, MotivoDemora, Estado);
        }

        [WebMethod]
        public bool DevolverGuia(string NumeroGuia, string Estado)
        {
            return MovilizacionBL.Instancia.DevolverGuia(NumeroGuia, Estado);
        }

        [WebMethod]
        public List<UnidadAlmacenamientoBE> BuscarUnidadAlmacenamiento(string NumeroGuia)
        {
            return UnidadAlmacenamientoBL.Instancia.BuscarUnidadAlmacenamiento(NumeroGuia);
        }

        [WebMethod]
        public List<OrdenRetiroBE> BuscarOrdenRetiro(string NumeroPlaca)
        {
            return SalidaBL.Instancia.BuscarOrdenRetiro(NumeroPlaca);
        }

        [WebMethod]
        public bool ActualizarUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        {
            return UnidadAlmacenamientoBL.Instancia.ActualizarUnidadAlmacenamiento(UnidadAlmacenamiento);
        }

        //[WebMethod]
        //public bool ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamientoBE UnidadAlmacenamiento)
        //{
        //    return UnidadAlmacenamientoBL.Instancia.ActualizarEstadoUnidadAlmacenamiento(UnidadAlmacenamiento);
        //}

        [WebMethod]
        public List<UnidadAlmacenamientoBE> BuscarPendientesDevolucion()
        {
            return MovilizacionBL.Instancia.BuscarPendientesDevolucion();
        }
    }
}
