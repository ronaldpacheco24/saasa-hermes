﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class GuiaBE
    {
        public int Ident_Guia { get; set; }
        public string NumeroManifiesto { get; set; }
        public string NumeroGuia { get; set; }
        public int I001_TipoOperacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Descripcion { get; set; }
        public int Bultos { get; set; }
        public decimal Peso { get; set; }        
        public int EstadoCarga { get; set; }
        public int I003_CargaCompleto { get; set; }
        public string Estado { get; set; }
        public List<UnidadAlmacenamientoBE> ListaUA { get; set; }
    }
}
