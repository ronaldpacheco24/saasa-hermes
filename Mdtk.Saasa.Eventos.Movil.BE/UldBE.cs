﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class UldBE
    {
        //Tipos, Concamos C, Mayas M, Palets P, Conteines N, Strups S
        public char Tipo { get; set; }
        //Cantidad
        public int Cantidad { get; set; }
        public string Codigo { get; set; }
        public string EstadoUld { get; set; }
        public string Estado { get; set; } 
    }
}
