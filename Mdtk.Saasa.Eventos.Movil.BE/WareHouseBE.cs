﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class WareHouseBE
    {
        public string NumWareHouse { get; set; }
        public int NumeroBultos { get; set; }
        public decimal Peso { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public List<UnidadAlmacenamientoBE> ListUnidadAlmacenamiento { get; set; }
    }
}
