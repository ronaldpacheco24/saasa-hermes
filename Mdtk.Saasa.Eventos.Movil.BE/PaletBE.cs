﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class PaletBE
    {
        public string CodigoPalet { get; set; }
        public string EstadoPalet { get; set; }
    }
}
