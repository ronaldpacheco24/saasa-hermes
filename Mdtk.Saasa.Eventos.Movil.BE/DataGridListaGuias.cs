﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridListaGuias
    {        
        public string NumeroGuia { get; set; }
        public int Bultos { get; set; }
        public decimal Peso { get; set; }
        public string Descripcion { get; set; }
    }
}
