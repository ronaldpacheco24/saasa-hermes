﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridDetalleGuiaBE
    {
        public string NroUA { get; set; }
        public string Ubicacion { get; set; }
        public int Bls { get; set; }
        public decimal Peso { get; set; }
    }
}
