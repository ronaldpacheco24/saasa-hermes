﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridPendienteOperarioExpoBE
    {
        public string WareHouse { get; set; }
        public string UAs { get; set; }
        public string UbicacionFinal { get; set; }
        public int Bts { get; set; }
        public decimal Peso { get; set; }
        public int TMin { get; set; }
        public int UA_P { get; set; }
        public int UA_C { get; set; }
    }
}
