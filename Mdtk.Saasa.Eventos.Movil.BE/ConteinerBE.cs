﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class ConteinerBE
    {
        public string CodigoConteiner { get; set; }
        public string EstadoConteiner { get; set; }
    }
}
