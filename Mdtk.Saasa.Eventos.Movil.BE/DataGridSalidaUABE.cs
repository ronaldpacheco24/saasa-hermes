﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridSalidaUABE
    {
        public string OrdenRetiro { get; set; }
        public string CodigoUA { get; set; }
        public string UbicacionActual { get; set; }
        public int NumeroBultos { get; set; }
        public decimal Peso { get; set; }
        public string Estado { get; set; }
    }
}
