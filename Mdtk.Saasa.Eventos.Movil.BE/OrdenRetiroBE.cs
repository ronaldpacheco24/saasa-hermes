﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class OrdenRetiroBE
    {
        public string CodigoRetiro { get; set; }
        public List<GuiaBE> GuiaList { get;set; }
        public string Estado { get; set; }
    }
}
