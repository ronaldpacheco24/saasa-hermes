﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class ManifiestoBE
    {
        public string NumeroManifiesto { get; set; }
        public string LineaArea { get; set; }
        public string NumeroVuelo { get; set; }
        public DateTime FechaArribo { get; set; }        
        public List<GuiaBE> ListaGuias { get; set; }
        public List<UldBE> ListaUlds { get; set; }

    }
}
