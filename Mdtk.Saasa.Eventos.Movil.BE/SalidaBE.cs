﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Mdtk.Saasa.Eventos.Movil.BE;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class SalidaBE
    {
        public string CodigoSalida { get; set; }
        public DateTime FechaSalida { get; set; }
        public List<OrdenRetiroBE> OrdenRetiroList { get; set; }
    }
}
