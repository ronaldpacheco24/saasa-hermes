﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    //RONALD PACHECO 23/07/2015 12:25
    public class UnidadAlmacenamientoBE
    {
        public string CodigoUA { get; set; }
        public int Bultos { get; set; }
        public int NumeroWareHouse { get; set; }
        public int BultosRetirados { get; set; }
        public decimal Peso { get; set; }
        public decimal PesoRetirado { get; set; }
        public string UbicacionActual { get; set; }
        public string UbicacionAsignada { get; set; }
        public string UbicacionDestino { get; set; }
        public DateTime FechaUbicacion { get; set; }
        public string TipoCarga { get; set; }
        public string NumeroGuia { get; set; }
        public string Estado { get; set; }
        public string EstadoSalida { get; set; }
    }
}
