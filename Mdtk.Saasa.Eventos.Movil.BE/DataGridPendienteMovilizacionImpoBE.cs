﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridPendienteMovilizacionImpoBE
    {
        public string NGuia { get; set; }
        public string UAs { get; set; }
        public string UbicacionFinal { get; set; }
        public int Bultos { get; set; }
        public decimal Peso { get; set; }
        public int TMin { get; set; }
    }
}
