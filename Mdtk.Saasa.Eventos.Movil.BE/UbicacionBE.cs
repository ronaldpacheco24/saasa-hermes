﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class UbicacionBE
    {
        public string CodigoUbicacion { get; set; }
        public string Almacen { get; set; }
        public string Rack { get; set; }
        public string Columna { get; set; }
        public string Nivel { get; set; }
    }
}
