﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class DataGridPendienteDevolucionExpoBE
    {
        public string WareHouse { get; set; }
        public List<UnidadAlmacenamientoBE> ListUA { get; set; }
        public string NUA { get; set; }
        public string UbiActual { get; set; }
        public string UbiAsignada { get; set; }
        public int Bultos { get; set; }
        public decimal Peso { get; set; }
        public int TMin { get; set; }
    }
}
