﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


namespace Mdtk.Saasa.Eventos.Movil.BE
{
    public class OperadorBE
    {
        public string CodigoOperador { get; set; }
        public string UsuarioOperador { get; set; }
        public string PasswordOperador { get; set; }
        public string Estado { get; set; }
        public List<GuiaBE> ListaGuia { get; set; }
    }
}
